<?php

class User_HomeController extends Zend_Controller_Action
{

    public function init()
    {
		$this->view->title="欢迎您使用本评测系统";
		$this->view->username=Zend_Auth::getInstance()->getIdentity()->name;
		$this->view->userid=Zend_Auth::getInstance()->getIdentity()->id;
		$this->view->serial=Zend_Auth::getInstance()->getIdentity()->serial;
		$this->view->id_card=Zend_Auth::getInstance()->getIdentity()->id_card;
		$reportMapper=new User_Model_ReportMapper();
		$report=$reportMapper->findByUserid(Zend_Auth::getInstance()->getIdentity()->id);
		
		if(isset($report)){
			$this->view->report=$report;
			$this->_forward("report");
			return;
		}
    }

    public function indexAction(){
    	$bm=new User_Model_BasicinfoMapper();
    	$userinfo=new User_Model_Basicinfo();
    	$bm->find(Zend_Auth::getInstance()->getIdentity()->id, $userinfo);
    	if(!$userinfo->getIs_complished()){
    		$this->_forward("basic");
    		return;
    	}

		$mapper=new User_Model_UserMapper();
		$result=$mapper->getCondition(Zend_Auth::getInstance()->getIdentity()->id,Zend_Auth::getInstance()->getIdentity()->sex);
		$this->view->gauges=new stdClass();
		if(is_object($result)){
			$this->view->gauges=$result;
		}
		
    }
    
    public function basicAction(){

    }
    
    public function reportAction(){
    	$answerMapper=new User_Model_AnswerMapper();
    	$answers=$answerMapper->findByUserid(Zend_Auth::getInstance()->getIdentity()->id);
    	$reportMapper=new User_Model_ReportMapper();
    	$report=$reportMapper->findByUserid(Zend_Auth::getInstance()->getIdentity()->id);
        $basicMapper = new User_Model_BasicinfoMapper();
        $basic = new User_Model_Basicinfo();
        $basicMapper->find(Zend_Auth::getInstance()->getIdentity()->id,$basic);
    	$date=new DateTime();
    	$this->view->time=$date->format("Y年m月d日");
    	$this->view->report=$report;
    	$this->view->answers=$answers;
        $this->view->basic = $basic;
    }
    
    public function newAction(){
    	$this->_helper->viewRenderer->setNoRender(true);
    	$mapper=new User_Model_BasicinfoMapper();
    	$info=new User_Model_Basicinfo($this->getRequest()->getParams());
    	$result=$mapper->save($info);
    	if(is_numeric($result)){
    		$this->getResponse()->setHttpResponseCode(201);
    	}else{
    		$this->getResponse()->setHttpResponseCode(422)->appendBody("保存失败，可能是由于：".$result);
    	}
    }
    
    public function gaugeAction(){
    	
    	$gid=$this->getRequest()->getParam("id");
    	$gaugeMapper=new System_Model_GaugeMapper();
    	$gauge=new System_Model_Gauge();
    	$gaugeMapper->find($gid, $gauge);
    	$this->view->gauge=$gauge;
    	
//     	$questionMapper=new System_Model_QuestionMapper();
//     	$questions=$questionMapper->findAll($gid,1,10,"ASC");
//     	$this->view->questions=$questions;
    	
//     	$dimensionMapper=new System_Model_DimensionMapper();
//     	$dimensions=$dimensionMapper->findAllList($gid);
//     	$this->view->dimensions=$dimensions;
//     	$aa="";
    }
    
    public function callAction(){
    	$this->_helper->viewRenderer->setNoRender(true);
    }


   
}

