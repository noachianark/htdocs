<?php
class System_SettingController extends Zend_Rest_Controller
{

    public function init()
    {
    	$this->view->admin_name=Zend_Auth::getInstance()->getIdentity()->username;
    	$this->view->admin_id=Zend_Auth::getInstance()->getIdentity()->id;
    	$this->view->title="系统管理";
    	$this->_helper->viewRenderer->setNoRender(true);
    }

    public function indexAction()
    {
    	$this->_helper->viewRenderer->setNoRender(false);
    	$role=Zend_Auth::getInstance()->getIdentity()->role_id;
    	$this->view->role=$role;
    	if($role==0){
    		//超级管理员，需要查找管理员列表。
    		$adminMapper=new System_Model_AdministratorMapper();
    		$users=$adminMapper->findAll(1,30);
    		$this->view->users=$users;
    	}else{
    		//普通管理员，只需要返回基本信息。
    		
    	}
    }
    
    public function editAction(){
    	echo "edit";
    }
     
    public function newAction(){
    	echo "new";
    }
    
    public function getAction(){
    	echo "get";
    }

    public function postAction(){
		$this->_helper->viewRenderer->setNoRender(true);
		$mapper=new System_Model_SettingMapper();
		$mapper->save(new System_Model_Setting($this->getRequest()->getParams()));
    }

    public function deleteAction(){
    	echo "delete";
    }
    
    public function putAction(){
		$params=$this->getRequest()->getParams();
		$mapper=new System_Model_SettingMapper();
		$result=$mapper->update($params);
		if($result===true){
			$this->getResponse()->setHttpResponseCode(201);
		}else{
			$this->getResponse()->setHttpResponseCode(422)->appendBody($result);			
		}
    }
    
    public function headAction(){
    	 
    }

}