{~include file="header.tpl"~}

<div class="container">
    
	<!-- 返回按钮 -->
    <div class="row">
        <div id="backToMgr" class="backBtn btn-hg col-md-2">
            <span class="glyphicon glyphicon-chevron-left"></span> 返回
        </div>    
    </div>

    <div class="row">
    	<h5>“{~$me["name"]~}”的组成部分</h5>
    </div>

	<div class="row">
		<form role="form" id="material_form">
			<input type="hidden" name="dish_id" value="{~$me['id']~}">
			<div class="col-md-3">
				<div class="from-group">
					<select name="material_id" id="selection">
						{~foreach $all as $option~}
							<option value="{~$option['id']~}">{~$option['name']~}</option>
						{~/foreach~}
					</select>						
				</div>
			</div>

			<div class="col-md-3">
				<div class="form-group">
					<div class="input-group">
						<input class="form-control" name="count" type="text" placeholder="数量">
						<span class="input-group-addon">公斤</span>
					</div>
				</div>
			</div>

			<div class="col-md-3">
				<a class="btn btn-primary addMaterialDepBtn">
					<span class="glyphicon glyphicon-ok"></span> 添加构成
				</a>
			</div>
		
		</form>
	</div>

	<div class="row">
		<table class="table table-striped table-responsive table-bordered">
			<thead>
				<tr>
					<th>原料名称</th>
					<th>份量</th>
					<th>单价</th>
					<th>小计</th>
					<th></th>
				</tr>
			</thead>
			<tbody id="madeup_table">
				{~foreach $materials as $material~}
					<tr>
						<td>{~$material["name"]~}</td>
						<td>{~$material["count"]~}<span class="pull-right">公斤</span></td>
						<td>{~$material["price"]~}<span class="pull-right">元</span></td>
						<td>{~$material["price"]*$material["count"]~}<span class="pull-right">元</span></td>
						<td style="width:80px;"><a class="btn btn-sm btn-danger delDepBtn" data-mid="{~$material['material_id']~}" data-did="{~$material['dish_id']~}">删除</a></td>
					</tr>
				{~/foreach~}
			</tbody>
		</table>
	</div>
	

	

</div>

{~include file="js.tpl"~}
<script type="text/javascript">
	$(function(){
		$("#backToMgr").on("click",function(){
			window.location.href = "/index/manage";
		});

		$("#selection").selectpicker({style: 'btn btn-primary', menuStyle: 'dropdown-inverse'});

		$(".addMaterialDepBtn").on("click",function(){
			$.post(
				"/dishmaterial",
				$("#material_form").serialize()
			).done(function(data){
				window.location.href="/dish/{~$me['id']~}";
			}).fail(faultHandler);
		});
		
		$(".delDepBtn").on("click",function(){
			var did = $(this).data("did");
			$.ajax({
				url:"/dishmaterial/"+$(this).data("mid")+"_"+$(this).data("did"),
				type:"DELETE"
			}).done(function(){
				window.location.href="/dish/"+did;
			}).fail(faultHandler);
		});


	});
</script>
{~include file="footer.tpl"~}