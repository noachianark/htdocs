<?php
class InstallController extends Zend_Controller_Action
{

    public function init()
    {
        /* Initialize action controller here */
    	//$this->view->hello="this is default index ";

    }

    public function indexAction()
    {
    	$this->view->title="系统数据管理";
    	$admin="";
    	try {
    		Zend_Db::factory('PDO_MYSQL', Zend_Registry::get("db_comm")->getConfig())->getConnection();
    		$admin=Zend_Db::factory('PDO_MYSQL', Zend_Registry::get("db_comm")->getConfig())->query("select username from system_user where id=1")->fetchAll();
    	} catch (Exception $e) {
    		if($e->getCode()==1049){
    			$this->view->hasDatabase=false;
    			return;
    		}
    	}
    	if(!count($admin)){
    		//$this->createAction();
    		$this->_forward("create","install","default");
    		return;
    	}
    	//$this->redirect("/");
    	$this->_forward("index","index","default");
    }
    public function createAction(){
    	$err_arr=array();
    	$admin="";
    	try {
    		Zend_Db::factory('PDO_MYSQL', Zend_Registry::get("db_comm")->getConfig())->getConnection();
    		$admin=Zend_Db::factory('PDO_MYSQL', Zend_Registry::get("db_comm")->getConfig())->query("select username from system_user where id=1")->fetchAll();
    	} catch (Exception $e) {
    		if($e->getCode()==1049){
    			
    			$config=Zend_Registry::get("db_comm")->getConfig();
    			$sql = file_get_contents(APPLICATION_PATH.'/configs/localhost.sql');
    			$sql=$this->getSQLStrArr($sql);
    			$conn = mysql_connect($config['host'],$config['username'],$config['password']) or die("database error");
    			foreach ($sql as $v){
    				try{
    					mysql_query("set names utf8");
    					mysql_query($v,$conn);
    				}catch(Exception $e){
    					array_push($err_arr, $e->getMessage());
    				}
    			}
    			if(count($err_arr)){
    				$this->view->result=$err_arr;
    				$this->view->suggest="请联系实施人员对数据库或数据情况进行分析。";
    			}else{
    				$this->view->result="success";//进入账户创建阶段。
    			}
    			return;
    		}
    	}
    	 
    	if(!count($admin)){
    		$this->view->result="success";//进入账户创建阶段。
    	}else{
    		$this->_forward("index","index","default");
    	}
    	 
    }

    public function adminAction(){
    	$this->_helper->viewRenderer->setNoRender(true);
    	$request = $this->getRequest();
    	if (!$request->isXmlHttpRequest()) {
    		$this->redirect("/");
    		return;
    	}
    	if(!count($this->getRequest()->getParams())){
    		$this->redirect("/");
    		return;
    	}
    	 
    	 
    	$mapper=new System_Model_AdministratorMapper();
    	$admin=new System_Model_Administrator();
    	$admin->setOptions($this->getRequest()->getParams());
    	$result=$mapper->save($admin);
    	if($result===true){
    		echo "success";
    	}else{
    		echo $result;
    	}
    }

    
    
    private function getSQLStrArr($file_str){
    	$str = $file_str;
    	$_tmp_arr = explode("\n", $str);
    	$n = 0;
    	foreach ($_tmp_arr as $k=>$v){
    		$v = trim($v);
    		if(empty($v)){
    			continue;
    		}else if(preg_match('/^--/', $v)){
    			continue;
    		}else if(preg_match('/^#/', $v)){
    			continue;
    		}else if (preg_match('/^\/\*/', $v)){
    			continue;
    		}else{
    			$sql_arr[$n] = @$sql_arr[$n]?($sql_arr[$n] . $v):$v;
    			preg_match('/^(.*);$/', $v) && $n++;
    		}
    	}
    	return $sql_arr;
    }
    
}