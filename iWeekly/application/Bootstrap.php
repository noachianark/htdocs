<?php
class Bootstrap extends Zend_Application_Bootstrap_Bootstrap
{
    /**
     */
    protected function _initView()
    {
        // initialize smarty view
        $view = new Ext_View_Smarty($this->getOption('smarty'));
        // setup viewRenderer with suffix and view
        $viewRenderer = Zend_Controller_Action_HelperBroker::getStaticHelper('ViewRenderer');
        $viewRenderer->setViewSuffix('tpl');
        $viewRenderer->setView($view);
         
        // ensure we have layout bootstraped
        $this->bootstrap('layout');
        // set the tpl suffix to layout also
        $layout = Zend_Layout::getMvcInstance();
        $layout->setViewSuffix('tpl');
         
        return $view;
    }
    
    
//     protected function _initFrontController()
//     {
//     	$front = Zend_Controller_Front::getInstance();
//     	$front->addModuleDirectory( APPLICATION_PATH . '/modules');
//     	$front->setDefaultModule('default');
    
//     	$front->registerPlugin(new Zend_Controller_Plugin_ErrorHandler(array(
//     			'module'     => 'default',
//     			'controller' => 'error',
//     			'action'     => 'index'
//     	)));
    
//     	return $front;
//     }
    
    protected function _initRestRoute()
    {
		$front = Zend_Controller_Front::getInstance();
		$router = $front->getRouter();
		
		// Specifying all controllers as RESTful:
// 		$restRoute = new Zend_Rest_Route($front);
// 		$router->addRoute('default', $restRoute);
		
		$restRoute = new Zend_Rest_Route($front, array(), array(
				'default' => array('dish',"material","dishmaterial","cookbook","statistic"),
		));
		$router->addRoute('rest', $restRoute);		
		
    }
    
    
    protected function _initDb(){
    	$resources=$this->getPluginResource("multidb")->init();
    	Zend_Registry::set("db_comm", $this->getPluginResource("multidb")->getDb("comm"));
    }
}

