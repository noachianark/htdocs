<?php
class System_Model_Gauge{
	protected $gid;
	protected $gauge_name;
	protected $description;
	protected $introduction;
	protected $peroration;
	protected $time_limit;
	protected $is_display;
	protected $sex;
	protected $manual;
	protected $is_average;
	
	public function __construct(array $options = null)
	{
		if (is_array($options)) {
			$this->setOptions($options);
		}
	}
	 
	public function setOptions(array $options)
	{
		$methods = get_class_methods($this);
		foreach ($options as $key => $value) {
			$method = 'set' . ucfirst($key);
			if (in_array($method, $methods)) {
				$this->$method($value);
			}
		}
		return $this;
	}
	
	public function toJson(){
		return json_encode($this->toArray());
	}
	
	public function toArray(){
		$array=array();
		foreach ($this as $key => $value) {
			if($value!=null){
				$array[$key] = $value;
			}
		}
		return $array;
	}
	/**
	 * @return the $sex
	 */
	public function getSex() {
		return $this->sex;
	}

	/**
	 * @param field_type $sex
	 */
	public function setSex($sex) {
		$this->sex = $sex;
	}

	/**
	 * @return the $gid
	 */
	public function getGid() {
		return $this->gid;
	}

	/**
	 * @return the $gauge_name
	 */
	public function getGauge_name() {
		return $this->gauge_name;
	}



	/**
	 * @return the $description
	 */
	public function getDescription() {
		return $this->description;
	}

	/**
	 * @param field_type $description
	 */
	public function setDescription($description) {
		$this->description = $description;
	}

	/**
	 * @return the $explanation
	 */
	public function getExplanation() {
		return $this->explanation;
	}

	/**
	 * @return the $introduction
	 */
	public function getIntroduction() {
		return $this->introduction;
	}



	/**
	 * @return the $peroration
	 */
	public function getPeroration() {
		return $this->peroration;
	}



	/**
	 * @return the $time_limit
	 */
	public function getTime_limit() {
		return $this->time_limit;
	}



	/**
	 * @param field_type $gid
	 */
	public function setGid($gid) {
		$this->gid = $gid;
	}

	/**
	 * @param field_type $gauge_name
	 */
	public function setGauge_name($gauge_name) {
		$this->gauge_name = $gauge_name;
	}



	/**
	 * @return the $is_display
	 */
	public function getIs_display() {
		return $this->is_display;
	}

	/**
	 * @param field_type $is_display
	 */
	public function setIs_display($is_display) {
		$this->is_display = $is_display;
	}

	/**
	 * @param field_type $explanation
	 */
	public function setExplanation($explanation) {
		$this->explanation = $explanation;
	}

	/**
	 * @param field_type $introduction
	 */
	public function setIntroduction($introduction) {
		$this->introduction = $introduction;
	}


	/**
	 * @param field_type $peroration
	 */
	public function setPeroration($peroration) {
		$this->peroration = $peroration;
	}

	/**
	 * @return the $is_average
	 */
	public function getIs_average() {
		return $this->is_average;
	}

	/**
	 * @param field_type $is_average
	 */
	public function setIs_average($is_average) {
		$this->is_average = $is_average;
	}

	/**
	 * @return the $manual
	 */
	public function getManual() {
		return $this->manual;
	}

	/**
	 * @param field_type $manual
	 */
	public function setManual($manual) {
		$this->manual = $manual;
	}

	/**
	 * @param number $time_limit
	 */
	public function setTime_limit($time_limit) {
		$this->time_limit = (int)$time_limit;
	}

}

?>