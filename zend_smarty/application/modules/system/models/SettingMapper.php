<?php
class System_Model_SettingMapper {
	protected $_dbTable;
	
	public function setDbTable($dbTable)
	{
		if (is_string($dbTable)) {
			$dbTable = new $dbTable();
		}
		if (!$dbTable instanceof Zend_Db_Table_Abstract) {
			throw new Exception('Invalid table data gateway provided');
		}
		$this->_dbTable = $dbTable;
		return $this;
	}
	public function getDbTable(){
		if (null === $this->_dbTable) {
			$this->setDbTable('System_Model_DbTable_Setting');
		}
		return $this->_dbTable;
	}
	public function save(System_Model_Setting $sets){
		try{
			$result=$this->getDbTable()->insert($sets->toArray());
		}catch (Exception $e){
			return Utils_SQLErrorMsg::formatException($e);
		}
		return (int)$result;
	}
	
	public function findCount(){
		$rows=$this->getDbTable()->getAdapter()->fetchRow("select count(*) from gauge_set");
		return $rows;
	}
	
	public function update($data){
		try{
			$model=new System_Model_Setting($data);
			$data=$this->getDbTable()->update($model->toArray(), "id=1");
			return true;
		}catch(Exception $e){
			return $e;
		}
	}
	
	
	public function find($userid){
		try{
			$data=$this->getDbTable()->find(1)->current()->toArray();
			return  new System_Model_Setting($data);
			
		}catch(Exception $e){
			return $e;
		}
	}
	
}
