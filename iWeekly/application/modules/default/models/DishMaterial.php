<?php
class Default_Model_DishMaterial{
	protected $dish_id;
	protected $material_id;
	protected $count;
	
	public function __construct(array $options = null)
	{
		if (is_array($options)) {
			$this->setOptions($options);
		}
	}
	
	public function setOptions(array $options)
	{
		$methods = get_class_methods($this);
		foreach ($options as $key => $value) {
			$method = 'set' . ucfirst($key);
			if (in_array($method, $methods)) {
				$this->$method($value);
			}
		}
		return $this;
	}
	
	public function toJson(){
		return json_encode($this->toArray());
	}
	
	public function toArray(){
		$array=array();
		foreach ($this as $key => $value) {
			if($value!=null){
				$array[$key] = $value;
			}
		}
		return $array;
	}
	/**
	 * @return the $dish_id
	 */
	public function getDish_id() {
		return $this->dish_id;
	}

	/**
	 * @return the $material_id
	 */
	public function getMaterial_id() {
		return $this->material_id;
	}

	/**
	 * @return the $count
	 */
	public function getCount() {
		return $this->count;
	}

	/**
	 * @param field_type $dish_id
	 */
	public function setDish_id($dish_id) {
		$this->dish_id = $dish_id;
	}

	/**
	 * @param field_type $material_id
	 */
	public function setMaterial_id($material_id) {
		$this->material_id = $material_id;
	}

	/**
	 * @param field_type $count
	 */
	public function setCount($count) {
		$this->count = $count;
	}










	
}

?>