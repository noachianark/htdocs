<script>
	$(function(){
		page=parseInt("{~$questions->page~}");
		total=parseInt("{~$questions->total~}");
		records=parseInt("{~$questions->records~}");
		$("#current_page").text(page);
		$("#total_page").text(total);
		$("#count").text(records);
		

		$("input[type='radio']").click(function(){
			var choose=$(this).parent().find("input[type='radio']:checked").val();
			if($(this).parent().parent().parent().hasClass("success")){
				return;
			}
			
			if(choose!=undefined){
				done+=1;
				if(done>0){
					rate=parseInt(done/records*100);
				}else{
					rate=0;
				}
				$(this).parent().parent().parent().addClass("success");
				$(this).parent().parent().parent().find(".done_icon").addClass("icon-ok");
				$(this).parent().parent().parent().removeClass("error");
				$("#progress").css("width",rate+"%");
				$("#percent").text(rate+"%");
				$("#done").text(done);
				if(rate==100){
					$("#submit").removeClass("disabled");
					$("#submit").click(submitAnswer);
				}
			}
		});		

	});
</script>
<div class="question_form"  data-page="{~$questions->page~}">
	<form class="form-horizontal question_form">
		<fieldset>
			{~foreach $questions->rows as $question~}
			<div class="control-group">											
				<label for="phone" class="control-label" style="font-weight:bolder;font-size:15px;"><i class="done_icon"></i>{~$question['sequence']~}.</label>
				<div class="controls">
					<label class="radio" style="padding-left:0px;font-weight:bolder;font-size:15px;">{~$question['content']~}</label>
					{~foreach $question['choices'] as $choice~}
					<label class="radio">
						<input type="radio" value="{~$choice['weight']~}" data-id="{~$question['qid']~}" data-dimension="{~$question['dimension_id']~}" class="question_input" name="{~$question['qid']~}">{~$choice['answer']~}
					</label>
					{~/foreach~}
				</div> <!-- /controls -->				
			</div> <!-- /control-group -->		
			{~/foreach~}									
		</fieldset>
	</form>
</div>
