<?php
class User_Model_Result{
	protected $score;
	protected $conclusion;
	protected $passed;
	protected $decisive;
	protected $dimension_name;
	
	public function __construct(array $options = null)
	{
		if (is_array($options)) {
			$this->setOptions($options);
		}
	}
	
	public function setOptions(array $options)
	{
		$methods = get_class_methods($this);
		foreach ($options as $key => $value) {
			$method = 'set' . ucfirst($key);
			if (in_array($method, $methods)) {
				$this->$method($value);
			}
		}
		return $this;
	}
	
	/**
	 * @return the $score
	 */
	public function getScore() {
		return $this->score;
	}

	/**
	 * @return the $conclusion
	 */
	public function getConclusion() {
		return $this->conclusion;
	}

	/**
	 * @param field_type $score
	 */
	public function setScore($score) {
		$this->score = $score;
	}

	/**
	 * @param field_type $conclusion
	 */
	public function setConclusion($conclusion) {
		$this->conclusion = $conclusion;
	}
	/**
	 * @return the $passed
	 */
	public function getPassed() {
		return $this->passed;
	}

	/**
	 * @param field_type $passed
	 */
	public function setPassed($passed) {
		$this->passed = $passed;
	}
	/**
	 * @return the $decisive
	 */
	public function getDecisive() {
		return $this->decisive;
	}

	/**
	 * @param field_type $decisive
	 */
	public function setDecisive($decisive) {
		$this->decisive = $decisive;
	}
	/**
	 * @return the $dimension_name
	 */
	public function getDimension_name() {
		return $this->dimension_name;
	}

	/**
	 * @param field_type $dimension_name
	 */
	public function setDimension_name($dimension_name) {
		$this->dimension_name = $dimension_name;
	}




}

?>