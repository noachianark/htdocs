<?php
class System_DimensionController extends Zend_Rest_Controller
{

    public function init()
    {
    	
    }

    public function indexAction()
    {
		$gid=$this->getRequest()->getParam("gid");
		$page=$this->getRequest()->getParam("page");
		$mapper=new System_Model_DimensionMapper();
		$dimensions=$mapper->findAll(10,isset($page)?$page:1,$gid,false);
		foreach($dimensions->rows as $value){
			$stdClass=json_decode($value['rules']);
			$aaa="";
		}
		$this->view->dimensions=$dimensions;
    }
    
    public function editAction(){
    	$id=$this->getRequest()->getParam("id");
    	$dimension=new System_Model_Dimension();
    	$mapper=new System_Model_DimensionMapper();
    	$result=$mapper->findById($id, $dimension);
    	if(isset($result)){
    		$this->_helper->viewRenderer->setNoRender(true);
    		$this->getResponse()->setHttpResponseCode(422)->appendBody($result);
    	}else{
    		$this->view->dimension=$dimension;
    	}
    }
     
    public function newAction(){
		$this->view->dimension=new System_Model_Dimension();
    }
    
    public function getAction(){
    	echo "get";
    }

    public function postAction(){
		$this->_helper->viewRenderer->setNoRender(true);
		$gid=$this->getRequest()->getParam("gid");
		$dimension=new System_Model_Dimension($this->getRequest()->getParams());
		$mapper=new System_Model_DimensionMapper();
		$result=$mapper->save($dimension);
    	if(is_numeric($result)){
			$this->getResponse()->setHttpResponseCode(201)->appendBody($dimension->getGid());
		}else{
			$this->getResponse()->setHttpResponseCode(422)->appendBody($result);
		}
    }

    public function deleteAction(){
    	echo "delete";
    }
    
    public function putAction(){
    	$this->_helper->viewRenderer->setNoRender(true);
		$dimension=new System_Model_Dimension($this->getRequest()->getParams());
		$mapper=new System_Model_DimensionMapper();
		$result=$mapper->update($dimension);
    	if(is_numeric($result)){
			$this->getResponse()->setHttpResponseCode(201)->appendBody($dimension->getGid());
		}else{
			$this->getResponse()->setHttpResponseCode(422)->appendBody($result);
		}
    }
    
    public function headAction(){
    	 
    }

}