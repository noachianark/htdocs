<?php
class Utils_SQLErrorMsg {
	
	public static function formatException(Exception $e){
		$translate=new Zend_Translate(array(
				"adapter"=>"array",
				"content"=>self::$translateArray,
				"locale"=>"zh"
				));
		//$translate->addTranslation();
		switch($e->getCode()){
			case 23000://数据唯一性验证不通过。
				if($e->getPrevious()->errorInfo){
					//return $translate->_($e->getPrevious()->errorInfo[2]);
					return "唯一性的数据在数据库中已经保存过，无法保存";
				}
				
				//return "数据已经存在，无法保存";
				break;
			default:
				return $e->getMessage().$e->getCode();
				break;
		}
	}
	
	private static $translateArray=array(
			"Duplicate entry"=>"唯一性的数据在数据库中已经保存过，无法保存"
			);
}

?>