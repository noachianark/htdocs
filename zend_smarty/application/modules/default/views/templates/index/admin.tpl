<style>
   #login_panel{
   	  background-image:url('/images/login_panel.png');
      position: absolute;
      width:468px;
      height:290px;
      left:50%;
      top:50%;
	  margin-left:-234px;
	  margin-top:-145px;
   }
   #error_tips{
       float: left;
   }
   #form_field{
		position:absolute;
		bottom:80px;
		left:30px;
		right:30px;
   }
</style>

<div id="login_panel">
	<div style="position:absolute;left:30px;top:8px;">
		<label style="color:#feffff;display:inline;font-size:24px;">账号登录</label>
	</div>
	<div id="form_field">
		<label id="user_label" style="color:#7c7c7c;font-size:14px;font-weight:bolder;">用户名</label>
		<input id="username_input" class="text ui-widget-content ui-corner-all" style="width:395px;height:20px;color:#ff6000;font-size:14px;font-weight:bolder;"/>
		<label style="color:#7c7c7c;font-size:14px;font-weight:bolder;">密码</label>
		<input id="password_input" type="password" class="text ui-widget-content ui-corner-all" style="width:395px;height:20px;color:#ff6000;font-size:14px;font-weight:bolder;"/>
	</div>
	<div style="position:absolute;right:55px;bottom:70px;vertical-align:middle;">
		<input id="checkbox" type="checkbox" value="1" style="float:left;"/><span style="color:#7c7c7c;margin-left:4px;margin-bottom:4px;">管理员身份</span>
	</div>
	<div style="color:#df7d3a;position:absolute;left:30px;bottom:45px;">
		<a id="register" style="cursor:pointer;">注册新用户</a>
	</div>
	<button id="btn_login" style="right:30px;bottom:30px;position:absolute;height:35px;width:100px;font-size:18px;"> 登 录 </button>
</div>