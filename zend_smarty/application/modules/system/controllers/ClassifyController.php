<?php
class System_ClassifyController extends Zend_Controller_Action
{

    public function init()
    {
    	$this->_helper->viewRenderer->setNoRender(true);
    	$this->view->admin_name=Zend_Auth::getInstance()->getIdentity()->username;
    }

    public function indexAction()
    {	
		//$this->overviewAction();
    }    

    public function addAction(){
    	if($this->getRequest()->isXMLHttpRequest()){
			if($this->getRequest()->getParam("name")){
				$mapper=new System_Model_ClassifyMapper();
				$classify=new System_Model_Classify();
				$classify->setClassify_name($this->getRequest()->getParam("name"));
				$result=$mapper->save($classify);
				if(is_numeric($result)){
					$this->getResponse()->setHttpResponseCode(200)->appendBody($result);
				}else{
					$this->getResponse()->setHttpResponseCode(400)->appendBody($result);
				}
			}
    	}
    }
    
    public function editAction(){
    	
    }
    
    public function newAction(){
    	
    }
    public function deleteAction(){
    	
    }
    public function updateAction(){
    	
    }
    public function showAction(){
    	
    }

}