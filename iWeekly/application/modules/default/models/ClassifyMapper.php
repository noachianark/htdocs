<?php
class Default_Model_ClassifyMapper {
	protected $_dbTable;
	
	public function setDbTable($dbTable)
	{
		if (is_string($dbTable)) {
			$dbTable = new $dbTable();
		}
		if (!$dbTable instanceof Zend_Db_Table_Abstract) {
			throw new Exception('Invalid table data gateway provided');
		}
		$this->_dbTable = $dbTable;
		return $this;
	}
	public function getDbTable(){
		if (null === $this->_dbTable) {
			$this->setDbTable('Default_Model_DbTable_Classify');
		}
		return $this->_dbTable;
	}
	public function save(Default_Model_Classify $classify){
		try{
			$result=$this->getDbTable()->insert($classify->toArray());
		}catch (Exception $e){
			return $e;
		}
		return (int)$result;
	}
	
	public function update(Default_Model_Classify $classify){
		if($classify->getId()){
			try{
				$result=$this->getDbTable()->update($classify->toArray(), "id=".$classify->getId());
			}catch(Exception $e){
				return "数据库有问题";
			}
		}else{
			return "没有找到该记录，无法更新";
		}
		return (int)$result;		
	}

	
	public function findAll(){
		try{
			$result = $this->getDbTable()->fetchAll()->toArray();
			return $result;
		}catch(Exception $e){
			return $e;
		}
	}
	
	
	public function delete($id){
		try{
			$result = $this->getDbTable()->find($id);
			$dish = $result->current();
			$dish->delete();
			return "success";
		}catch(Exception $e){
			return $e;
		}
	}
	

	

	
}
