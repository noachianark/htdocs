<?php
class Default_Model_MaterialMapper {
	protected $_dbTable;
	
	public function setDbTable($dbTable)
	{
		if (is_string($dbTable)) {
			$dbTable = new $dbTable();
		}
		if (!$dbTable instanceof Zend_Db_Table_Abstract) {
			throw new Exception('Invalid table data gateway provided');
		}
		$this->_dbTable = $dbTable;
		return $this;
	}
	public function getDbTable(){
		if (null === $this->_dbTable) {
			$this->setDbTable('Default_Model_DbTable_Material');
		}
		return $this->_dbTable;
	}
	public function save(Default_Model_Material $material){
		try{
			$result=$this->getDbTable()->insert($material->toArray());
		}catch (Exception $e){
			return json_encode($material);
		}
		return (int)$result;
	}
	
	public function update(Default_Model_Material $dish){
		if($dish->getId()){
			try{
				$result=$this->getDbTable()->update($dish->toArray(), "id=".$dish->getId());
			}catch(Exception $e){
				return "数据库有问题";
			}
		}else{
			return "没有找到该记录，无法更新";
		}
		return (int)$result;		
	}

	
	public function findAll(){
		try{
			$result = $this->getDbTable()->fetchAll()->toArray();
			return $result;
		}catch(Exception $e){
			return $e;
		}
	}
	
	
	public function delete($id){
		try{
			$result = $this->getDbTable()->find($id);
			$dish = $result->current();
			$dish->delete();
			return "success";
		}catch(Exception $e){
			return $e;
		}
	}
	
	public function findTotalPrice(){
	    	$c_mapper = new Default_Model_ClassifyMapper();
	    	$classify = $c_mapper->findAll();
	    	$array = array();
	    	foreach($classify as $cls){
	    		$id = $cls["id"];
	    		$m_mapper = new Default_Model_MaterialMapper();
	    		$result = $m_mapper -> findByType($id);
	    		$row = array();
	    		$row["name"] = $cls["name"];
	    		$total = 0.0;
	    		$count = 0.0;
	    		foreach($result as $material){
	    			$total += (float)$material["count"] * (float)$material["price"];
	    			$count += (float)$material["count"]; 
	    		}
	    		$row["total"] = $total;
	    		$row["count"] = $count;
	    		if($count > 0){
	    			array_push($array, $row);
	    		}
	    		
	    	}
	    	return $array;
	}
	
	public function findByType($id){
		try{
			$select=$this->getDbTable()->select()->where("type_id = $id");
			$data = $this->getDbTable()->fetchAll($select)->toArray();
			return $data;
		}catch(Exception $e){
			return $e;
		}
	}
	

	

	
}
