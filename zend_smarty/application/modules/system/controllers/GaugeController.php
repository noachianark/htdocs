<?php
class System_GaugeController extends Zend_Rest_Controller
{

    public function init()
    {
    	$this->view->admin_name=Zend_Auth::getInstance()->getIdentity()->username;
    	$this->view->title="系统管理";
    	$this->view->tab=$this->getRequest()->getParam("tab");
    }

    public function indexAction()
    {
        $page=$this->getRequest()->getUserParam("page");
    	$limit=$this->getRequest()->getUserParam("rows");
    	$page=isset($page)?$page:1;
    	$limit=isset($limit)?$limit:10;
    	$gaugeMapper=new System_Model_GaugeMapper();
    	$gauges=$gaugeMapper->findAll($page,$limit,false);
    	$userMapper=new User_Model_UserMapper();
    	$dashboard=$userMapper->getDashboard();
    	$this->view->gauges=$gauges;
    	$this->view->dash=$dashboard;
    }
    
    public function newAction(){
    	$gauge=new System_Model_Gauge();
    	$this->view->gauge=$gauge;
    }
    

    public function getAction(){
    	$id=$this->getRequest()->getParam("id");
    	$gauge=new System_Model_Gauge();
    	$gaugeMapper=new System_Model_GaugeMapper();
    	$error=$gaugeMapper->find($id, $gauge);
    	if(isset($error)){
    		$this->view->error= $error;
    	}
    	
    	$this->view->gauge=$gauge;//如果为新建，则为空对象。
    }
    
    public function editAction(){
    	$id=$this->getRequest()->getParam("id");
    	$gauge=new System_Model_Gauge();
    	$gaugeMapper=new System_Model_GaugeMapper();
    	$error=$gaugeMapper->find($id, $gauge);
    	if(isset($error)){
    		$this->view->error= $error;
    	}
    	$this->view->gauge=$gauge;//如果为新建，则为空对象。    	
    }
    
    public function putAction(){
    	$this->_helper->viewRenderer->setNoRender(true);
    	$gauge=new System_Model_Gauge($this->getRequest()->getParams());
    	//$gauge->setGid($this->getRequest()->getParam("id"));
    	$aaa=$this->getRequest()->getParams();
    	$mapper=new System_Model_GaugeMapper();
    	$result=$mapper->update($gauge);
    	if(is_numeric($result)){
    		$this->getResponse()->setHttpResponseCode(201)->appendBody($this->getRequest()->getParam("id"));
    	}else{
    		$this->getResponse()->setHttpResponseCode(422)->appendBody($result);
    	}
    }
    
    
    
    public function postAction(){
    	$this->_helper->viewRenderer->setNoRender(true);
    	if(count($this->getRequest()->getParams())){
			$gauge=new System_Model_Gauge();
			$gauge->setOptions($this->getRequest()->getParams());
			$mapper=new System_Model_GaugeMapper();
			$result=$mapper->save($gauge);
			if(is_numeric($result)){
				$this->getResponse()->setHttpResponseCode(201)->appendBody($gauge->getGid());
			}else{
				$this->getResponse()->setHttpResponseCode(422)->appendBody($result);
			}
    	}else{
    		$this->getResponse()->setHttpResponseCode(422)->appendBody("no param");
    	}
    	
    }

    
    public function deleteAction(){
    	$this->_helper->viewRenderer->setNoRender(true);
    	//$ids=explode(",", $this->getRequest()->getParam("id"));
    	//echo json_encode($ids);
    	$gid=$this->getRequest()->getParam("id");
    	$mapper=new System_Model_GaugeMapper();
    	$result=$mapper->delete($this->getRequest()->getParam("id"));
    	if(is_numeric($result)){
    		$this->getResponse()->setHttpResponseCode(200)->appendBody($result);
    	}else{
    		$this->getResponse()->setHttpResponseCode(422)->appendBody($result);
    	}
    }

    public function headAction(){
    	 
    }

    


}