<?php
class CookbookController extends Zend_Rest_Controller
{

    public function init()
    {
        /* Initialize action controller here */
    	//$this->view->hello="this is default index ";
    	$this->_helper->viewRenderer->setNoRender(true);
    }



    public function indexAction()
    {
    	$this->_helper->viewRenderer->setNoRender(false);
		$mapper=new Default_Model_CookbookMapper();
		$result = $mapper->findAll();


        $cookbookMap = array(
                                '0' =>array(
                                            "0"=>array(),
                                            "1"=>array(),
                                            "2"=>array(),
                                            "3"=>array(),
                                            "4"=>array(),
                                            "5"=>array(),
                                            "6"=>array()
                                            ),
                                '1' =>array(
                                            "0"=>array(),
                                            "1"=>array(),
                                            "2"=>array(),
                                            "3"=>array(),
                                            "4"=>array(),
                                            "5"=>array(),
                                            "6"=>array()
                                            ),
                                '2' =>array(
                                            "0"=>array(),
                                            "1"=>array(),
                                            "2"=>array(),
                                            "3"=>array(),
                                            "4"=>array(),
                                            "5"=>array(),
                                            "6"=>array()
                                            ),                                
                            );

    
        foreach($result as $record){
            array_push($cookbookMap[$record["time"]][$record["day"]], $record);
        }




        $this->view->cookbooks = $cookbookMap;

    }
    
    public function getAction(){
        $this->_helper->viewRenderer->setNoRender(false);
        $ids = explode("_", $this->getRequest()->getParam("id"));
        $day = $ids[0];
        $time = $ids[1];
        $mapper = new Default_Model_CookbookMapper();
        $dishMapper = new Default_Model_DishMapper();
        $dishes = $dishMapper -> findAll();
        $result = $mapper -> findByDaytime($day,$time);
    	$this->view->lists = $result;
        $this->view->dishes = $dishes;
        $this->view->day = $day;
        $this->view->time = $time;
        //echo json_encode($result);
    }
    
    public function putAction(){

    }
    
    public function postAction(){
        $mapper = new Default_Model_CookbookMapper();
        $cookbook = new Default_Model_Cookbook();
        $cookbook->setOptions($this->getRequest()->getParams());
        $result = $mapper -> save($cookbook);
        if(is_numeric($result)){
            $this->getResponse()->setHttpResponseCode(201)->appendBody($result);
        }else{
            $this->getResponse()->setHttpResponseCode(422)->appendBody($result);
        }
    }
    
    public function deleteAction(){
        $mapper = new Default_Model_CookbookMapper();
        $result = $mapper -> delete($this->getRequest()->getParam("id"));
        if($result == "success"){
            $this->getResponse()->setHttpResponseCode(201)->appendBody("success");
        }else{
            $this->getResponse()->setHttpResponseCode(422)->appendBody($result);
        }
    }
    
    public function headAction(){
    	 
    }
    
   

}