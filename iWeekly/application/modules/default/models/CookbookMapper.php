<?php
class Default_Model_CookbookMapper {
	protected $_dbTable;
	
	public function setDbTable($dbTable)
	{
		if (is_string($dbTable)) {
			$dbTable = new $dbTable();
		}
		if (!$dbTable instanceof Zend_Db_Table_Abstract) {
			throw new Exception('Invalid table data gateway provided');
		}
		$this->_dbTable = $dbTable;
		return $this;
	}
	public function getDbTable(){
		if (null === $this->_dbTable) {
			$this->setDbTable('Default_Model_DbTable_Cookbook');
		}
		return $this->_dbTable;
	}
	public function save(Default_Model_Cookbook $cookbook){
		try{
			$result=$this->getDbTable()->insert($cookbook->toArray());
		}catch (Exception $e){
			return $e;
		}
		return (int)$result;
	}
	
	public function update(Default_Model_Cookbook $cookbook){
		if($cookbook->getId()){
			try{
				$result=$this->getDbTable()->update($cookbook->toArray(), "id=".$cookbook->getId());
			}catch(Exception $e){
				return "数据库有问题";
			}
		}else{
			return "没有找到该记录，无法更新";
		}
		return (int)$result;		
	}

	public function findSelf($id){
		try{
			$result = $this->getDbTable()->find($id)->current();
			return $result;
		}catch(Exception $e){
			return $e;
		}
	}

	
	public function findAll(){
		try{
			
			$select = $this->getDbTable()->select()
							->setIntegrityCheck(false)
							->from("dishes",array("dishes.name"))
							->from("cookbooks")
							->where("dishes.id = cookbooks.dish_id");
			
			$result = $this->getDbTable()->fetchAll($select)->toArray();
			return $result;
		}catch(Exception $e){
			return $e;
		}
	}
	
	public function findByDaytime($day,$time){
		try{
				
			$select = $this->getDbTable()->select()
							->setIntegrityCheck(false)
							->from("dishes",array("dishes.name"))
							->from("cookbooks",array(
																"cookbooks.id",
																"cookbooks.dish_id",
																"cookbooks.parameter"))
							->where("dishes.id = cookbooks.dish_id")
							->where("cookbooks.day = $day")
							->where("cookbooks.time = $time");
				
			$result = $this->getDbTable()->fetchAll($select)->toArray();
			return $result;
		}catch(Exception $e){
			return $e;
		}		
	}
	
	
	
	public function findStatistic($day=null){
		try{
			$select = $this->getDbTable()->select()
									->setIntegrityCheck(false)
									->from("dishes_materials",array())
									->joinLeft("dishes", "dishes.id = dishes_materials.dish_id",array())
									->joinLeft("materials", "materials.id = dishes_materials.material_id",array())
									->joinRight("cookbooks", "cookbooks.dish_id = dishes_materials.dish_id",
											array(
													"name"=>"materials.name",
													"sum_count"=>"round(sum(dishes_materials.count * cookbooks.parameter),2)",
													"sum_price"=>"round(sum(dishes_materials.count * cookbooks.parameter * materials.price),2)"
													))
									->group("dishes_materials.material_id");
			if(isset($day)){
				$select = $this->getDbTable()->select()
										->setIntegrityCheck(false)
										->from("dishes_materials",array())
										->joinLeft("dishes", "dishes.id = dishes_materials.dish_id",array())
										->joinLeft("materials", "materials.id = dishes_materials.material_id",array())
										->joinRight("cookbooks", "cookbooks.dish_id = dishes_materials.dish_id",
												array(
														"name"=>"materials.name",
														"sum_count"=>"round(sum(dishes_materials.count * cookbooks.parameter),2)",
														"sum_price"=>"round(sum(dishes_materials.count * cookbooks.parameter * materials.price),2)"
														))
										->where("cookbooks.day = $day")
										->group("dishes_materials.material_id");
			}
			$list = $this->getDbTable()->fetchAll($select)->toArray();
				
			
//   SQL:            SELECT m.price AS price, m.name AS material_name, sum( dm.count * c.parameter ) AS total_count, sum( dm.count * m.price * c.parameter ) AS sum
// 						FROM dishes_materials AS dm
// 						LEFT JOIN dishes AS d ON d.id = dm.dish_id
// 						LEFT JOIN materials AS m ON m.id = dm.material_id
// 						RIGHT JOIN cookbooks AS c ON c.dish_id = dm.dish_id
// 						GROUP BY dm.material_id
				
			return $list;
		}catch (Exception $e){
			return $e;
		}
	}
	
	
	public function delete($id=null){
		try{
			if(isset($id)){
				$result = $this->getDbTable()->find($id);
				$cookbook = $result->current();
				$cookbook->delete();
				return "success";
			}
			$result = $this->getDbTable()->delete();
			return "success";
		}catch(Exception $e){
			return $e;
		}
	}
	

	

	
}
