{~include file="header2.tpl"~}
<script src="/scripts/scrollpagination.js"></script>
<style>
	.fixed_container{
		width:270px;
		float:right;
	}
</style>
<script>
	var page=0;
	var total=1;
	var records=0;
	var done=0;
	var rate=0;
	
	$(function(){
		$.get("/user/gauge/index/id/"+$("#gid").val()+"/page/"+(page+1)).promise().done(function(data){
			$("#questions_container").append(data);
			$("#loading").bind("click",loadNext);
		});
	});
	
	function loadNext(){
		$("#loading").unbind();
		if(page>=total){
			$("#loading").addClass("display_off");
			$("#loaded").removeClass("display_off");
		}else{
			$.get("/user/gauge/index/id/"+$("#gid").val()+"/page/"+(page+1)).promise().done(function(data){
				$("#questions_container").append(data);
				if(page>=total){
					$("#loading").addClass("display_off");
					$("#loaded").removeClass("display_off");			
				}else{
					$("#loading").removeClass("disabled");
					$("#loading").bind("click",loadNext);					
				}
			});
		}		
	}
	
	function submitAnswer(){
		console.log("卧槽");
		$("#submit").button("loading");
		$("#submit").unbind();
		var doneCount=$("input[type='radio']:checked");
		if(doneCount.length<records){
			toastr.warning("您还有题目没有选择，请选择后再提交");
			return;
		}
		
		var answerArray=[];
		$("input[type='radio']:checked").each(function(){
			answerArray.push({id:$(this).data("id"),dimension_id:$(this).data("dimension"),value:$(this).val()});
		});
		
		$.post("/user/gauge/"+$("#gid").val(),{
			answers:answerArray
		}).promise().done(function(data,text,xhr){
			if(xhr.status==200){
				toastr.success("恭喜您，已经完成所有测试！您马上就能知晓结果！");
				setTimeout(window.location.href="/user/home/report",2000);				
			}else{
				toastr.success("恭喜您，本套测试已经完成！但您仍需要完成其他测试。");
				setTimeout(window.location.href="/user/home",2000);
			}
			//setTimeout(window.location.href="/user/home",2000);
		}).fail(function(data){
			$("#submit").button("reset");
			$("#submit").html("<i class='icon-ok'></i>提交答案");
			$("#submit").bind("click",submitAnswer);
			toastr.warning(data.responseText,"提示信息");
		});
	}

</script>
			<input type="hidden" value="{~$gauge->getGid()~}" id="gid">
			<div class="span9" style="margin-left:30px;">
				<h1 class="page-title">
					<i class="icon-smile 2x"></i>
					<span onclick="" style="cursor:pointer;">用户首页</span>
				</h1>

				<div id="scroller">
				
					<div class="widget  widget-table">		
						<div class="widget-header">
							<i class="icon-th-list"></i>
							<h3>{~$gauge->getGauge_name()~}</h3>
						</div> <!-- /widget-header -->
						<div class="widget-content">
							<br>
							<br>
							<div id="questions_container"></div>
							<br>
							<div id="loading" class="alert alert-info disabled" style="margin-left:50px;margin-right:50px;text-align:center;cursor:pointer;">
								<i class="icon-double-angle-down"></i>
								<i class="icon-double-angle-down"></i>
								<i class="icon-double-angle-down"></i>
								点击查看更多题目
								<i class="icon-double-angle-down"></i>
								<i class="icon-double-angle-down"></i>
								<i class="icon-double-angle-down"></i>
							</div>
							<div id="loaded" class="alert alert-success display_off" style="margin-left:50px;margin-right:50px;text-align:center;">已经加载完所有的题目</div>
						</div> <!-- /widget-content -->
					</div>

					
				</div>
			</div>
			<div class="fixed_container">
				<div style="position:fixed;width:270px;">
					<div class="account-container">
					
						<div class="account-details">
						
							<span class="account-name" style="font-size:25px;margin-bottom:10px;"><a class="label label-primary" style="font-size:14px;margin-right:10px;margin-left:30px;">姓名</a>{~$username~}</span>
							
							<span class="account-role" style="font-size:16px;margin-bottom:10px;"><a class="label label-primary" style="font-size:14px;margin-right:10px;">身份证号</a>{~$id_card~}</span>
							
							<span class="account-role" style="font-size:16px;">
								<a class="label label-important" style="font-size:14px;">请您仔细核对身份证以及姓名防止误答</a>
							</span>
						
						</div> <!-- /account-details -->
					
					</div> <!-- /account-container -->

					<hr>
					<div class="alert alert-info">
						您当前需要完成测试项：<span class="label label-warning" id="count"></span><br>
						您当前已经完成测试项：<span class="label label-warning" id="done">0</span><br>
						<strong>当前总进度<span class="label label-warning" id="percent">0%</span></strong>
					    <div class="progress progress-striped active progress-info" style="margin-bottom:0px;">
					    	<div id="progress" class="bar"></div>
					    </div>
						<div style="text-align:center;">
							已经加载 <span id="current_page">0</span> 页 / 总 <span id="total_page"></span> 页
						</div>
						<div id="submit"  class="btn btn-warning btn-large disabled" data-loading-text="正在提交请稍后..." style="width:190px;"><i class="icon-ok"></i>提交答案</div>
						<br>
					</div>	
					

					<hr>
					<div class="alert alert-success">
						<strong>引导语：</strong><br>
驾驶适性检测可以使其了解自身的心理和生理素质，便于其针对地进行心理训练，以配合技术训练，从而提高驾驶员训练质量，保证行车安全。

					</div>	
				</div>
			</div>
{~include file="footer.tpl"~}