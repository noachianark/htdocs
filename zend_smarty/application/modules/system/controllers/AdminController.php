<?php
class System_AdminController extends Zend_Rest_Controller
{

    public function init()
    {
    	$this->_helper->viewRenderer->setNoRender(true);
    	$this->view->admin_name=Zend_Auth::getInstance()->getIdentity()->username;
    	$this->view->admin_id=Zend_Auth::getInstance()->getIdentity()->id;
    	$this->view->title="系统管理";
    	
    }

    public function indexAction()
    {
    	$this->_helper->viewRenderer->setNoRender(false);
    }
    
    public function editAction(){
    	//echo md5(md5($this->getRequest()->getParam("oldpass")));
    	$oldpass=md5(md5($this->getRequest()->getParam("oldpass")));
    	if($oldpass==Zend_Auth::getInstance()->getIdentity()->password){
    	    	$admin=new System_Model_Administrator($this->getRequest()->getParams());
		    	$adminMapper=new System_Model_AdministratorMapper();
		    	$result=$adminMapper->update($admin);
		    	if(is_numeric($result)){
		    		$this->getResponse()->setHttpResponseCode(201)->appendBody("success");
		    	}else{
		    		$this->getResponse()->setHttpResponseCode(422)->appendBody($result);
		    	}	
    	}else{
    		$this->getResponse()->setHttpResponseCode(422)->appendBody("旧的密码不正确，无法更新");
    	}
    }
     
    public function newAction(){
    	$this->_helper->viewRenderer->setNoRender(false);
    }
    
    public function getAction(){
    	echo "get";
    }
    
    public function postAction(){
    	$admin=new System_Model_Administrator($this->getRequest()->getParams());
    	$adminMapper=new System_Model_AdministratorMapper();
    	$result=$adminMapper->save($admin);
    	if($result===true){
    		$this->getResponse()->setHttpResponseCode(201)->appendBody("success");
    	}else{
    		$this->getResponse()->setHttpResponseCode(422)->appendBody($result);
    	}
    }

    public function deleteAction(){
    	echo "delete";
    }
    
    public function putAction(){
    	$admin=new System_Model_Administrator($this->getRequest()->getParams());
    	$adminMapper=new System_Model_AdministratorMapper();
    	$result=$adminMapper->update($admin);
    	if(is_numeric($result)){
    		$this->getResponse()->setHttpResponseCode(201)->appendBody("success");
    	}else{
    		$this->getResponse()->setHttpResponseCode(422)->appendBody($result);
    	}		
    }
    
    public function headAction(){
    	 
    }

}