{~include file="header.tpl"~}
<script>
	$(function(){
		$("#profile_navi").addClass("active");
		
		$("#speed_btn").click(function(){
			$.post("/system/setting/1",$("#speed_form").serialize()).promise().done(function(data){
				toastr.success("保存成功");
				setTimeout(window.location.href="/system/profile/entry?user_id="+$("#user_id").val(),1000);
			}).fail(function(data){
				toastr.warning("保存失败"+data);
			});
		});
		
		$("#response_btn").click(function(){
			$.post("/system/setting/1",$("#response_form").serialize()).promise().done(function(data){
				toastr.success("保存成功");
				setTimeout(window.location.href="/system/profile/entry?user_id="+$("#user_id").val()+"#response",1000);
			}).fail(function(data){
				toastr.success("保存失败"+data);
			});			
		});
		
		$("#deep_btn").click(function(){
			$.post("/system/setting/1",$("#deep_form").serialize()).promise().done(function(data){
				window.location.href="/system/profile/entry?user_id="+$("#user_id").val()+"#deep";
			}).fail(function(data){
				
			});			
		});		

		// $("#hand_btn").click(function(){
		// 	$.post("/system/setting/1",$("#hand_form").serialize()).promise().done(function(data){
		// 		window.location.href="/system/profile/entry?user_id="+$("#user_id").val()+"#twohand";
		// 	}).fail(function(data){
				
		// 	});			
		// });
		$("#tab_navigation li:first").addClass("active");
		$(".tab-content div:first").addClass("active");

	});
	
		function saveSpeed(gid){
			var total=0;
			if($("input[name='speed_boundary_min']").val().length<1){
				toastr.warning("速度知觉边界判定值不能为空");
				return;
			}
			if($("input[name='speed_boundary_max']").val().length<1){
				toastr.warning("速度知觉边界判定值不能为空");
				return;
			}
			if(!validateRegexp($("input[name='speed_boundary_max']"),/^-?\d+\.?\d*$/,"速度知觉边界值只能填写数字")){
				return;
			}
			if(!validateRegexp($("input[name='speed_boundary_min']"),/^-?\d+\.?\d*$/,"速度知觉边界值只能填写数字")){
				return;
			}
			var validate=true;
			$(".speed_input").each(function(){
				if(!validateLength($(this),1,10,"速度知觉数据")){
					validate=false;
					return;
				}
				if(!validateRegexp($(this),/^-?\d+\.?\d*$/,"速度知觉只能填写数字")){
					validate=false;
					return;
				}
				total+=parseInt($(this).val());
			});
			if(!validate){
				return;
			}
			var score=(total/12).toFixed(2);
			var passed=false;
			var conclusion=$("#speed_unfort").val();
			if(score>=parseInt($("input[name='speed_boundary_min']").val()) && score<=parseInt($("input[name='speed_boundary_max']").val())){
				passed=true;
				conclusion=$("#speed_pass").val();
			}			
			$.post("/user/answer/"+$("#user_id").val(),{
				gid:1,
				name:"速度知觉测试",
				score:score,
				passed:passed,
				conclusion:conclusion,
				sex:"{~$sex~}"
			}).promise().done(function(data,status,xhr){
				
				if(xhr.status==201){
				 	$("#speed").remove();
				 	$("#speed_tab").remove();		
					$("#tab_navigation li:first").addClass("active");
					$(".tab-content div:first").addClass("active");								
				}else{
					//跳转
					window.location.href="/system/profile/index/page/{~$page~}";
				}

			}).fail(function(data){
			
			});			
		}
		
		function saveDeep(){
			var type="deep";
			var total=0;
			var tip="深度测试";
			if($("input[name='"+type+"_boundary']").val().length<1){
				toastr.warning(tip+"边界判定值不能为空");
				return;
			}
			if(!validateRegexp($("input[name='"+type+"_boundary']"),/^-?\d+\.?\d*$/,tip+"边界值只能填写数字")){
				return;
			}
			var validate=true;
			$("."+type+"_input").each(function(){
				if(!validateLength($(this),1,10,tip+"数据")){
					validate=false;
					return;
				}
				if(!validateRegexp($(this),/^-?\d+\.?\d*$/,tip+"只能填写数字")){
					validate=false;
					return;
				}
				total+=parseInt($(this).val());
			});

			if(!validate){
				return;
			}
			var score=(total/6).toFixed(2);
			var passed=false;
			var conclusion=$("#"+type+"_low").val();
			if(score<=parseInt($("input[name='"+type+"_boundary']").val())){
				passed=true;
				conclusion=$("#"+type+"_high").val();
			}
			$.post("/user/answer/"+$("#user_id").val(),{
				gid:3,
				name:"深度知觉测试",
				score:score,
				passed:passed,
				conclusion:conclusion,
				sex:"{~$sex~}"
			}).promise().done(function(data,status,xhr){
				console.log(status,"sdsd",data,"asd",xhr);
				if(xhr.status==201){
				 	$("#"+type).remove();
				 	$("#"+type+"_tab").remove();		
					$("#tab_navigation li:first").addClass("active");
					$(".tab-content div:first").addClass("active");								
				}else{
					//跳转
					window.location.href="/system/profile/index/page/{~$page~}";
				}

			}).fail(function(data){
			
			});
						
		}
		
		function saveResponse(){
			$(".response_boundary").each(function(){
				if($(this).val().length<1){
					toastr.warning("反应时的边界判定值不能为空");
					return;					
				}
				if(!validateRegexp($(this),/^-?\d+\.?\d*$/,"反应时的边界值只能填写数字")){
					return;
				}
			});
			
			//这里计算选择反应时的时间和次数
			
			var select_total=0;
			$(".select_time").each(function(){
				
				if(!validateLength($(this),1,10,"选择反应的数据")){
					validate=false;
					return;
				}
				if(!validateRegexp($(this),/^-?\d+\.?\d*$/,"选择反应的数据只能填写数字")){
					validate=false;
					return;
				}
				select_total+=parseInt($(this).val());				
			
			});
			var pass=true;
			if(select_total/4 <parseInt($("#response_time_min").val()) || select_total/4 >parseInt($("#response_time_max").val())){
				pass=false;
			}
			$(".select_right").each(function(){
				if(parseInt($(this).val())<parseInt($("#response_time_right").val())){
					pass=false;
				}
			});
			
			
			//这里计算四肢反应时的时间和次数
			var four_total=0;
			$(".four_time").each(function(){
				
				if(!validateLength($(this),1,10,"四肢反应数据")){
					validate=false;
					return;
				}
				if(!validateRegexp($(this),/^-?\d+\.?\d*$/,"四肢反应只能填写数字")){
					validate=false;
					return;
				}
				four_total+=parseInt($(this).val());				
			
			});
			if(four_total/4 < parseInt($("#response_four_min").val()) || four_total/4 >parseInt($("#response_four_max").val())){
				pass=false;
			}
			$(".four_right").each(function(){
				if(parseInt($(this).val())<parseInt($("#response_four_right").val())){
					pass=false;
				}
			});
			//开始传送数据
			var conclusion=$("#response_unfort").val();
			if(pass){
				conclusion=$("#response_pass").val();
			}
			$.post("/user/answer/"+$("#user_id").val(),{
				gid:2,
				name:"反应时测试",
				score:0,
				passed:pass,
				conclusion:conclusion,
				sex:"{~$sex~}"
			}).promise().done(function(data,status,xhr){
				console.log(status,"sdsd",data,"asd",xhr);
				if(xhr.status==201){
				 	$("#response").remove();
				 	$("#response_tab").remove();		
					$("#tab_navigation li:first").addClass("active");
					$(".tab-content div:first").addClass("active");								
				}else{
					//跳转
					window.location.href="/system/profile/index/page/{~$page~}";
				}

			}).fail(function(data){
			
			});
			

			
		}

		function saveSight(){
			var type = "sight";
			var tip = "夜视力和动视力";
			var validate=true;
			$(".sight_input").each(function(){
				if(!validateLength($(this),1,10,tip+"数据")){
					validate=false;
					return;
				}
				if(!validateRegexp($(this),/^-?\d+\.?\d*$/,tip+"只能填写数字")){
					validate=false;
					return;
				}
			});
			if(!validate){
				toastr.warning("测试数据必须填写数字");
				return;
			}

			var conclusion = "裸视力："+$(".sight_general").val()+"\n矫正视力："+$(".sight_fix").val()+"\n夜视力："+$(".sight_night").val()+"\n动视力："+$(".sight_move").val();


			console.log(console.log("啦啦啦"));
			var score = 0;
			var passed = false;
			if(Number($(".sight_night").val()) < 60 && Number($(".sight_move").val()) > 0.1 ){
				passed = true;
			}

			$.post("/user/answer/"+$("#user_id").val(),{
				gid:4,
				name:"夜视力和动视力",
				score:score,
				passed:passed,
				conclusion:conclusion,
				sex:"{~$sex~}"
			}).promise().done(function(data,status,xhr){
				if(xhr.status==201){
				 	$("#"+type).remove();
				 	$("#"+type+"_tab").remove();		
					$("#tab_navigation li:first").addClass("active");
					$(".tab-content div:first").addClass("active");								
				}else{
					//跳转
					window.location.href="/system/profile/index/page/{~$page~}";
				}

			}).fail(function(data){
			
			});


		}
		
		// function saveHand(){
		// 	var type="hand";
		// 	var total=0;
		// 	var tip="双手调节器测试";
		// 	$(".hand_boundary").each(function(){
		// 		if($(this).val().length<1){
		// 			toastr.warning(tip+"边界判定值不能为空");
		// 			return;
		// 		}
		// 		if(!validateRegexp($(this),/^-?\d+\.?\d*$/,tip+"边界值只能填写数字")){
		// 			toastr.warning(tip+"边界值只能填写数字");
		// 			return;
		// 		}
		// 	});

		// 	var validate=true;
		// 	$(".hand_input").each(function(){
		// 		if(!validateLength($(this),1,10,tip+"数据")){
		// 			validate=false;
		// 			return;
		// 		}
		// 		if(!validateRegexp($(this),/^-?\d+\.?\d*$/,tip+"只能填写数字")){
		// 			validate=false;
		// 			return;
		// 		}
		// 	});


		// 	$(".hand2_input").each(function(){
		// 		if(!validateLength($(this),1,10,tip+"数据")){
		// 			validate=false;
		// 			return;
		// 		}
		// 		if(!validateRegexp($(this),/^-?\d+\.?\d*$/,tip+"只能填写数字")){
		// 			validate=false;
		// 			return;
		// 		}
		// 	});

		// 	if(!validate){
		// 		toastr.warning("测试数据必须填写数字");
		// 		return;
		// 	}
			
			
		// 	var passed=true;
		// 	$(".hand_input_time").each(function(){
		// 		if(parseInt($(this).val())>parseInt($("#hand_time").val())){
		// 			passed=false;
		// 		}
		// 	});
		// 	$(".hand_input_count").each(function(){
		// 		if(parseInt($(this).val()>parseInt($("#hand_count").val()))){
		// 			passed=false;
		// 		}
		// 	});

		// 	$(".hand2_input_time").each(function(){
		// 		if(parseInt($(this).val())>parseInt($("#hand2_time").val())){
		// 			passed=false;
		// 		}
		// 	});
		// 	$(".hand2_input_count").each(function(){
		// 		if(parseInt($(this).val()>parseInt($("#hand2_count").val()))){
		// 			passed=false;
		// 		}
		// 	});

		// 	var score=0;
		// 	var conclusion=$("#hand_unfort").val();
		// 	if(passed){
		// 		conclusion=$("#hand_pass").val();
		// 	}
		// 	$.post("/user/answer/"+$("#user_id").val(),{
		// 		gid:4,
		// 		name:"双手调节器",
		// 		score:score,
		// 		passed:passed,
		// 		conclusion:conclusion,
		// 		sex:"{~$sex~}"
		// 	}).promise().done(function(data,status,xhr){
		// 		if(xhr.status==201){
		// 		 	$("#"+type).remove();
		// 		 	$("#"+type+"_tab").remove();		
		// 			$("#tab_navigation li:first").addClass("active");
		// 			$(".tab-content div:first").addClass("active");								
		// 		}else{
		// 			//跳转
		// 			window.location.href="/system/profile/index/page/{~$page~}";
		// 		}

		// 	}).fail(function(data){
			
		// 	});
			
		// }

		
	
	
</script>
<style>
	.speed_input{
		width:120px;
	}
</style>

<input type="hidden" value="{~$user_id~}" id="user_id">
<h1 class="page-title">
	<i class="icon-pushpin"></i>
	<span style="cursor:pointer;" onclick="window.location.href='/system/profile'">档案管理->录入仪器数据</span>
</h1>

<div class="widget">
		
		<div class="widget-header">
			<h3 style="margin-top:10px;">
					录入仪器数据
			</h3>
			
		</div> <!-- /widget-header -->	
		
		<div class="widget-content">

			<div class="tabbable">
				<ul class="nav nav-tabs" id="tab_navigation">
					{~if $result->speed eq false~}
					<li id="speed_tab" class="active"><a data-toggle="tab" href="#speed">速度知觉</a></li>
					{~/if~}
					{~if $result->response eq false~}
					<li id="response_tab"><a data-toggle="tab" href="#response">反应时</a></li>
					{~/if~}
					{~if $result->deep eq false~}
					<li id="deep_tab"><a data-toggle="tab" href="#deep">深度知觉</a></li>
					{~/if~}
					<!-- {~if $result->hand eq false~}
					<li id="hand_tab"><a data-toggle="tab" href="#twohand">双手调节器</a></li>
					{~/if~} -->
					{~if $result->sight eq false~}
					<li id="sight_tab"><a data-toggle="tab" href="#sight">夜视力和动视力</a></li>
					{~/if~}
				</ul>	
				
				<div class="tab-content">
					{~if $result->speed eq false~}
					<div id="speed" class="tab-pane active">
						<table class="table table-striped table-bordered table-condensed" style="text-align:center;vertical-align:middle;">
							<thead>
						          <tr>
						          	<th>秒</th>
						            <th>快远</th>
						            <th>顺序</th>
						            <th>快近</th>
						            <th>顺序</th>
									<th>慢远</th>
									<th>顺序</th>
									<th>慢近</th>
									<th>顺序</th>
						          </tr>
						     </thead>
						        
							 <tbody>
							 	  <tr>
							 	  	<th rowspan="6"><label style="width:20px;">误差时间</label></th> 
							 	  </tr>
						          <tr>
						            <td>1</td>
						            <td><input class="speed_input"></td>
						            <td>3</td>
						            <td><input class="speed_input"></td>
						            <td>5</td>
						            <td><input class="speed_input"></td>	
						            <td>7</td>
						            <td><input class="speed_input"></td>																	
						          </tr>
						          <tr>
						            <td>2</td>
						            <td><input class="speed_input"></td>
						            <td>4</td>
						            <td><input class="speed_input"></td>
						            <td>6</td>
						            <td><input class="speed_input"></td>	
						            <td>8</td>
						            <td><input class="speed_input"></td>																	
						          </tr>								  
						          <tr>
						            <td>9</td>
						            <td><input class="speed_input"></td>
						            <td>10</td>
						            <td><input class="speed_input"></td>
						            <td>11</td>
						            <td><input class="speed_input"></td>	
						            <td>12</td>
						            <td><input class="speed_input"></td>																	
						          </tr>	
						          								  								  
						    </tbody>
						 </table>
						 <div id="speed_save" class="btn btn-info btn-large" onclick="saveSpeed(1)">保存数据</div>
						 <hr>
						 
						 <form class="form-horizontal" id="speed_form">
							<div class="control-group">
								<label for="sequence" class="control-label"><strong style="color:#468847">最小界限</strong></label>
								<div class="controls">
					               <input name="speed_boundary_min" type="text" class="span" value="{~$setting->getSpeed_boundary_min()~}">										
								</div>
							</div>
							
							<div class="control-group">
								<label for="sequence" class="control-label"><strong style="color:#468847">最大界限</strong></label>
								<div class="controls">
					               <input name="speed_boundary_max" type="text" class="span" value="{~$setting->getSpeed_boundary_max()~}">										
								</div>
							</div>

							<div class="control-group">
								<label for="sequence" class="control-label"><strong style="color:#468847">适合说明</strong></label>
								<div class="controls">
					               <textarea id="speed_pass" name="speed_pass" class="span6">{~$setting->getSpeed_pass()~}</textarea>										
								</div>
							</div>
							<div class="control-group">
								<label for="sequence" class="control-label"><strong style="color:#468847">不适合说明</strong></label>
								<div class="controls">
					               <textarea id="speed_unfort" name="speed_unfort" class="span6">{~$setting->getSpeed_unfort()~}</textarea>										
								</div>
							</div>
							<div class="control-group">
								<label for="sequence" class="control-label"><strong style="color:#468847"></strong></label>
								<div class="controls">
					               	<a class="btn btn-warning" id="speed_btn" >更新评测设置</a>										
								</div>
							</div>																
						</form>
						
					
																		
						</div>
						{~/if~}

						 				
					
					{~if $result->response eq false~}
					<div id="response" class="tab-pane">
						<table class="table table-striped table-bordered table-condensed" style="text-align:center;vertical-align:middle;">
							<thead>
						          <tr>
						          	<th>类型</th>
						            <th>色光刺激</th>
						            <th>平均反应时(ms)</th>
						            <th>正确次数(次)</th>
						            <!-- <th>延迟反应次数(次)</th> -->
						          </tr>
						     </thead>
						        
							 <tbody>
							 	  <tr>
							 	  	<th rowspan="5"><label style="width:20px;">选择反应时任务</label></th> 
							 	  </tr>
						          <tr>
						            <td>红</td>
						            <td><input class="span response_input select_time"></td>
						            <td><input class="span response_input select_right"></td>
<!-- 						            <td><input class="span response_input select_delay"></td>	
 -->					          	  </tr>
						          <tr>
						            <td>黄</td>
						            <td><input class="span response_input select_time"></td>
						            <td><input class="span response_input select_right"></td>
<!-- 						            <td><input class="span response_input select_delay"></td>	
 -->					          	  </tr>
						          <tr>
						            <td>绿</td>
						            <td><input class="span response_input select_time"></td>
						            <td><input class="span response_input select_right"></td>
<!-- 						            <td><input class="span response_input select_delay"></td>	
 -->					          	  </tr>
						          <tr>
						            <td>蓝</td>
						            <td><input class="span response_input select_time"></td>
						            <td><input class="span response_input select_right"></td>
<!-- 						            <td><input class="span response_input select_delay"></td>	
 -->					          	  </tr>
							 	  <tr>
							 	  	<th rowspan="5"><label style="width:20px;">四肢反应时任务</label></th> 
							 	  </tr>
						          <tr>
						            <td>红--左手</td>
						            <td><input class="span response_input four_time"></td>
						            <td><input class="span response_input four_right"></td>
<!-- 						            <td><input class="span response_input four_delay"></td>	
 -->					          	  </tr>
						          <tr>
						            <td>黄--右手</td>
						            <td><input class="span response_input four_time"></td>
						            <td><input class="span response_input four_right"></td>
<!-- 						            <td><input class="span response_input four_delay"></td>	
 -->					          	  </tr>
						          <tr>
						            <td>绿--左脚</td>
						            <td><input class="span response_input four_time"></td>
						            <td><input class="span response_input four_right"></td>
<!-- 						            <td><input class="span response_input four_delay"></td>	
 -->					          	  </tr>
						          <tr>
						            <td>蓝--右脚</td>
						            <td><input class="span response_input four_time"></td>
						            <td><input class="span response_input four_right"></td>
						           <!--  <td><input class="span response_input four_delay" ></td>	 -->
					          	  </tr>								  								  							  								  						          						  								  
						    </tbody>
					
						 </table>
						 <div id="response_save" class="btn btn-info btn-large" onclick="saveResponse()">保存数据</div>
						 <hr>
						 <form id="response_form" class="form-horizontal">
							<div class="control-group">
								<label for="sequence" class="control-label"><strong style="color:#468847">选择反应时的区间</strong></label>
								<div class="controls">
					               大于等于<input id="response_time_min" name="response_time_min" type="text" class="span" value="{~$setting->getResponse_time_min()~}">（ms）
								   小于等于<input id="response_time_max" name="response_time_max" type="text" class="span" value="{~$setting->getResponse_time_max()~}">	（ms）									
								</div>
							</div>
							<div class="control-group">
								<label for="sequence" class="control-label"><strong style="color:#468847">选择反应时正确次数</strong></label>
								<div class="controls">
					              	大于等于<input id="response_time_right" name="response_time_right" type="text" class="span response_boundary" value="{~$setting->getResponse_time_right()~}">（次）								
								</div>
							</div>												
							<div class="control-group">
								<label for="sequence" class="control-label"><strong style="color:#468847">四肢反应时的区间</strong></label>
								<div class="controls">
					              	大于等于<input id="response_time_min" name="response_four_min" type="text" class="span  response_boundary" value="{~$setting->getResponse_four_min()~}">（ms）
								   小于等于<input id="response_time_max" name="response_four_max" type="text" class="span  response_boundary" value="{~$setting->getResponse_four_max()~}">	（ms）									
								</div>
							</div>
							<div class="control-group">
								<label for="sequence" class="control-label"><strong style="color:#468847">四肢反应时正确次数</strong></label>
								<div class="controls">
					              	大于等于<input id="response_four_right" name="response_four_right" type="text" class="span  response_boundary" value="{~$setting->getResponse_four_right()~}">（次）								
								</div>
							</div>		

								
							<div class="control-group">
								<label for="sequence" class="control-label"><strong style="color:#468847">适合说明</strong></label>
								<div class="controls">
					               <textarea id="response_pass" name="response_pass" class="span6">{~$setting->getResponse_pass()~}</textarea>										
								</div>
							</div>
							<div class="control-group">
								<label for="sequence" class="control-label"><strong style="color:#468847">不适合说明</strong></label>
								<div class="controls">
					               <textarea id="response_unfort" name="response_unfort" class="span6">{~$setting->getResponse_unfort()~}</textarea>										
								</div>
							</div>
							<div class="control-group">
								<label for="sequence" class="control-label"><strong style="color:#468847"></strong></label>
								<div class="controls">
					               	<a id="response_btn" class="btn btn-warning">更新评测设置</a>										
								</div>
							</div>							
																		
						</form>			 
						 
						 
					</div>
					{~/if~}
					{~if $result->deep eq false~}
					<div id="deep" class="tab-pane">
						
							<table class="table table-striped table-bordered table-condensed" style="text-align:center;vertical-align:middle;">
								<thead>
							          <tr>
							          	<th></th>
							            <th colspan="1">近调整（近--远）</th>
							            <th colspan="1">远调整（远--近）</th>
							          </tr>
							     </thead>
							        
								 <tbody>
								 	  <th rowspan="2">次数</th>
									  <tr>
								 		<!-- <td>原始误差数据cm</td> -->
										<td>误差绝对值cm</td>
										<!-- <td>原始误差数据cm</td> -->
										<td>误差绝对值cm</td>
								 	  </tr>
							          <tr>
							            <td>1</td>
<!-- 							            <td><input class="span deep_input" style="width:120px;"></td>
 -->							            <td><input class="span deep_input" style="width:120px;"></td>
<!-- 										<td><input class="span deep_input" style="width:120px;"></td>	
 -->										<td><input class="span deep_input" style="width:120px;"></td>	
						          	  </tr>
									  <tr>
							            <td>2</td>
<!-- 							            <td><input class="span deep_input" style="width:120px;"></td>
 -->							            <td><input class="span deep_input" style="width:120px;"></td>
<!-- 										<td><input class="span deep_input" style="width:120px;"></td>	
 -->										<td><input class="span deep_input" style="width:120px;"></td>	
						          	  </tr>
									  <tr>
							            <td>3</td>
<!-- 							            <td><input class="span deep_input" style="width:120px;"></td>
 -->							            <td><input class="span deep_input" style="width:120px;"></td>
<!-- 										<td><input class="span deep_input" style="width:120px;"></td>	
 -->										<td><input class="span deep_input" style="width:120px;"></td>	
						          	  </tr>
									  			          	  
							    </tbody>
							 </table>
						 <div id="deep_save" class="btn btn-info btn-large" onclick="saveDeep()">保存数据</div>
						 <hr>							 
							 <form id="deep_form" class="form-horizontal">
								<div class="control-group">
									<label for="sequence" class="control-label"><strong style="color:#468847">适合界限（≤）</strong></label>
									<div class="controls">
						               <input id="deep_boundary" name="deep_boundary" type="text" class="span" value="{~$setting->getDeep_boundary()~}">										
									</div>
								</div>
								<div class="control-group">
									<label for="sequence" class="control-label"><strong style="color:#468847">适合说明</strong></label>
									<div class="controls">
						               <textarea id="deep_high" name="deep_high" class="span6">{~$setting->getDeep_high()~}</textarea>										
									</div>
								</div>
								<div class="control-group">
									<label for="sequence" class="control-label"><strong style="color:#468847">不适合说明</strong></label>
									<div class="controls">
						               <textarea id="deep_low" name="deep_low" class="span6">{~$setting->getDeep_low()~}</textarea>										
									</div>
								</div>
								<div class="control-group">
									<label for="sequence" class="control-label"><strong style="color:#468847"></strong></label>
									<div class="controls">
						               	<a id="deep_btn" class="btn btn-warning">更新评测设置</a>										
									</div>
								</div>							
																			
							</form>							
					</div>
					{~/if~}


					{~if $result->sight eq false~}

					<div id="sight" class="tab-pane">
						<table class="table table-striped table-bordered table-condensed" style="text-align: center;vertical-align:middle;">
							<thead>
						          <tr>
						          	<th>裸视力</th>
						            <th>矫正视力</th>
						            <th>夜视力</th>
						            <th>动视力</th>
						          </tr>
						     </thead>
						     <tbody>
						     	<tr>
						     		<td><input style="width:120px" class="sight_input span sight_general"></td>
						     		<td><input style="width:120px" class="sight_input span sight_fix"></td>
						     		<td><input style="width:120px" class="sight_input span sight_night"></td>
						     		<td><input style="width:120px" class="sight_input span sight_move"></td>
						     	</tr>
						     </tbody>
						</table>
						<div class="btn btn-info btn-large" onclick="saveSight()">保存数据</div>
					</div>

					{~/if~}
					
					
					
					<!-- {~if $result->hand eq false~}
					<div id="twohand" class="tab-pane">
						<table class="table table-striped table-bordered table-condensed" style="text-align:center;vertical-align:middle;">
							<thead>
						          <tr>
						          	<th>方向</th>
						            <th>次数</th>
						            <th>时间(ms)</th>
						            <th>错误次数(次)</th>
						          </tr>
						     </thead>
						        
							 <tbody>

						        <tr>
						            <td>顺时针</td>
						            <td>1</td>
						            <td><input class="span hand_input hand_input_time"></td>
						            <td><input class="span hand_input hand_input_count"></td>	
					          	</tr>
						        <tr>
						            <td>逆时针</td>
						            <td>2</td>
						            <td><input class="span hand2_input hand2_input_time"></td>
						            <td><input class="span hand2_input hand2_input_count"></td>	
					          	</tr>						      					          						          						  								  							  								  						          						  								  
						    </tbody>
						 </table>
						 <div id="hand" class="btn btn-info btn-large" onclick="saveHand()">保存数据</div>
						 <hr>						 
						 <form id="hand_form" class="form-horizontal">
							<div class="control-group">
								<label for="sequence" class="control-label"><strong style="color:#468847">顺时针时间范围小于等于</strong></label>
								<div class="controls">
					               <input id="hand_time" name="hand_time" type="text" class="span" value="{~$setting->getHand_time()~}">										
								</div>
							</div>
							<div class="control-group">
								<label for="sequence" class="control-label"><strong style="color:#468847">顺时针错误次数小于等于</strong></label>
								<div class="controls">
					               <input id="hand_count" name="hand_count" type="text" class="span" value="{~$setting->getHand_count()~}">										
								</div>
							</div>							
							<div class="control-group">
								<label for="sequence" class="control-label"><strong style="color:#468847">逆时针时间范围小于等于</strong></label>
								<div class="controls">
					               <input id="hand2_time" name="hand2_time" type="text" class="span" value="{~$setting->getHand2_time()~}">										
								</div>
							</div>
							<div class="control-group">
								<label for="sequence" class="control-label"><strong style="color:#468847">逆时针错误次数小于等于</strong></label>
								<div class="controls">
					               <input id="hand2_count" name="hand2_count" type="text" class="span" value="{~$setting->getHand2_count()~}">										
								</div>
							</div>	
							<div class="control-group">
								<label for="sequence" class="control-label"><strong style="color:#468847">合适说明</strong></label>
								<div class="controls">
					               <textarea id="hand_pass" name="hand_pass" class="span6">{~$setting->getHand_pass()~}</textarea>										
								</div>
							</div>
							<div class="control-group">
								<label for="sequence" class="control-label"><strong style="color:#468847">不适合说明</strong></label>
								<div class="controls">
					               <textarea id="hand_unfort" name="hand_unfort" class="span6">{~$setting->getHand_unfort()~}</textarea>										
								</div>
							</div>
							<div class="control-group">
								<label for="sequence" class="control-label"><strong style="color:#468847"></strong></label>
								<div class="controls">
					               	<a id="hand_btn" class="btn btn-warning">更新评测设置</a>										
								</div>
							</div>							
																		
						</form>			
					</div>	
					{~/if~}		 -->												
				</div>
				
				<hr>
				
				
			</div>
			
		</div>
		
</div>



{~include file="footer.tpl"~}