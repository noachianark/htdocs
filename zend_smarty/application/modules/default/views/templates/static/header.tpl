<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>{~$title~}</title>
<link rel="stylesheet" type="text/css" href="/css/toastr.min.css"/>
<link rel="stylesheet" type="text/css" href="/css/global.css"/>
<link href="/css/bootstrap.min.css" rel="stylesheet" />
<link href="/css/bootstrap-responsive.min.css" rel="stylesheet" />

<link href="/css/fonts.css" rel="stylesheet" />
<link href="/css/font-awesome.css" rel="stylesheet" />

<link href="/css/adminia.css" rel="stylesheet" /> 
<link href="/css/adminia-responsive.css" rel="stylesheet" /> 
<script src="/scripts/jquery-1.8.3.js"></script>
    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

<!--全局设定 -->
<script>
	
	function checkLength( o, n, min, max ) {
        if ( o.val().length > max || o.val().length < min ) {
            o.addClass( "ui-state-error" );
            updateTips( n + " 的长度必须在 " +
                min + " 到 " + max + "之间." );
            return false;
        } else {
            return true;
        }
    }
	
    function checkRegexp( o, regexp, n ) {
        if ( !( regexp.test( o.val() ) ) ) {
            o.addClass( "ui-state-error" );
            updateTips( n );
            return false;
        } else {
            return true;
        }
    }
	
	function checkEquals(value1,value2,o,n){
		if(value1.val()!=value2.val()){
			o.addClass("ui-state-error");
			updateTips(n);
			return false;
		}else{
			return true;
		}
	}
	
	$(function() {
        $('button').button();
		toastr.options={
		  "debug": false,
		  "positionClass": "toast-top-right",
		  "onclick": null,
		  "fadeIn": 300,
		  "fadeOut": 1000,
		  "timeOut": 4000,
		  "extendedTimeOut": 1000
		}

    });
    
    jQuery.fn.center = function () {
        this.css("position","absolute");
        this.css("top", ( $(window).height() - this.height() ) / 2+$(window).scrollTop() + "px");
        this.css("left", ( $(window).width() - this.width() ) / 2+$(window).scrollLeft() + "px");
        return this;
    }

	
</script>
</head>