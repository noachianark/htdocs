<?php
require_once 'Zend/Db/Table/Abstract.php';
class System_Model_DbTable_Category extends Zend_Db_Table_Abstract{
	protected $_name="category";
	protected $_primary="cat_id";
}