{~foreach $cookbooks as $cookbook~}
<tr>
	<td style="width:40px;"><span class="td_titile" data-time="{~$cookbook@key~}"></span></td>
	{~foreach $cookbook as $cooks~}
		<td class="item_td">
			<div class="items" data-day="{~$cooks@key~}" data-time="{~$cookbook@key~}">
				{~foreach $cooks as $cook~}
					<dd>{~$cook['name']~}</dd>
				{~/foreach~}				
			</div>
		</td>
	{~/foreach~}
</tr>
{~/foreach~}
<style type="text/css">
	.items{
		height:auto !important;
		height:60px;
		min-height:60px;
	}
	.item_td:hover{
		background-color: #8E44AD;
		cursor: pointer;
		color: #ffffff;
		-webkit-transition: background  0.7s ease;
		-moz-transition: background  0.7s ease;
		-ms-transition: background  0.7s ease;
		-o-transition: background  0.7s ease;
		transition: background  0.7s ease;
		
	}
	.item_td{
		-webkit-transition: background  0.7s ease;
		-moz-transition: background  0.7s ease;
		-ms-transition: background  0.7s ease;
		-o-transition: background  0.7s ease;
		transition: background  0.7s ease;
	}

</style>
<script type="text/javascript">
	$(function(){
		$(".items").on("click",function(){
			window.location.href = "/cookbook/"+$(this).data("day")+"_"+$(this).data("time");
		});
		$(".td_titile").each(function(){
			$(this).text(timeArr[$(this).data("time")]);
		});
	});
</script>