<?php
class Default_Model_DishMapper {
	protected $_dbTable;
	
	public function setDbTable($dbTable)
	{
		if (is_string($dbTable)) {
			$dbTable = new $dbTable();
		}
		if (!$dbTable instanceof Zend_Db_Table_Abstract) {
			throw new Exception('Invalid table data gateway provided');
		}
		$this->_dbTable = $dbTable;
		return $this;
	}
	public function getDbTable(){
		if (null === $this->_dbTable) {
			$this->setDbTable('Default_Model_DbTable_Dish');
		}
		return $this->_dbTable;
	}
	public function save(Default_Model_Dish $dish){
		try{
			$result=$this->getDbTable()->insert($dish->toArray());
		}catch (Exception $e){
			return json_encode($dish);
		}
		return (int)$result;
	}
	public function update(Default_Model_Dish $dish){
		if($dish->getId()){
			try{
				$result=$this->getDbTable()->update($dish->toArray(), "id=".$dish->getId());
			}catch(Exception $e){
				return "数据格式不正确";
			}
		}else{
			return "没有找到该记录，无法更新";
		}
		return (int)$result;		
	}
	
	public function findByDayTime($day,$time){
		try{
			$select=$this->getDbTable()->select()->where("day = $day")->where("time = $time");
			$data = $this->getDbTable()->fetchAll($select)->toArray();
			return $data;
		}catch(Exception $e){
			return $e;
		}

	}
	
	public function findSelf($id){
		try{
			$select = $this -> getDbTable() -> find($id) -> current();
			$obj = $select->toArray();
			return $obj;
		}catch(Exception $e){
			return $e;
		}
	}
	
	public function findById($id){
		try{
			//$select = $this -> getDbTable() -> find($id) -> current();
			//$list = $select -> findManyToManyRowset("Default_Model_DbTable_Material", "Default_Model_DbTable_DishMaterial",null,null)->toArray();
			
			
			//$select = $this->getDbTable()->select();
			$select = $this->getDbTable()->select()
							->setIntegrityCheck(false)
							->from("dishes_materials")->where("dish_id = $id")
							->joinLeft("dishes", "dishes.id = dishes_materials.dish_id",array())
							->joinLeft("materials", "materials.id = dishes_materials.material_id",array("materials.name","materials.price"));
			
			
			$list = $this->getDbTable()->fetchAll($select)->toArray();
			
			
			return $list;
		}catch(Exception $e){
			return $e;
		}
	}
	
	
	public function findAll(){
		try{
			$result = $this->getDbTable()->fetchAll()->toArray();
			return $result;
		}catch(Exception $e){
			return $e;
		}
	}
	
	
	public function delete($id){
		try{
			$result = $this->getDbTable()->find($id);
			$dish = $result->current();
			$dish->delete();
			return "success";
		}catch(Exception $e){
			return $e;
		}
	}
	
	public function findMaterial($id){
		try{
			$rowset = $this->getDbTable()->find($id);
			$dish = $rowset->current();
			$result = $dish->findDependentRowset("Default_Model_DbTable_DishMaterial");
			$cls = array("dish"=>$dish->toArray(),"materials"=>$result->toArray());
			 return $cls;
		}catch(Exception $e){
			return $e;
		}
	}

	

	
}
