{~include file="header.tpl"~}
<script>
	$(function(){
		$("#gauge_navi").addClass("active");
	});
	function publishGauge(){

			$.post("/system/gauge/"+$("#need_for_operate").val(),{gid:$("#need_for_operate").val(),is_display:1}).done(function(data){
				$('#publishDialog').modal('hide');
				toastr.success("发布成功，现在受测者可以进行该量表的测试。");
				setTimeout(function(){
					window.location.href="/system/gauge";					
				},1500);
			}).fail(function(data){
				console.log(data.responseText);
			});					


	}
	function removeGauge(){

			$.post("/system/gauge/"+$("#need_for_operate").val(),{gid:$("#need_for_operate").val(),is_display:0}).done(function(data){
				$('#removeDialog').modal('hide');
				toastr.success("撤销发布成功，现在受测者不可以进行该量表的测试。");
				setTimeout(function(){
					window.location.href="/system/gauge";					
				},1500);
			}).fail(function(data){
				console.log(data.responseText);
			});					

	}
	function confirmDelete(){
		$.ajax({url:"/system/gauge/"+$("#need_for_operate").val(),type:"DELETE"}).done(function(data){
			$('#deleteDialog').modal('hide');
			toastr.success("删除成功，您再也无法查看到之前的数据了。");
			setTimeout(function(){
				window.location.href="/system/gauge";					
			},1500);
		}).fail(function(data){
			console.log(data.responseText);
		});			
	}
</script>
<link rel="stylesheet" type="text/css" href="/css/dashboard.css"/>

	<h1 class="page-title">
		<i class="icon-th-list"></i>
		<span style="cursor:pointer;" onclick="window.location.href='/system'">量表管理</span>
	</h1>
	<div class="stat-container">
										
			<div class="stat-holder">						
				<div class="stat">							
					<span>{~$dash->finish~}人</span>							
					进行了测试
				</div> <!-- /stat -->						
			</div> <!-- /stat-holder -->
			 
			<div class="stat-holder">						
				<div class="stat">							
					<span>{~$dash->passed~}人</span>							
					通过了测试
				</div> <!-- /stat -->						
			</div> <!-- /stat-holder -->
			
			<div class="stat-holder">						
				<div class="stat">
					{~if $dash->finish eq 0 ~}
						<span>--</span>
					{~else~}
						<span>{~round($dash->passed / $dash->finish,2)*100~}%</span>		
					{~/if~}						
					通过率
				</div> <!-- /stat -->						
			</div> <!-- /stat-holder -->
			
			<div class="stat-holder">						
				<div class="stat">							
					<span>{~$dash->count~}道</span>							
					测试试题
				</div> <!-- /stat -->						
			</div> <!-- /stat-holder -->
					
	</div>
	<!--<div class="pull-left" style="margin-bottom: 1.25em;">
		<button class="btn btn-warning btn-large" type="submit" onclick="window.location.href='/system/gauge/new'"> 添加量表 </button>
	</div>-->
	<a class="btn btn-large btn-success" href="/system/gauge/new"  style="margin-bottom: 1.25em;">
		<i class="icon-plus icon-large pull-left" style="line-height:inherit;margin-top:0px;margin-right:3px;"></i>
		新建量表	
	</a>
	<div class="widget  widget-table">		
		<div class="widget-header">
			<i class="icon-th-list"></i>
			<h3>量表列表</h3>
			<div class="pull-right" style="margin-right:15px;">
			{~if ($gauges->page > 1) and ($gauges->page < $gauges->total) ~}
				<a href="/system/gauge/index/page/{~$gauges->page-1~}">上一页</a> | <a href="/system/gauge/index/page/{~$gauges->page+1~}">下一页</a> |
			{~elseif $gauges->page eq 1 ~}
				<a href="/system/gauge/index/page/{~$gauges->page+1~}">下一页</a> |
			{~else~}
				<a href="/system/gauge/index/page/{~$gauges->page-1~}">上一页</a> |
			{~/if~}
				总页数：{~$gauges->total~} | 当前页：{~$gauges->page~}
			</div>
		</div> <!-- /widget-header -->
		<div class="widget-content">					
			<table class="table table-striped table-bordered">
				<thead>
					<th>id号</th>
					<th>量表名称</th>
					<th>描述</th>
					<th>是否显示</th>
					<th> </th>
				</thead>
				<tbody>
					{~foreach $gauges->rows as $gauge~}
						<tr>
							<td style="width:30px;">{~$gauge['gid']~}</td>
							<td>{~$gauge['gauge_name']~}</td>
							<td style="width:300px;">
								{~if mb_strlen($gauge['description'],"UTF8")<22~}
									{~$gauge['description']~}
								{~else~}
									<span data-toggle="tooltip" title="{~$gauge['description']~}">{~mb_substr($gauge['description'],0,22,"UTF8")~}...</span>									
								{~/if~}
							</td>
							<td style="width:65px;">
							{~if $gauge['is_display'] eq 0~}
								否
							{~else~}
								是
							{~/if~}
							</td>
							<td class="action-td" style="width:140px;">
							
								{~if $gauge['manual'] eq 1~}
									<input type="hidden" value="{~$gauge['gid']~}" class="gid">
									{~if $gauge['is_display'] eq 0~}
									<a onclick="$('#need_for_operate').val($('.gid',$(this).parent()).val())"  class="btn btn-success"  title="发布量表" data-toggle="modal" href="#publishDialog"  data-keyboard="false" data-backdrop="false">
										<i class="icon-play-sign"></i>						
									</a>
									{~else~}
									<a onclick="$('#need_for_operate').val($('.gid',$(this).parent()).val())"  class="btn btn-danger"  title="撤销发布"  data-toggle="modal" href="#removeDialog"  data-keyboard="false" data-backdrop="false">
										<i class="icon-minus-sign-alt"></i>						
									</a>
									{~/if~}
									
									<a class="btn" href="/system/gauge/{~$gauge['gid']~}" title="设置量表">
										<i class="icon-cog"></i>								
									</a>			
									<a onclick="$('#need_for_operate').val($('.gid',$(this).parent()).val())" class="btn btn-danger"  title="删除量表" data-toggle="modal" href="#deleteDialog"  data-keyboard="false" data-backdrop="false">
										<i class="icon-trash"></i>						
									</a>
								{~/if~}
							</td>
						</tr>
					{~/foreach~}
				</tbody>
			</table>
		</div> <!-- /widget-content -->
	</div>
	<input type="hidden" id="need_for_operate">
	<div id="deleteDialog" class="modal hide fade" aria-hidden="true" tabindex="-1">
		 
		 <div class="modal-header">
			<a class="close" type="button" data-dismiss="modal" aria-hidden="true">×</a>
			<h3>警告：您即将进行危险操作</h3>
		 </div>
		 <div class="modal-body">
			<p>是否确认要删除量表？该操作会连带删除所有该量表下的试题及维度信息。</p>
		 </div>
		 <div class="modal-footer">
			<a  class="btn" data-dismiss="modal" aria-hidden="true">关闭</a>
			<a  class="btn btn-primary" id="confirmDel"  onclick="confirmDelete($('#need_for_operate').val())">确认</a>
			
		 </div>
	</div>
	
	<div id="publishDialog" class="modal hide fade" aria-hidden="true" tabindex="-1">
		 
		 <div class="modal-header">
			<a class="close" type="button" data-dismiss="modal" aria-hidden="true">×</a>
			<h3>发布量表</h3>
		 </div>
		 <div class="modal-body">
			<p>是否确认要发布量表？该操作将发布量表，受测者将会被要求完成该测试。</p>
		 </div>
		 <div class="modal-footer">
			<a  class="btn" data-dismiss="modal" aria-hidden="true">关闭</a>
			<a  class="btn btn-primary" id="confirmPub" onclick="publishGauge($('#need_for_operate').val())">确认</a>
		 </div>
	</div>

	<div id="removeDialog" class="modal hide fade" aria-hidden="true" tabindex="-1">
		 
		 <div class="modal-header">
			<a class="close" type="button" data-dismiss="modal" aria-hidden="true">×</a>
			<h3>撤销量表发布</h3>
		 </div>
		 <div class="modal-body">
			<p>是否确认要撤销量表的发布？该操作将取消发布的量表，受测者将无法完成该测试。</p>
		 </div>
		 <div class="modal-footer">
			<a  class="btn" data-dismiss="modal" aria-hidden="true">关闭</a>
			<a  class="btn btn-primary" id="confirmRemove" onclick="removeGauge($('#need_for_operate').val())">确认</a>
		 </div>
	</div>
{~include file="footer.tpl"~}