	<input type="hidden" value="{~$questions->records+1~}" id="hidden_sequence"/>
	<div class="widget  widget-table">		
		<div class="widget-header">
			<i class="icon-th-list"></i>
			<h3>试题列表</h3>
			<div class="pull-right" style="margin-right:15px;">
			{~if ($questions->page > 1) and ($questions->page < $questions->total) ~}
				<a onclick="questions_page('/system/question/{~$questions->gid~}?page={~$questions->page-1~}')" >上一页</a>
				 | 
				 <a onclick="questions_page('/system/question/{~$questions->gid~}?page={~$questions->page+1~}')">下一页</a> |
			{~elseif $questions->page eq 1 ~}
				<a onclick="questions_page('/system/question/{~$questions->gid~}?page={~$questions->page+1~}')">下一页</a> |
			{~else~}
				<a onclick="questions_page('/system/question/{~$questions->gid~}?page={~$questions->page-1~}')">上一页</a> |
			{~/if~}
				总页数：{~$questions->total~} | 当前页：<span id="current_page">{~$questions->page~}</span>
			</div>
		</div> <!-- /widget-header -->
		<div class="widget-content">					
			<table class="table table-striped table-bordered">
				<thead>
					<th>顺序号</th>
					<th>题干</th>
					<th>所属维度</th>
					<th> </th>
				</thead>
				<tbody>
					{~foreach $questions->rows as $question~}
						<tr>
							<td style="width:50px;">{~$question['sequence']~}</td>
							<td>{~$question['content']~}</td>
							<td style="width:200px;">{~$question['name']~}</td>
							<td class="action-td">
								
								<a  data-target="#questionDialog" role="button" data-toggle="modal"  class="btn" title="修改题目设置" onclick="modifyQuestion($('input',this).val())">
									<input type="hidden" value="{~$question['qid']~}">
									<i class="icon-cog"></i>								
								</a>
								
							
								
								
								<a class="btn btn-danger" title="删除题目">
									<i class="icon-trash"></i>						
								</a>				
							</td>
						</tr>
					{~/foreach~}
				</tbody>
			</table>
			
			
								
	    <!-- Modal -->
	    <div id="questionDialog" style="width:680px;" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="questionDialogLabel" aria-hidden="true">
		    <div class="modal-header">
		    	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		    	<h3 id="questionDialogLabel">题目设置</h3>
		    </div>
		    <div class="modal-body">
		    	<p></p>
		    </div>
		    <div class="modal-footer">
		    	<button class="btn" data-dismiss="modal" aria-hidden="true">关闭</button>
		    	<button class="btn btn-primary" onclick="saveQuestion()">保存设置</button>
		    </div>
	    </div>	
		</div> <!-- /widget-content -->
	</div>	