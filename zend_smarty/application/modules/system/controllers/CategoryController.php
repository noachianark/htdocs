<?php
class System_CategoryController extends Zend_Rest_Controller
{

    public function init()
    {
    	$this->_helper->viewRenderer->setNoRender(true);
    }
    
    public function listAction(){
    	echo "list";
    }

    public function indexAction()
    {
    	$string='{"min":3,"max":4,"alter":5}';
    	$aa=json_decode($string);
		echo "index";
    }
    
    public function editAction(){
    	echo "edit";
    }
     
    public function newAction(){
    	echo "new";
    }
    
    public function getAction(){
    	echo "get";
    }

    public function postAction(){
		echo "post";
    }

    public function deleteAction(){
    	echo "delete";
    }
    
    public function putAction(){
    	echo "put";
    }
    
    public function headAction(){
    	 
    }

}