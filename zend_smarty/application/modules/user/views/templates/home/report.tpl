<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>{~$serial~}_{~$username~}_测试报告</title>
<link href="/css/bootstrap.min.css" rel="stylesheet" />
<link href="/css/bootstrap-responsive.min.css" rel="stylesheet" />

<link href="/css/fonts.css" rel="stylesheet" />
<link href="/css/font-awesome.css" rel="stylesheet" />
<script src="/scripts/jquery-1.8.3.js"></script>
    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
</head>
<style media="print">
	.noprint{
		display:none;
	}
</style>
<style>
	.block{
	    background-color: #ffffff;
	    border-radius: 6px 6px 6px 6px;
	}
	h4{
		margin:0px;
		color:#000000;
	}
	hr{
		border-color:#f1f1f1;
	}
	pre{
		font-size:16px;
	}
	div{
		font-size:16px;
	}
</style>
<script>
	$(function(){
		$("#download").bind("click",download);
	});
	function download(){
		$("#download").unbind();
		$("#download").button("loading");
		$.post("/user/report/export",{report:$("#report").html()}).done(function(data){
			
		});
	}
</script>
<body>

<div id="report" style="font-size:16px">
<div class="container" style="width:760px;">
	
	
	<div class="noprint">
		<a class="btn btn-info" data-toggle="modal" href="#myModal">打印帮助</a>	
	    <div class="modal fade" id="myModal" style="width:650px;">
		    <div class="modal-header">
			    <a class="close" data-dismiss="modal">×</a>
			    <h3>如何打印或者保存为PDF？</h3>
		    </div>
		    <div class="modal-body">
		    	<p>
		    		<div class="alert alert-info">
		    			<strong>1、使用firefox浏览器点击文件、打印选项</strong>
						<div style="text-align:center;">
							<img src="/images/help/down_help_1.jpg">
						</div>
					</div>
					<br>
		    		<div class="alert alert-warning">
		    			<strong>2、点击打印设置选项并按照以下方式设置</strong>
						<div style="text-align:center;">
							<img src="/images/help/down_help_2.jpg">
						</div>
					</div>					
					<br>
		    		<div class="alert alert-info">
		    			<strong>3、如果安装过adobe acrobat pdf或者打印机时，直接点击打印</strong>
						<div style="text-align:center;">
							<img src="/images/help/down_help_3.jpg">
						</div>
					</div>
					<br>
		    		<div class="alert alert-success">
		    			<strong>4、如果没有安装pdf转换工具或者想输出PDF文本，则需要安装doPdf工具</strong>
						<div style="text-align:center;">
							<a href="/soft/dopdf-7.exe">点击此处下载软件</a>
						</div>
					</div>									
					<br>
		    		<div class="alert alert-info">
		    			<strong>5、doPdf工具安装完毕后，在步骤1、2的前提下，选择打印，目标选择doPdf</strong>
						<div style="text-align:center;">
							<img src="/images/help/down_help_3.jpg"><br>
						</div>
						<br>
						<strong>选择要保存的位置，点击确定后即可。</strong>
						<div style="text-align:center;">
							<img src="/images/help/down_help_4.jpg">
						</div>						
					</div>					
					
		    	</p>
		    </div>
	    </div>
	</div>
	
	
	<div class="block">
		<div class="pull-right" style="margin-right:15px;">报告编号：<span style="color:#ff0000;">{~$serial~}</span></div>
		<div style="height:158px;width:100%;"><img src="/images/report_title.gif"></div>		
		
		<div class="identity" style="padding:16px;">
			<div class="pull-left" style="color:#3A87AD;margin-left:10px;font-size:16px;">
<!-- 				综合评测结论：
				{~if $report->getPassed() eq 1~}
					<span><i class="icon-star  icon-large"></i>适合从事职业驾驶工作</span>
				{~else~}
					<span><i class="icon-star-half-empty  icon-large"></i>不适合从事职业驾驶工作</span>
				{~/if~} -->
			</div>
			<div class="pull-right" style="font-size:16px;color:#173885;font-weight:bolder;"><span style="color:#797979">身份证号：</span>{~$id_card~}</div>
			<div class="pull-right" style="font-size:16px; margin-right:30px;color:#173885;font-weight:bolder;"><span style="color:#797979">姓名：</span>{~$username~}</div>
		</div>

		<hr>
		
		<div class="divider" style="margin-bottom:20px;s">
			<span class="badge" style="font-size:16px;" >基本信息</span>
			<div style="margin-top:20px;font-weight:bold;">
				<div style="display:inline-block;width: 100%;">
					<div class="col-md-4" style="width:33%;float:left;">
						<ul>
							<li>年龄：{~$basic->getAge()~}岁</li>
							<li>驾龄：{~$basic->getDriver_age()~}年</li>
							<li>联系方式：{~$basic->getPhone()~}</li>
							<li>工作单位：{~$basic->getPhone()~}</li>
						</ul>
						
					</div>
					<div class="col-md-4" style="width:33%;float:left;">
						<ul>
							<li>职业驾驶：{~$basic->getIs_driver()~}</li>
							<li>驾驶车型：{~$basic->getCar_type()~}</li>
							<li>驾照：{~$basic->getLisence()~} 类别</li>
						</ul>
					</div>
					<div class="col-md-4" style="width:33%;float:left;">
						<ul>
							<li>一年内酒驾经历：{~$basic->getAlcohol()~} 次</li>
							<li>驾驶时有抽烟习惯：{~$basic->getSmoke_drive()~}</li>
							<li>一年内肇事经历：{~$basic->getIs_accident()~}</li>
						</ul>
					</div>
				</div>
			</div>
			<hr>
		</div>

		{~foreach $answers as $answer~}

		{~if ($answer->guage_name === "双手调节器实验")~}

		{~else~}
			<div class="divider">
				<span class="badge" style="font-size:16px;" >{~$answer->guage_name~}</span>
				<span style="float:right;margin-right:15px;">
				评测结论：
				{~if ($answer->getResult()->passed === "true") or ($answer->getResult()->passed == 1)~}
					<span  style="color:Green;"><i class="icon-ok  icon-large"></i>测试合格</span>
				{~else~}
					<span style="color:#3A87AD;"><i class="icon-info-sign icon-large"></i>需要注意</span>
				{~/if~}
				</span>
				
			</div>
			

				<ul style="margin-top:15px;margin-bottom:15px;">
				{~foreach $answer->getResult()->comment as $result~}

					{~if ($result->getDimension_name()==="不计分")~}
					
					{~else~}
						<li>
							<h4><div {~if $result->getDecisive() eq 1 ~}
										{~if $result->getPassed() === false or $result->getPassed() === 'false' ~}
										 	style='display:inline-block;border-bottom:2px solid #000;' 
									 	{~/if~}

							
									 {~else~} 
									 	
									 {~/if~}>{~$result->getDimension_name()~}</div></h4>
							<pre>{~$result->getConclusion()~}</pre>									
						</li>
					{~/if~}

				{~/foreach~}
					
				</ul>	

			
			<hr>
		{~/if~}
		{~/foreach~}
		
		<div class="pull-right">
			<p  style="font-size:20px;">云南交通职业技术学院驾驶员心理素质测评研究中心</p>
			<p style="text-align:center;font-size:20px;">{~$time~}</p>	
		</div>
		
	</div>
	
</div>	
	
</div>
<script src="/scripts/framework/bootstrap.js"></script>
</body>
</html>