<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="content-type" content="text/plain; charset=UTF-8"/>
    <title>iWeekly 一周食材</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Loading Bootstrap -->
    <link href="/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="/css/docs.css">
	

    <!-- Loading Flat UI -->
    <link href="/css/flat-ui.css" rel="stylesheet">
	<link rel="stylesheet" href="/css/bootstrap-editable.css">

    <link rel="shortcut icon" href="/images/favicon.ico">

    <!-- HTML5 shim, for IE6-8 support of HTML5 elements. All other JS at the end of file. -->
    <!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
    <![endif]-->

  </head>
  <body>
    <div class="bs-header">
        <div class="container">
            <h1>iWeekly</h1>
            <p>轻松计算、展示一个星期食材的用量、价格，两手都要抓，两手都要硬，做一个掌控全局的男人</p>
        	<a class="btn btn-primary btn-hg dishBtn pull-right">管理菜品与原料</a>
		</div>
    </div>