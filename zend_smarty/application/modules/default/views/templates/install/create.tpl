{~include file="header.tpl"~}
<body>
    {~literal~}
    <script>
    $(function(){
        var name = $( "#name" ),
        email = $( "#email" ),
        password = $( "#password" ),
        password2 = $("#password2"),
        allFields = $( [] ).add( name ).add( email ).add( password ).add(password2),
        tips = $( ".validateTips" );
        error_tip=$("#error_tip");
 
        function updateTips( t ) {
            error_tip.text( t );
            tips.addClass( "ui-state-highlight ui-state-error ui-corner-all" );
            tips.removeClass("display_off");
            setTimeout(function() {
                tips.removeClass( "ui-state-highlight ui-state-error ui-corner-all", 1500 );
            }, 500 );
        }
 
        function checkLength( o, n, min, max ) {
            if ( o.val().length > max || o.val().length < min ) {
                o.addClass( "ui-state-error" );
                updateTips( n + " 的长度必须在 " +
                    min + " 到 " + max + "之间." );
                return false;
            } else {
                return true;
            }
        }
 
        function checkRegexp( o, regexp, n ) {
            if ( !( regexp.test( o.val() ) ) ) {
                o.addClass( "ui-state-error" );
                updateTips( n );
                return false;
            } else {
                return true;
            }
        }
        
        function checkPasswordEqual(p1,p2,name,min,max){
            if(p2.val().length>max || p2.val().length<min){
                p2.addClass( "ui-state-error" );
                updateTips( name + " 的长度必须在 " +
                    min + " 到 " + max + "之间." );
                return false;
            }
            
            if(p2.val()!==p1.val()){
                p2.addClass( "ui-state-error" );
                updateTips( "两次密码输入不一致.");
                return false;
            }
            return true;
        }
        
        function submit(){
                    var bValid = true;
                    allFields.removeClass( "ui-state-error" );
 
                    bValid = bValid && checkLength( name, "账户名", 6, 20 );
                    bValid = bValid && checkLength( email, "电子邮件", 6, 50 );
                    bValid = bValid && checkLength( password, "密码", 6, 20 );
                    bValid = bValid && checkPasswordEqual(password,password2,"确认密码",5,16);
                     
                    bValid = bValid && checkRegexp( name, /^([0-9a-z_A-Z])+$/i, "用户名只能包含 字母、数字、下划线." );
                    // From jquery.validate.js (by joern), contributed by Scott Gonzalez: http://projects.scottsplayground.com/email_address_validation/
                    bValid = bValid && checkRegexp( email, /^[a-zA-Z0-9_-]+@[a-zA-Z0-9_-]+(\.[a-zA-Z0-9_-]+)+$/, "电子邮件格式错误,请更正为如下格式：xxx@xxx.xx" );
                    bValid = bValid && checkRegexp( password, /^([0-9a-zA-Z])+$/, "密码只能使用字母和数字的组合 : a-z 0-9" );
                    if ( bValid ) {
                         error_tip.text( "" );
                         tips.addClass("display_off");
                        return true;
                    }else{
                        tips.removeClass("display_off");
                        return false;
                    }
        }
        
        
           $("#dialog_config").dialog({
            width:"480px",
            minHeight:'320px',
            closeOnEscape:false,
            position:"center",
            modal:true,
            draggable:false,
            resizable:false,
            title:"系统数据初始化向导",
            show:{
                effect:'fade',
                duration:1200
            },
            buttons:{
                
                "下一步":function(){
                    if(submit()){
                        $.post(
                            "/default/install/admin",
                            {
                                "username":name.val(),
                                "password":password.val()
                            },
                            function (data,textStatus){
                                if(data=="success"){
                                    //window.location.href="/default/install/finish";
                                    $("#dialog_config").dialog("close");
                                    
                                    $("#dialog_finish").dialog({
                                        width:"480px",
                                        minHeight:'320px',
                                        closeOnEscape:false,
                                        position:"center",
                                        modal:true,
                                        draggable:false,
                                        resizable:false,
                                        title:"系统数据初始化向导",
                                        show:{
                                            effect:'fade',
                                            duration:1200
                                        },
                                        buttons:{
                                            "完成初始化":function(){
                                                window.location.href="/";
                                            }
                                        }
                                    });
                                    $("#dialog_finish").removeClass("display_off");
                                }else{
                                    updateTips(data);
                                }
                            }
                        );
                    }
                }
            }
       });

        
    });
</script>
{~/literal~}
    <style>
        .ui-dialog-titlebar-close{
         display: none;
        }
    </style>
    
    
    
    
    
{~if $result eq 'success'~}


         <div id="dialog_config" class="ui-dialog ui-widget ui-widget-content ui-corner-all ui-dialog-buttons">
           <div style="height: 360px;font-size: 16px;">
               <p class="dialog_p">
                   数据库创建成功，现在您需要对新数据库进行配置，首先创建<span style="color:#0073EA;">管理员账号</span>，您将使用该管理员账号对系统进行管理，
                   稍后您可以在<span style="color:#0073EA;">管理员界面</span>当中添加更多适当的角色以便分管系统的各项功能。
               </p> 
                    <div id="account_form" style="font-size: 12px;">
                        <div class="validateTips">
                            <span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;">
                            </span>
                            <div id="error_tip"> 所有项目均为必填项</div>
                        </div>
                     
                        <form style="font-size: 12px;">
                            <fieldset>
                                <label for="name">账号：</label>
                                <input type="text" name="name" id="name" class="text ui-widget-content ui-corner-all" />
                                <label for="email">邮件地址：</label>
                                <input type="text" name="email" id="email" value="" class="text ui-widget-content ui-corner-all" />
                                <label for="password">账号密码：</label>
                                <input type="password" name="password" id="password" value="" class="text ui-widget-content ui-corner-all" />
                                <label for="password">确认密码：</label>
                                <input type="password" name="password2" id="password2" value="" class="text ui-widget-content ui-corner-all" />
                            </fieldset>
                        </form>
                    </div>
           </div>
        </div>
{~elseif $result neq ''~}
         <div id="dialog_config" class="ui-dialog ui-widget ui-widget-content ui-corner-all ui-dialog-buttons">
           <div style="height: 260px;font-size: 16px;">
               <p class="dialog_p">
                   数据库无法创建成功，可能的原因如下：
               </p> 
                    {~foreach $result as $message~}
                        <p class="dialog_p">{~$message~}</p>
                    {~/foreach~}
                 <p class="dialog_p">
                     为了解决此问题，建议您：{$suggest}
                 </p>
                 
           </div>
        </div>
{~else~}
    
{~/if~}


        <div id="dialog_finish" class=" display_off ui-widget ui-widget-content ui-corner-all ui-dialog-buttons">
           <div style="height: 230px;font-size: 16px;">
               <p class="dialog_p">
                   管理员用户已经创建, 您现在可以使用该账号对系统进行管理。
               </p> 
               <p class="dialog_p">
                   点击<span style="color:#0073EA;">完成</span>开始使用系统。
               </p>
           </div>
        </div>


</body>
{~include file="footer.tpl"~}