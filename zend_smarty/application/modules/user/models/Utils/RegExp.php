<?php
class User_Model_Utils_RegExp{
	//去除字符串空格
	static function strTrim($str)
	{
		return preg_replace("/\s/","",$str);
	}
	
	static function nickName($str="",$min,$max){
		if(strlen($str)>=$min && strlen($str)<=$max){
			if(preg_match("/^[\x{4e00}-\x{9fa5}]+$/u", $str)){
					return true;
			}else{
				return "姓名必须是中文字符";
			}
		}else{
			return "姓名长度必须在2-6个字符";
		}
	}
	 
	//验证用户名
	static function userName($str="",$type,$min,$max)
	{
		$str=self::strTrim($str);
		if($max<=strlen($str) && min>=strlen($str))
		{
			return "用户名长度必须在".$min."至".$max."之间";
		}else{
			switch($type)
			{
				case "EN"://纯英文
					if(preg_match("/^[a-zA-Z]+$/",$str))
					{
						return true;
					}else{
						return false;
					}
					break;
				case "ENNUM"://英文数字
					if(preg_match("/^[a-zA-Z0-9]+$/",$str))
					{
						return true;
					}else{
						return false;
					}
					break;
				case "ALL":    //允许的符号(|-_字母数字)
					if(preg_match('/^[\|\-\_a-zA-Z0-9]+$/',$str))
					{
						return true;
					}else{
						return "用户名只能包含|-_以及字母和数字";
					}
					break;
			}
		}
	}
	 
	//验证密码长度
	static function passWord($str="",$min,$max)
	{
		$str=self::strTrim($str);
		if(strlen($str)>=$min && strlen($str)<=$max)
		{
			return true;
		}else{
			return $str;
		}
	}
	 
	//验证Email
	static function Email($str)
	{
		$str=self::strTrim($str);
	
		if(filter_var($str,FILTER_VALIDATE_EMAIL)===false)
		{
			return "邮件格式错误";
		}else{
			return true;
		}
	
	}
	 
	//验证身份证(中国)
	static function idCard($str="")
	{
		$str=self::strTrim($str);
		if(preg_match("/^([0-9]{15}|[0-9]{17}[0-9x-x])$/i",$str))
		{
			return true;
		}else{
			return "身份证格式错误";
		}
	}
	 
	//验证座机电话
		static function Phone($type,$str)
		{
			$str=self::strTrim($str);
			switch($type)
			{
				case "CHN":
					if(preg_match("/^([0-9]{3}|0[0-9]{3})-[0-9]{7,8}$/",$str))
					{
						return true;
					}else{
						return false;
					}
					break;
				case "INT":
					if(preg_match("/^[0-9]{4}-([0-9]{3}|0[0-9]{3})-[0-9]{7,8}$/",$str))
					{
						return true;
					}else{
						return false;
					}
					break;
			}
		}
	
	 

}