        <script>
        	var dayArr = ["星期一","星期二","星期三","星期四","星期五","星期六","星期日"];
            var timeArr = ["早餐","中餐","晚餐"];
        	$(function(){
				$(".dishpanel h5").text(dayArr[eval("{~$day~}")] +  timeArr[eval("{~$time~}")]);
	        	$("#backToMain").on("click",function(){
	                	$.get("/dish").done(function(data){
							$(".dishpanel").slideUp();
							$(".dishpanel").html();
							$(".main").html(data);
							$(".main").slideDown();
							$(".main").css("overflow","visible");
						}).fail(faultHandler);    
	            });
				$("#addDishBtn").on("click",function(){
					$.ajax({
						url:"/dish",
						type:"POST",
						data:$("#dish_form").serialize()
					}).done(function(){
		                $.get("/dish",{day:eval("{~$day~}"),time:eval("{~$time~}")}).done(function(data){
		                    $(".dishpanel").html(data);
		                }).fail(faultHandler);
					}).fail(faultHandler);
				});
				
				$(".delDish").on("click",function(){
					$.ajax({
						url:"/dish/"+$(this).data("id"),
						type:"DELETE"
					}).done(function(){
						$.get("/dish",{day:eval("{~$day~}"),time:eval("{~$time~}")}).done(function(data){
		                    $(".dishpanel").html(data);
		                }).fail(faultHandler);
					}).fail(faultHandler);
				});
				
				
				$(".edit_dish_name").editable({
					title:"修改类型",
				    ajaxOptions: {
					    type: 'POST'
				    },
					params: function(params) {
					    //originally params contain pk, name and value
					    params.name = params.value;
					    return params;
				    },
					success:function(){
						$.get("/dish",{day:eval("{~$day~}"),time:eval("{~$time~}")}).done(function(data){
		                    $(".dishpanel").html(data);
		                }).fail(faultHandler);				
					}
				});
				
				$(".edit_dish_parameter").editable({
					title:"修改类型",
				    ajaxOptions: {
					    type: 'POST'
				    },
					params: function(params) {
					    //originally params contain pk, name and value
						var par = {};
					    par.parameter = params.value;
					    return par;
				    },
					success:function(){
						$.get("/dish",{day:eval("{~$day~}"),time:eval("{~$time~}")}).done(function(data){
		                    $(".dishpanel").html(data);
		                }).fail(faultHandler);				
					}
				});								
				
				
				$(".saveDish").on("click",function(){
					$.ajax({
						url:"/dish/"+$(this).data("id"),
						type:"GET"
					}).done(function(data){
						
						$(".materialpanel").html(data);
						$(".materialpanel").slideDown();
						$(".dishpanel").slideUp();
						
					}).fail(faultHandler);
				});
				
			});

        </script>

        <div class="row">
            <div id="backToMain" class="backBtn btn-hg col-md-2">
                <span class="glyphicon glyphicon-chevron-left"></span> 返回
            </div>
            <div class="col-md-4">
                <h5></h5>
            </div>         
        </div>

		<form id="dish_form" role="form" class="alert alert-success">
            <div class="row">
               
			    <div class="col-md-4">
                    <div class="form-group">
                        <input name="name" class="form-control" type="text" placeholder="请输入菜名" value="">
                    </div>
                </div>
				
                <div class="col-md-4">
                    <div class="form-group">
					     <div class="input-group">
                            <input name="parameter"  type="text" class="form-control" placeholder="系数">
                            <span class="input-group-addon">必须为数字</span>
                        </div>
                    </div>
                </div>
				
				<div class="col-md-4">
	                <a type="button" class="btn btn-primary" id="addDishBtn">
	                  <span class="glyphicon glyphicon-ok"></span> 添加菜品
	                </a>
	            </div>
				
				<input type="hidden" value="{~$day~}" name="day">
				<input type="hidden" value="{~$time~}"	name="time">			
				
            </div>
		     

			
		</form>

			
			
        <table class="table table-striped table-responsive  table-bordered">
            <thead>
                <tr>
                    <th>菜名</th>
					<th>系数</th>
                    <th></th>
                </tr>
            </thead>
			<tbody>
				{~foreach $dishes as $dish ~}
				<tr>
					<td>
						<a class="edit_dish_name" data-url="/dish/{~$dish['id']~}" data-name="name" data-type="text" data-pk="{~$dish['id']~}">{~$dish["name"]~}</a>						
					</td>
					<td>
						<a class="edit_dish_parameter" data-url="/dish/{~$dish['id']~}" data-name="parameter" data-type="text" data-pk="{~$dish['id']~}">{~$dish["parameter"]~}</a>											
					</td>
					<td style="width:130px;">
						<a class="btn btn-sm btn-danger delDish"  data-id="{~$dish['id']~}">删除</a>
						<a class="btn btn-sm btn-primary saveDish"  data-id="{~$dish['id']~}">编辑</a>
					</td>
				</tr>
				{~/foreach~}
			</tbody>
        </table>