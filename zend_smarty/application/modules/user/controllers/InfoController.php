<?php

class User_InfoController extends Zend_Rest_Controller
{

	public function init()
	{
		$this->_helper->viewRenderer->setNoRender(true);
	}
    public function indexAction()
    {
    	$this->getAction(); 
    }
 
    public function getAction()
    {
    	$userMapper=new User_Model_UserMapper();
    	$user=new User_Model_User();
    	$result=$userMapper->find($this->getRequest()->getParam("id"), $user,array("username","nickname"));
    	if($result===true){
    		$this->getResponse()->setHttpResponseCode(200)->appendBody($user->toJson());
    	}else{
    		$this->getResponse()->setHttpResponseCode(422)->appendBody($result);
    	}
    	
    }
 
    public function postAction()
    {
     	$userMapper=new User_Model_UserMapper();
     	$user=new User_Model_User();
     	$user->setOptions($this->getRequest()->getParams());
     	//var_dump($user);
     	$result=$userMapper->save($user);
		if($result===true){
			$this->getResponse()->setHttpResponseCode(201);
		}else{
			$this->getResponse()->setHttpResponseCode(422)->appendBody($result);
		}
    }
 
    public function putAction()
    {
    	$userMapper=new User_Model_UserMapper();
    	$user=new User_Model_User();
    	$user->setUserid($this->getRequest()->getParam('id'));
    	$user->setOptions($this->getRequest()->getParams());
    	$result=$userMapper->update($user);
    	if($result===true){
			$this->getResponse()->setHttpResponseCode(201);
		}else{
			$this->getResponse()->setHttpResponseCode(422)->appendBody($result);
		}
    }
 
    public function deleteAction()
    {
    	$userMapper=new User_Model_UserMapper();
    	$result=$userMapper->delete($this->getRequest()->getParam('id'));
    	if($result===true){
    		$this->getResponse()->setHttpResponseCode(200);
    	}else{
    		$this->getResponse()->setHttpResponseCode(422)->appendBody($result);
    	}
    }

    public function headAction(){
    	
    }

}

