{~include file="header2.tpl"~}
<style>
	.fixed_container{
		width:270px;
		float:right;
	}
	.control-label{
		color:#3A87AD;
	}
</style>
<script>
	
	
	function validateInfo(obj){
		if(obj.val().length>obj.data("max")){
			return "长度必须在"+obj.data("max")+"以内";
		}
		if(obj.val().length<obj.data("min")){
			return "长度必须大于"+obj.data("min");
		}
		if(obj.hasClass("number")){
			if(!(/^([0-9])+$/i.test(obj.val()))){
				return "必须填数字";
			}
		}
		if(obj.hasClass("chinese")){
			if(!(/^[\u4e00-\u9fa5]+$/.test(obj.val()))){
				return "只能填写汉字";
			}
		}
		if(obj.hasClass("float")){
			if(!(/^-?\d+\.?\d*$/.test(obj.val()))){
				return "必须填整数或小数";
			}
		}
		return true;
	}
	
	
	var total=11;
	var done=0;
	var rate=0;

	$(function(){
		
		//输入框的脚本
		$("input[type='text']").focusout(function(){
			var validated=validateInfo($(this));
			if($(this).parent().parent().hasClass("success")){
				if(validated!==true){
					//-1
					done-=1;
					$(this).parent().parent().removeClass("success");
					$(this).parent().parent().addClass("error");
					$(this).parent().find(".help-inline").remove();
					$(this).parent().append("<span class='help-inline'><i class='icon-exclamation'></i>"+validated+"</span>");					
				}else{
					//success
					$(this).parent().parent().addClass("success");
					$(this).parent().parent().removeClass("error");
					$(this).parent().find(".help-inline").remove();
					$(this).parent().append("<span class='help-inline'><i class='icon-ok'></i></span>");			
				}
			}else{
				if(validated===true){
					//+1
					done+=1;
					$(this).parent().parent().addClass("success");
					$(this).parent().parent().removeClass("error");
					$(this).parent().find(".help-inline").remove();
					$(this).parent().append("<span class='help-inline'><i class='icon-ok'></i></span>");						
				}else{
					//error info
					$(this).parent().parent().removeClass("success");
					$(this).parent().parent().addClass("error");
					$(this).parent().find(".help-inline").remove();
					$(this).parent().append("<span class='help-inline'><i class='icon-exclamation'></i>"+validated+"</span>");		
				}
			}
			
			
			if(done>0){
				rate=parseInt(done/total*100);
			}else{
				rate=0;
			}
			$("#progress").css("width",rate+"%");
			$("#percent").text(rate+"%");
			$("#done").text(done);

		});
		
		//radio框的脚本
		$("input[type='radio']").focusout(function(){
			var choose=$(this).parent().find("input[type='radio']:checked").val();
			console.log(choose);
			if($(this).parent().parent().parent().hasClass("success")){
				return;
			}
			
			if(choose!=undefined){
				done+=1;
				if(done>0){
					rate=parseInt(done/total*100);
				}else{
					rate=0;
				}
				$(this).parent().parent().parent().addClass("success");
				$(this).parent().parent().parent().removeClass("error");
				$("#progress").css("width",rate+"%");
				$("#percent").text(rate+"%");
				$("#done").text(done);
			}
		});
		
		$("select").focusout(function(){
			console.log($(this).val());
			var choose=$(this).val();
			if(choose && !$(this).parent().parent().hasClass("success")){
				done+=1;
				$(this).parent().parent().addClass("success");
				$(this).parent().parent().removeClass("error");
				$(this).parent().find(".help-inline").remove();
				$(this).parent().append("<span class='help-inline'><i class='icon-ok'></i></span>");	
			}else{
				done-=1;
				$(this).parent().parent().removeClass("success");
				$(this).parent().parent().addClass("error");
				$(this).parent().find(".help-inline").remove();
				$(this).parent().append("<span class='help-inline'><i class='icon-exclamation'></i>请选择该项目</span>");
			}
			if(done>0){
				rate=parseInt(done/total*100);
			}else{
				rate=0;
			}
			$("#progress").css("width",rate+"%");
			$("#percent").text(rate+"%");
			$("#done").text(done);			
		});
		
	});

	function saveBasicInfo(){
		var bool=true;
		$("input[type='text']").each(function(){
			var info=validateInfo($(this));
			if(info!==true){
				bool=false;
				$(this).parent().parent().removeClass("success");
				$(this).parent().parent().addClass("error");
				$(this).parent().find(".help-inline").remove();
				$(this).parent().append("<span class='help-inline'><i class='icon-exclamation'></i>"+info+"</span>");	
			}
		});
		$("input[type='radio']").each(function(){
			if(!$(this).parent().parent().parent().hasClass("success")){
				bool=false;
				$(this).parent().parent().parent().addClass("error");				
			}
		});
		$("select").each(function(){
			if($(this).val().length<1){
				bool=false;
				$(this).parent().parent().removeClass("success");
				$(this).parent().parent().addClass("error");
				$(this).parent().find(".help-inline").remove();
				$(this).parent().append("<span class='help-inline'><i class='icon-exclamation'></i>请选择该项目</span>");
			}
		});

		if(bool){
			$.post("/user/home/new",{
				userid:$("#userid").val(),
				phone:$("#phone").val(),
				age:$("#age").val(),
				unit:$("#unit").val(),
				name:$("#name").val(),
				car_type:$("#car_type").val(),
				is_driver:$("input[name='isDriver']:checked").val(),
				is_accident:$("input[name='isAccident']:checked").val(),
				driver_age:$("#drive_age").val(),
				lisence:$("input[name='lisence']:checked").val(),
				smoke_drive:$("input[name='isSmoke']:checked").val(),
				alcohol:$("#alcohol").val(),
				is_complished:1
			}).promise().done(function(data){
				//console.log("gaodingle!!!!!");
				toastr.success("保存成功，现在将跳转至评测试题列表");
				setTimeout(function(){
					window.location.href="/user/home";
				},1500);
				
			}).fail(function(data){
				toastr.error(data.responseText,"错误信息");
			});
		}else{
			toastr.error("您还有选项没有填或填写不规范，请检查填写再提交","提示信息");
		}

	}

</script>
<input type="hidden" id="userid" value="{~$userid~}">
			<div class="span9" style="margin-left:30px;">
				<div style="min-height:400px;">
					<div class="widget">
							<div class="widget-header">
								<h3 style="margin-top:10px;">
									驾驶员基本情况
								</h3>
							</div> <!-- /widget-header -->
									
							<div class="widget-content">

								<form id="basic_form" class="form-horizontal">
									<fieldset>

										<div class="control-group">											
											<label for="name" class="control-label">姓名</label>
											<div class="controls">
												<input  type="text" value="" id="name" class="input-large chinese"  data-min="2" data-max="5">
											</div> <!-- /controls -->				
										</div> <!-- /control-group -->	

										<div class="control-group">											
											<label for="phone" class="control-label">电话号码</label>
											<div class="controls">
												<input  type="text" value="" id="phone" class="input-large number"  data-min="7" data-max="13">
											</div> <!-- /controls -->				
										</div> <!-- /control-group -->	

										<div class="control-group">											
											<label for="unit" class="control-label">单位</label>
											<div class="controls">
												<input  type="text" value="" id="unit" class="input-large chinese"  data-min="1" data-max="25">
											</div> <!-- /controls -->				
										</div> <!-- /control-group -->	

										
										<div class="control-group">											
											<label for="age" class="control-label">年龄</label>
											<div class="controls">
												<input type="text" value="" id="age" class="input-mini number"  data-min="1" data-max="2">周岁
											</div> <!-- /controls -->				
										</div> <!-- /control-group -->	
										
								

										<div class="control-group">											
											<label class="control-label">是否为职业驾驶员</label>
											<div class="controls">
								              <label class="radio">
								                <input type="radio" value="是" name="isDriver">
													是
								              </label>
											  <label class="radio">
								                <input type="radio" value="否" name="isDriver">
													否
								              </label>
											</div> <!-- /controls -->				
										</div> <!-- /control-group -->	


										<div class="control-group">											
											<label class="control-label">驾龄</label>
											<div class="controls">
												<input type="text" value="" id="drive_age" class="input-medium number"  data-min="1" data-max="2">
											</div> <!-- /controls -->				
										</div> <!-- /control-group -->	


										<div class="control-group">											
											<label class="control-label">驾照种类</label>
											<div class="controls">
								              <label class="radio">
								                <input type="radio" value="A" name="lisence">
													A照
								              </label>
											  <label class="radio">
								                <input type="radio" value="B" name="lisence">
													B照
								              </label>
											  <label class="radio">
								                <input type="radio" value="C" name="lisence">
													C照
								              </label>
											  <label class="radio">
								                <input type="radio" value="D" name="lisence">
													D照
								              </label>											  											  
											</div> <!-- /controls -->				
										</div> <!-- /control-group -->

										<div class="control-group">											
											<label class="control-label">常驾驶的车辆类型</label>
											<div class="controls">
												<select id="car_type" style="height:27px;padding:0.4em;width:292px;">
													<option></option>
													<option>载客（大型）</option>
													<option>载客（中型）</option>
													<option>载客（小型）</option>
													<option>载客（微型）</option>
													<option>载货（大型）</option>
													<option>载货（中型）</option>
													<option>载货（小型）</option>
													<option>载货（微型）</option>
												</select>
											</div> <!-- /controls -->				
										</div> <!-- /control-group -->	


										<div class="control-group">											
											<label class="control-label">最近一年内是否有肇事经历</label>
											<div class="controls">
								              <label class="radio">
								                <input type="radio" value="是" name="isAccident">
													是
								              </label>
											  <label class="radio">
								                <input type="radio" value="否" name="isAccident">
													否
								              </label>
											</div> <!-- /controls -->				
										</div> <!-- /control-group -->

											

										

										<div class="control-group">											
											<label class="control-label">最近一年内酒后驾驶次数</label>
											<div class="controls">
												<input type="text" value="" id="alcohol" class="input-mini number" data-min="1" data-max="2"> 次
												<p class="help-block">颁发禁酒令后</p>
											</div> <!-- /controls -->				
										</div> <!-- /control-group -->	
																	

										<div class="control-group">											
											<label class="control-label">驾车时是否有吸烟习惯</label>
											<div class="controls">
								              <label class="radio">
								                <input type="radio" value="是" name="isSmoke">
													是
								              </label>
											  <label class="radio">
								                <input type="radio" value="否" name="isSmoke">
													否
								              </label>
											</div> <!-- /controls -->				
										</div> <!-- /control-group -->			
									</fieldset>
								</form>

							</div>
					</div>				
				</div><!--内容页。 -->
				
			</div>
			<div class="fixed_container">
				<div style="position:fixed;width:270px;">
					<div class="account-container">

					
						<div class="account-details">
						
							<span class="account-name" style="font-size:25px;margin-bottom:10px;"><a class="label label-primary" style="font-size:14px;margin-right:10px;margin-left:30px;">姓名</a>{~$username~}</span>
							
							<span class="account-role" style="font-size:16px;margin-bottom:10px;"><a class="label label-primary" style="font-size:14px;margin-right:10px;">身份证号</a>{~$id_card~}</span>
							
							<span class="account-role" style="font-size:16px;">
								<a class="label label-important" style="font-size:14px;">请您仔细核对身份证以及姓名防止误答</a>
							</span>
						
						</div> <!-- /account-details -->
					
					</div> <!-- /account-container -->

					<hr>
					<div class="alert alert-info">
						<strong>进行评测前，您需要完善您的个人驾驶情况的基本信息。</strong><br>
						<strong>您需要完善的信息数：</strong><span class="label label-warning">11</span>项<br>
						<strong>您已经完善的信息数：</strong><span class="label label-warning" id="done">0</span>项<br>
						<strong>当前进度<span class="label label-warning" id="percent">0%</span></strong>
						    <div class="progress progress-striped active progress-info" style="margin-bottom:0px;">
						    	<div class="bar" style="width: 0%;" id="progress"></div>
						    </div>
							<div style="text-align:center;margin-top:10px;"><a class="btn btn-large btn-info" style="width:100px;" onclick="saveBasicInfo()">提交</a></div>
							
					</div>	
					<hr>
					<div class="alert alert-success">
						<strong>量表说明：</strong><br>
驾驶适性检测可以使其了解自身的心理和生理素质，便于其针对地进行心理训练，以配合技术训练，从而提高驾驶员训练质量，保证行车安全。
					</div>	
				</div>
			</div>


{~include file="footer.tpl"~}