<?php
class System_Model_GaugeMapper{

	protected $_dbTable;
	
	public function setDbTable($dbTable)
	{
		if (is_string($dbTable)) {
			$dbTable = new $dbTable();
		}
		if (!$dbTable instanceof Zend_Db_Table_Abstract) {
			throw new Exception('Invalid table data gateway provided');
		}
		$this->_dbTable = $dbTable;
		return $this;
	}
	
	public function getDbTable(){
		if (null === $this->_dbTable) {
			$this->setDbTable('System_Model_DbTable_Gauge');
		}
		return $this->_dbTable;
	}
	
	
	public function update(System_Model_Gauge $gauge){
		try{
			$result=$this->getDbTable()->update($gauge->toArray(), $this->getDbTable()->getAdapter()->quoteInto('gid = ?', $gauge->getGid()));
		}catch(Exception $e){
			return Utils_SQLErrorMsg::formatException($e);
		}
		return (int)$result;
	}
	
	
	public function save(System_Model_Gauge $gauge){
		if($gauge->getGauge_name()){
			$uniq=$this->checkUniq($gauge->getGauge_name());
			if($uniq!==true){
				return $uniq;
			}
		}else{
			return "量表名称不能为空";
		}
		try{
			if($gauge->getGid()==null){
				$result=$this->getDbTable()->insert($gauge->toArray());
				$gauge->setGid($result);
			}else{
				$result=$this->getDbTable()->update($gauge->toArray(), $this->getDbTable()->getAdapter()->quoteInto('gid = ?', $gauge->getGid()));
			}
		}catch (Exception $e){
			return Utils_SQLErrorMsg::formatException($e);
		}
		return (int)$result;
	}
	
	public function find($id,System_Model_Gauge $gauge,array $filter=null){
		try{
			$result=$this->getDbTable()->find($id);
		}catch (Exception $e){
			return $e;
		}
		if(0==count($result)){
			//$error['message']="没有找到";
			//return json_encode($error);
			return "没有找到ID记录号为".$id."的量表信息";
		}
		$row = $result->current()->toArray();
	    if(isset($filter)){
			$options=array();
        	foreach($filter as $key=>$value){
				$options[$value]=$row[$value];
			}
        	$gauge->setOptions($options);
        }else{
        	$gauge->setOptions($row);
        }
	}
	
	public function findPublished($sex,$userid,$curPage=1,$perPage=10){
		$select=$this->getDbTable()->select()->where("is_display=1 and sex IN ( 0 ,".$sex." ) and gid NOT IN (select gauge_id from answers where user_id = ".$userid.")")
							->order("gid");
							//->limitPage($curPage,$perPage);
		$rows=$this->getDbTable()->getAdapter()->fetchOne("select count(*) from gauge where is_display=1 and sex IN( 0 ,".$sex." )");
		$done=$this->getDbTable()->getAdapter()->fetchOne("select count(*) from answers where user_id=".$userid);
		$data=array();
		$result=array();
		try{
			$data=$this->getDbTable()->fetchAll($select)->toArray();
			foreach($data as $item){
				$gauge=new System_Model_Gauge($item);
				array_push($result,$gauge);
			}
		}catch(Exception $e){
			return Utils_SQLErrorMsg::formatException($e);
		}
		$response=new stdClass();
		$response->page=$curPage;
		$response->total=ceil($rows/$perPage);
		$response->records=(int)$rows;
		$response->rows=$result;
		$response->done=$done;
		return $response;
	}
	
	
	
	public function findAll($curPage=1,$perPage=20,$to_json=true){
		$select=$this->getDbTable()->select()->from("gauge")->order("gid")
							->limitPage($curPage, $perPage);
		$rows=$this->getDbTable()->getAdapter()->fetchOne("select count(*) from gauge");
		$data=array();
		try{
			$data=$this->getDbTable()->fetchAll($select)->toArray();
		}catch (Exception $e){
			return Utils_SQLErrorMsg::formatException($e);
		}
		$response=new stdClass();
		$response->page=$curPage;
		$response->total=ceil($rows/$perPage);
		$response->records=(int)$rows;
		$response->rows=$data;
		if($to_json){
			return json_encode($response);
		}else{
			return $response;
		}
		
	}
	
	public function checkUniq($gauge_name){
		try{
			$data=$this->getDbTable()->fetchRow($this->getDbTable()->getAdapter()->quoteInto('gauge_name = ?', $gauge_name));
		}catch (Exception $e){
			return Utils_SQLErrorMsg::formatException($e);
		}
		if($data===null){
			return true;
		}
		return count($data->toArray())>0?true:"已经存在同名的量表，无法保存";
	}
	
	public function delete($gid){
		try{
			$where = $this->getDbTable()->getAdapter()->quoteInto('gid ='.$gid);
			$result=$this->getDbTable()->delete($where);
			$mapper=new System_Model_DimensionMapper();
			$mapper->deleteAll($gid);
			$mapper2=new System_Model_QuestionMapper();
			$mapper2->deleteAll($gid);
		}catch(Exception $e){
			return Utils_SQLErrorMsg::formatException($e);
		}
		return (int)$result;
		
	}
}

?>