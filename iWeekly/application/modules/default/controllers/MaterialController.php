<?php
class MaterialController extends Zend_Rest_Controller
{

    public function init()
    {
        /* Initialize action controller here */
    	//$this->view->hello="this is default index ";
    	$this->_helper->viewRenderer->setNoRender(true);
    }

    public function indexAction(){
    	$this->_helper->viewRenderer->setNoRender(false);
    	$material = new Default_Model_MaterialMapper();
    	$this->view->materials = $material -> findAll();    	
    }
    
    public function abortedIndexAction()
    {
    	$c_mapper = new Default_Model_ClassifyMapper();
    	$classify = $c_mapper->findAll();
    	$array = array();
    	foreach($classify as $cls){
    		$id = $cls["id"];
    		$m_mapper = new Default_Model_MaterialMapper();
    		$result = $m_mapper -> findByType($id);
    		$row = array();
    		$row["name"] = $cls["name"];
    		$total = 0.0;
    		$count = 0.0;
    		foreach($result as $material){
    			$total += (float)$material["count"] * (float)$material["price"];
    			$count += (float)$material["count"]; 
    		}
    		$row["total"] = $total;
    		$row["count"] = $count;
    		array_push($array, $row);
    	}
    	$this->view->static = $array;
    	
    }
    
    public function getAction(){
    	
    }
    
    public function putAction(){
    	$material = new Default_Model_Material();
    	$material->setOptions($this->getRequest()->getParams());
    	$mapper = new Default_Model_MaterialMapper();
    	$result = $mapper->update($material);
    	if(is_numeric($result)){
    		$this->getResponse()->setHttpResponseCode(201)->appendBody("success~?");
    	}else{
    		$this->getResponse()->setHttpResponseCode(422)->appendBody("server error");
    	}
    }
    
    public function postAction(){
        $material = new Default_Model_Material();
    	$material->setOptions($this->getRequest()->getParams());
    	$mapper = new Default_Model_MaterialMapper();
    	$result = $mapper->save($material);
    	if(is_numeric($result)){
    		$this->getResponse()->setHttpResponseCode(201)->appendBody("success~?.....");
    	}else{
    		$this->getResponse()->setHttpResponseCode(422)->appendBody("server error");
    	}
    }
    
    public function deleteAction(){
    	 $id = $this->getRequest()->getParam("id");
    	 if(isset($id)){
    	 	$mapper = new Default_Model_MaterialMapper();
    	 	$result = $mapper->delete($this->getRequest()->getParam("id"));
    	 	if($result=="success"){
    	 		$this->getResponse()->setHttpResponseCode(200)->appendBody("delete success");
    	 	}else{
    	 		$this->getResponse()->setHttpResponseCode(422)->appendBody($result);
    	 	}
    	 }
    }   
    public function headAction(){
    	
    }

    
   

}