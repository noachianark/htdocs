<?php
require_once 'Zend/Db/Table/Abstract.php';
class Default_Model_DbTable_Material extends Zend_Db_Table_Abstract{
	protected $_name="materials";
	protected $_primary="id";
	protected $_dependentTables = array("Default_Model_DbTable_DishMaterial");
}