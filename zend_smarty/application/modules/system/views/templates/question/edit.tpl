<input type="hidden" id="question_id" value="{~$question->getQid()~}"/>
								<form class="form-horizontal" id="edit-profile">
									<fieldset>
										<div class="control-group">
											<label for="sequence" class="control-label">序号 </label>
											<div class="controls">
												<input type="text" value="{~$question->getSequence()~}" id="sequence" class="input-small">											
											</div>
										</div>
										<div class="control-group">
											<label for="question_dimension" class="control-label">选择所属维度</label>
											<div class="controls">
											    <div class="btn-group" style="width:160px;">
													<select class="input-large" id="dimen_id">
												    	{~foreach $dimension_list as $dimen~}
															{~if $question->getDimension_id() eq $dimen['id']~}
																<option selected="true" value="{~$dimen['id']~}">{~$dimen['name']~}</option>
															{~else~}
																<option value="{~$dimen['id']~}">{~$dimen['name']~}</option>		
															{~/if~}
																										
														{~/foreach~}														
													</select>
											    </div>											
											</div>
										</div>
										<div class="control-group">
											<label for="question" class="control-label">题干</label>
											<div class="controls">
												<textarea style="width:496px;" type="text" value="" id="question" class="input-very-large">{~$question->getContent()~}</textarea>
											</div>
										</div>
										<div id="differ_answer_group" >
											<input id="count" type="hidden" value="{~count($question->getChoices())~}">
											{~foreach $question->getChoices() as $key=>$value~}
											<div class="control-group">
												<label for="answer" class="control-label">备选答案</label>
												<div class="controls choice">
													<input type="text" value="{~$value['answer']~}"  class="input-very-large answer">
													权重：
													<input type="text" value="{~$value['weight']~}" class="input-small weight">
												</div>
											</div>
											{~/foreach~}	
											{~assign var="count" value=6-count($question->getChoices())~}
											{~if ($count)>0~}
												{~for $item=1 to $count~}
													<div class="control-group">
														<label for="answer" class="control-label">备选答案</label>
														<div class="controls choice">
															<input type="text" value=""  class="input-very-large answer">
															权重：
															<input type="text" value="0" class="input-small weight">
														</div>
													</div>
												{~/for~}
											{~/if~}

										</div>
									</fieldset>
								</form>	