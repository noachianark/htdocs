<?php
class System_Model_Dimension{
	protected $id;
	protected $gid;
	protected $name;
	protected $description;
	protected $rules;
	protected $boundary;
	protected $lowpass;
	protected $decisive;
	protected $high;
	protected $low;
	
	public function __construct(array $options = null)
	{
		if (is_array($options)) {
			$this->setOptions($options);
		}
	}
	 
	public function setOptions(array $options)
	{
		$methods = get_class_methods($this);
		foreach ($options as $key => $value) {
			$method = 'set' . ucfirst($key);
			if (in_array($method, $methods)) {
				$this->$method($value);
			}
		}
		return $this;
	}
	
	public function toJson(){
		return json_encode($this->toArray());
	}
	
	public function toArray(){
		$array=array();
		foreach ($this as $key => $value) {
			if($value!=null){
				$array[$key] = $value;
			}
		}
		return $array;
	}
	/**
	 * @return the $id
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * @return the $gid
	 */
	public function getGid() {
		return $this->gid;
	}

	/**
	 * @param field_type $gid
	 */
	public function setGid($gid) {
		$this->gid = $gid;
	}

	/**
	 * @return the $name
	 */
	public function getName() {
		return $this->name;
	}

	/**
	 * @return the $description
	 */
	public function getDescription() {
		return $this->description;
	}

	/**
	 * @return the $rules
	 */
	public function getRules() {
		return $this->rules;
	}

	/**
	 * @return the $min
	 */
	public function getBoundary() {
		return $this->boundary;
	}

	/**
	 * @return the $max
	 */
	public function getLowpass() {
		return $this->lowpass;
	}

	/**
	 * @param field_type $id
	 */
	public function setId($id) {
		$this->id = $id;
	}

	/**
	 * @param field_type $name
	 */
	public function setName($name) {
		$this->name = $name;
	}

	/**
	 * @param field_type $description
	 */
	public function setDescription($description) {
		$this->description = $description;
	}

	/**
	 * @param field_type $rules
	 */
	public function setRules($rules) {
		$this->rules = $rules;
	}

	/**
	 * @param field_type $min
	 */
	public function setBoundary($min) {
		$this->boundary = $min;
	}

	/**
	 * @param field_type $max
	 */
	public function setLowpass($max) {
		$this->lowpass = $max;
	}
	/**
	 * @return the $desicive
	 */
	public function getDecisive() {
		return $this->decisive;
	}

	/**
	 * @param field_type $desicive
	 */
	public function setDecisive($decisive) {
		$this->decisive = $decisive;
	}
	/**
	 * @return the $high
	 */
	public function getHigh() {
		return $this->high;
	}

	/**
	 * @return the $low
	 */
	public function getLow() {
		return $this->low;
	}

	/**
	 * @param field_type $high
	 */
	public function setHigh($high) {
		$this->high = $high;
	}

	/**
	 * @param field_type $low
	 */
	public function setLow($low) {
		$this->low = $low;
	}





}

?>