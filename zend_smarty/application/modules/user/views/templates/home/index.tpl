{~include file="header2.tpl"~}
<style>
	.fixed_container{
		width:270px;
		float:right;
	}
</style>
<script>
	$(function(){
		rate=parseInt(parseInt($("#done").text())/parseInt($("#records").text())*100);
		$("#loading").css("width",rate+"%");
		console.log("ca",rate+"%");
	});
</script>
			<div class="span9" style="margin-left:30px;">
				<h1 class="page-title">
					<i class="icon-smile 2x"></i>
					<span onclick="" style="cursor:pointer;">用户首页</span>
				</h1>
				<div style="min-height:400px;">
				
					<div class="widget  widget-table">		
						<div class="widget-header">
							<i class="icon-th-list"></i>
							<h3>量表列表</h3>
						</div> <!-- /widget-header -->
						<div class="widget-content">					
							<table class="table table-striped table-bordered">
								<thead>
									<th>量表名称</th>
									<th>描述</th>
									<th>时间限制</th>
									<th> </th>
								</thead>
								<tbody>
									{~foreach $gauges->rows as $gauge~}
										<tr>
											<td>{~$gauge->getGauge_name()~}</td>
											<td style="width:300px;">{~$gauge->getDescription()~}</td>
											<td style="width:65px;">
												{~if $gauge->getTime_limit() eq 0~}
													<span class="label label-info">不限制</span>
												{~else~}
													<span class="label label-info"><i class="icon-time"></i>{~$gauge->getTime_limit()~}分钟</span>
												{~/if~}
											</td>
											<td class="action-td" style="width:40px;">
												<input type="hidden" value="{~$gauge->getGid()~}" class="gid">
												{~if $gauge->getManual() eq 1~}
												<a class="btn btn-success" href="/user/home/gauge/id/{~$gauge->getGid()~}" title="进入测试">
													<i class="icon-pencil"></i>								
												</a>	
												{~else~}
												<a class="btn  disabled" title="数据还未导入或未完成测试">
													<i class="icon-minus-sign"></i>								
												</a>		
												{~/if~}
				
											</td>
										</tr>
									{~/foreach~}
								</tbody>
							</table>
						</div> <!-- /widget-content -->
					</div>

					
				</div>
			</div>
			<div class="fixed_container">
				<div style="position:fixed;width:270px;">
					<div class="account-container">
					

					
						<div class="account-details">
						
							<span class="account-name" style="font-size:25px;margin-bottom:10px;"><a class="label label-primary" style="font-size:14px;margin-right:10px;margin-left:30px;">姓名</a>{~$username~}</span>
							
							<span class="account-role" style="font-size:16px;margin-bottom:10px;"><a class="label label-primary" style="font-size:14px;margin-right:10px;">身份证号</a>{~$id_card~}</span>
							
							<span class="account-role" style="font-size:16px;">
								<a class="label label-important" style="font-size:14px;">请您仔细核对身份证以及姓名防止误答</a>
							</span>
						
						</div> <!-- /account-details -->
					
					</div> <!-- /account-container -->

					<hr>
					<div class="alert alert-info">
						完成评测需要您进行总共<span class="label label-warning" id="records">{~$gauges->records~}</span>项测试
						<br>您已经完成了其中的<span class="label label-warning" id="done">{~$gauges->done~}</span>项<br>
						<strong>当前总进度</strong>
						    <div class="progress progress-striped active progress-info" style="margin-bottom:0px;">
						    	<div class="bar" id="loading"></div>
						    </div>
					</div>	
					<hr>
					<div class="alert alert-success">
						<strong>评测说明：</strong><br>
								驾驶适性是职业适性的一种，是指驾驶员安全、有效地驾驶汽车所必须具备的生理、心理素质的最低特征，其评测主要是采用心理学、医学和计算机检测手段，通过建立驾驶员适应性检测设备系统，来检测和评价驾驶员所具备的完成驾驶工作的素质和能力，主要用于对驾驶员生理、心理素质的检测。
					</div>	
				</div>
			</div>
{~include file="footer.tpl"~}