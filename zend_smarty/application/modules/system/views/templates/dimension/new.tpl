<input id="dimension_id" type="hidden" value="{~$dimension->getId()~}">
<form class="form-horizontal" id="edit-profile2">
	<fieldset>
		<div class="control-group">											
			<label for="dimension_name" class="control-label">维度名称</label>
			<div class="controls">
				<input type="text" id="dimension_name" class="input-very-large" value="{~$dimension->getName()~}">
			</div> <!-- /controls -->				
		</div> <!-- /control-group -->
		
		<div class="control-group">											
			<label for="dimension_desc" class="control-label">维度描述</label>
			<div class="controls">
				<textarea type="text" id="dimension_desc" class="input-very-large">{~$dimension->getDescription()~}</textarea>
			</div> <!-- /controls -->				
		</div> <!-- /control-group -->												

		<div class="control-group">											
			<label for="lowpass" class="control-label">低分适合</label>
			<div class="controls">
				
				{~if $dimension->getLowpass() eq 1~}
					<label class="radio">
						<input type="radio" value="1"  class="question_input" name="lowpass" checked="true">是
					</label>
					<label class="radio">
						<input type="radio" value="0" class="question_input" name="lowpass">否
					</label>
				{~else~}
					<label class="radio">
						<input type="radio" value="1"  class="question_input" name="lowpass">是
					</label>
					<label class="radio">
						<input type="radio" value="0" class="question_input" name="lowpass" checked="true">否
					</label>
				{~/if~}			
			</div> <!-- /controls -->				
		</div> <!-- /control-group -->		

		<div class="control-group">											
			<label for="boundary" class="control-label">评判界限</label>
			<div class="controls">
				<input type="text" id="boundary" class="input-small" value="{~$dimension->getBoundary()~}">
			</div> <!-- /controls -->
			<p class="help-block">以该界限判断维度总分的高低，大于等于该值判定为高分。</p>			
		</div> <!-- /control-group -->

		<div class="control-group">											
			<label for="dimension_desc" class="control-label">高分评价</label>
			<div class="controls">
				<textarea type="text" id="high" class="input-very-large">{~$dimension->getHigh()~}</textarea>
			</div> <!-- /controls -->				
		</div> <!-- /control-group -->	
		
		<div class="control-group">											
			<label for="dimension_desc" class="control-label">低分评价</label>
			<div class="controls">
				<textarea type="text" id="low" class="input-very-large">{~$dimension->getLow()~}</textarea>
			</div> <!-- /controls -->				
		</div> <!-- /control-group -->	
				
		<div class="control-group">											
			<label for="decisive" class="control-label">是否属于决定性因素</label>
			<div class="controls">
				
				{~if $dimension->getDecisive() eq 1~}
					<label class="radio">
						<input type="radio" value="1"  class="question_input" name="decisive" checked="true">是
					</label>
					<label class="radio">
						<input type="radio" value="0" class="question_input" name="decisive">否
					</label>
				{~else~}
					<label class="radio">
						<input type="radio" value="1"  class="question_input" name="decisive">是
					</label>
					<label class="radio">
						<input type="radio" value="0" class="question_input" name="decisive" checked="true">否
					</label>				
				
				{~/if~}			
			</div> <!-- /controls -->				
		</div> <!-- /control-group -->		
		
		<div class="control-group">											
			<label for="dimension_attributes" class="control-label">维度属性设置</label>
			<div class="controls">
				<textarea style="height:300px;font-size:20px;letter-spacing:2px;line-height:25px;" type="text" id="dimension_attributes" class="input-very-large">{~$dimension->getRules()~}</textarea>
				<p class="help-block">
					<div class="alert alert-info" style="width:400px;">
			            <strong>填写说明：</strong><br>
				    	填写例子：[<br>
						{"min":0,"max":1,"alter":1},{"min":2,"max":3,"alter":2},<br>
						{"min":0,"max":1,"alter":1},{"min":2,"max":3,"alter":3}<br>
						]<br>
						该例子表明了一个维度所允许的范围，分别以min和max作为区间，当该值处于其中一个区间时,系统取alter值作为转换值。
			        </div>											
				</p>
			</div> <!-- /controls -->				
		</div> <!-- /control-group -->								
		<br>
	</fieldset>
</form>