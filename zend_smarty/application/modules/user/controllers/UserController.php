<?php

class User_UserController extends Zend_Rest_Controller
{

    public function init()
    {
        /* Initialize action controller here */
    	$this->_helper->viewRenderer->setNoRender(true);
    }

    public function indexAction()
    {
    	//is_seach
    	$this->_helper->viewRenderer->setNoRender(false);
    	$page=$this->getRequest()->getParam("page");
    	$mapper=new User_Model_UserMapper();
    	$where=array(
    			"keyword"=>$this->getRequest()->getParam("keyword"),
    			"sex"=>$this->getRequest()->getParam("sex"),
    			"finish"=>$this->getRequest()->getParam("finish")
    			);
    	$result=$mapper->findAll($page?$page:1,30,$where);
    	$this->view->users=$result;
    	$this->view->page=$page;
    	

    }
   
   public function headAction(){
		
   }
   
   public function getAction(){
   	  $this->_helper->viewRenderer->setNoRender(false);
	  $userid=$this->getRequest()->getParam("id");
   }
   
   public function putAction(){
   	
   }
   
   public function postAction(){
   	  $user=new User_Model_User($this->getRequest()->getParams());
   	  $mapper=new User_Model_UserMapper();
   	  $result=$mapper->save($user);
   	  if(is_numeric($result)){
   	  	 $this->getResponse()->setHttpResponseCode(201)->appendBody($result);
   	  }else{
   	  	 $this->getResponse()->setHttpResponseCode(422)->appendBody($result);
   	  }
   }
   
   public function deleteAction(){
   	  $id = $this->getRequest()->getParam("id");
      $mapper = new User_Model_UserMapper();
      $result = $mapper->delete($id);
      if($result===true){
        $this->getResponse()->setHttpResponseCode(200)->appendBody($result);
      }else{
        $this->getResponse()->setHttpResponseCode(422)->appendBody($result);
      }
   }
   
}

