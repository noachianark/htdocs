<?php
class User_Model_BasicinfoMapper{
	protected $_dbTable;
	
	public function setDbTable($dbTable)
	{
		if (is_string($dbTable)) {
			$dbTable = new $dbTable();
		}
		if (!$dbTable instanceof Zend_Db_Table_Abstract) {
			throw new Exception('Invalid table data gateway provided');
		}
		$this->_dbTable = $dbTable;
		return $this;
	}
	
	public function getDbTable()
	{
		if (null === $this->_dbTable) {
			$this->setDbTable('User_Model_DbTable_Basicinfo');
		}
		return $this->_dbTable;
	}
	

	
	public function save(User_Model_Basicinfo $user)
	{
		try{
			$result=$this->getDbTable()->insert($user->toArray());
		}catch (Exception $e){
			return Utils_SQLErrorMsg::formatException($e);
		}
		return (int)$result;

	}
	
	public function update(User_Model_User $user){
		

	}
	
    public function find($id, User_Model_Basicinfo $user,array $filter=null)
    {
		try{
			$result=$this->getDbTable()->find($id)->current();
		}catch(Exception $e){
			return $e;
		}
		if(count($result)){
			$user->setOptions($result->toArray());
		}else{
			return false;
		}
    }
    

    
    public function delete($id){
    	if($id===null || $id==0){
    		return "ID不能为空";
    	}
    	$result = $this->getDbTable()->find($id);
        if (0 == count($result)) {
            return "没有找到该用户";
        }
        $result=$this->getDbTable()->update(array('actived'=>0), array("userid=? "=>$id));
        return isset($result)?true:false;
    }
    


}