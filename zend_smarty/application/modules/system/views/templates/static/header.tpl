<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>{~$title~}</title>
<link rel="stylesheet" type="text/css" href="/css/toastr.min.css"/>

<link href="/css/bootstrap.min.css" rel="stylesheet" />
<link href="/css/bootstrap-responsive.min.css" rel="stylesheet" />

<link href="/css/fonts.css" rel="stylesheet" />
<link href="/css/font-awesome.css" rel="stylesheet" />

<link href="/css/adminia.css" rel="stylesheet" /> 
<link href="/css/adminia-responsive.css" rel="stylesheet" /> 
<script src="/scripts/jquery-1.8.3.js"></script>
    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
</head>

<body>
<style>
	.input-very-large{
		width:350px;
	}
	.display_off{
		display:none;
	}
</style>
<script>
	function validateLength(obj,min,max,tip){
		if(obj.val().length>max || obj.val().length<min){
			toastr.warning(tip+"的长度必须在"+min+"到"+max+"之间","提示");
			return false;
		}
		return true;
	}
	function validateRegexp(obj,regexp,tip){
		if(!(regexp.test(obj.val()))){
			toastr.warning(tip,"提示");
			return false;
		}
		return true;
	}
	function validateEquals(value1,value2,tip){
		if(value1.val()!=value2.val()){
			toastr.warning(tip,"提示");
			return false;
		}
		return true;
	}
	function navigate(obj){
		$("#main-nav li").removeClass("active");
		//$(obj).addClass("active");
	}
	$(function(){
		$(".change_pass").click(function(){
			window.location.href="/system/admin/";
		});
	});
</script>
<div class="navbar navbar-fixed-top">
	
	<div class="navbar-inner">
		
		<div class="container">
			
			
			<a href="/system" class="brand">客运汽车驾驶员心理素质测评系统</a>
			
			<div class="nav-collapse in" style="height: 0px;">
			
				<ul class="nav pull-right">
					<li>
						<a onclick="$.get('/index/logout').promise().done(function(){window.location.href='/'})"><span class="badge badge-warning">注销</span></a>
					</li>
					
					<li class="divider-vertical"></li>
					
					<li class="dropdown">
						
						<a href="#" class="dropdown-toggle " data-toggle="dropdown">
							{~$admin_name~} <b class="caret"></b>							
						</a>
						
						<ul class="dropdown-menu">
							<li>
								<a href="./account.html"><i class="icon-user"></i> 账号设置  </a>
							</li>
							
							<li>
								<a class="change_pass"><i class="icon-lock"></i> 修改密码</a>
							</li>
							
							<li class="divider"></li>
							
							<li>
								<a onclick="$.get('/index/logout').promise().done(function(){window.location.href='/'})"><i class="icon-off"></i> 注销</a>
							</li>
						</ul>
					</li>
				</ul>
				
			</div> <!-- /nav-collapse -->
			
		</div> <!-- /container -->
		
	</div> <!-- /navbar-inner -->
	
</div>	
	

<div id="content">
	<div class="container">
		<div class="row">
			<div class="span3">
				
				<div class="account-container">
				

				
					<div class="account-details">
					
						<span class="account-name">{~$admin_name~}</span>
						
						<span class="account-role">系统管理员</span>
						
						<span class="account-actions">
							<a href="javascript:;">账户信息</a> |
							
							<a href="javascript:;"  class="change_pass">修改密码</a>
						</span>
					
					</div> <!-- /account-details -->
				
				</div> <!-- /account-container -->
				
				<hr>
				
				<ul class="nav nav-tabs nav-stacked" id="main-nav">
					
					<li id="gauge_navi">
						<a href="/system/gauge" onclick="navigate(this)">
							<i class="icon-th-list"></i>
							量表管理 		
						</a>
					</li>
					
					<li id="profile_navi">
						<a href="/system/profile" onclick="navigate(this)">
							<i class="icon-pushpin"></i>
							档案管理
						</a>
					</li>
					
					<li id="setting_navi">
						<a href="/system/setting" onclick="navigate(this)">
							<i class="icon-user"></i>
							系统设置	
						</a>
					</li>
					
					
				</ul>	
				
				<hr>
				
				<div class="sidebar-extra">
					<p>系统管理主要分为三个部分，量表管理、测评人员档案管理以及系统设置。</p>
				</div> <!-- .sidebar-extra -->
				
				<br>
		
			</div>
			<div class="span9" id="con">

				<!--这里开始填写各种片段 -->

