<?php
require_once 'Zend/Db/Table/Abstract.php';
class Default_Model_DbTable_DishMaterial extends Zend_Db_Table_Abstract{
	protected $_name="dishes_materials";
	protected $_referenceMap = array(
		"Dish" => array(
			"columns" 		=>	array("dish_id"),
			"refTableClass" =>  "Default_Model_DbTable_Dish",
			"refColumns" 	=>  array("id"),
			'onDelete'          => self::CASCADE
		),
		"Material" => array(
			"columns"		=>	array("material_id"),
			"refTableClass"	=>	"Default_Model_DbTable_Material",
			"refColumns"	=>	array("id"),
			'onDelete'          => self::CASCADE
		)
	);
}