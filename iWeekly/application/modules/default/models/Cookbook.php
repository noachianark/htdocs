<?php
class Default_Model_Cookbook{
	protected $id;
	protected $day;
	protected $time;
	protected $dish_id;
	protected $parameter;
	
	
	public function __construct(array $options = null)
	{
		if (is_array($options)) {
			$this->setOptions($options);
		}
	}
	
	public function setOptions(array $options)
	{
		$methods = get_class_methods($this);
		foreach ($options as $key => $value) {
			$method = 'set' . ucfirst($key);
			if (in_array($method, $methods)) {
				$this->$method($value);
			}
		}
		return $this;
	}
	
	public function toJson(){
		return json_encode($this->toArray());
	}
	
	public function toArray(){
		$array=array();
		foreach ($this as $key => $value) {
			if($value!=null){
				$array[$key] = $value;
			}
		}
		return $array;
	}
	/**
	 * @return the $id
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * @return the $day
	 */
	public function getDay() {
		return $this->day;
	}

	/**
	 * @return the $time
	 */
	public function getTime() {
		return $this->time;
	}

	/**
	 * @return the $dish_id
	 */
	public function getDish_id() {
		return $this->dish_id;
	}

	/**
	 * @return the $parameter
	 */
	public function getParameter() {
		return $this->parameter;
	}

	/**
	 * @param field_type $id
	 */
	public function setId($id) {
		$this->id = $id;
	}

	/**
	 * @param field_type $day
	 */
	public function setDay($day) {
		$this->day = $day;
	}

	/**
	 * @param field_type $time
	 */
	public function setTime($time) {
		$this->time = $time;
	}

	/**
	 * @param field_type $dish_id
	 */
	public function setDish_id($dish_id) {
		$this->dish_id = $dish_id;
	}

	/**
	 * @param field_type $parameter
	 */
	public function setParameter($parameter) {
		$this->parameter = $parameter;
	}











	
}

?>