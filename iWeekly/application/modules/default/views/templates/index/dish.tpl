{~include file="header.tpl"~}
<div class="container">
        <div class="row">
            <div id="backToMain" class="backBtn btn-hg col-md-2">
                <span class="glyphicon glyphicon-chevron-left"></span> 返回
            </div>    
        </div>
		
		<div class="row"><h5>菜品设定</h5></div>

</div>
{~include file="js.tpl"~}


<script>
	$(function(){
		
		$.get("/material").done(function(data){
			$("#material_table").html(data);
		}).fail(faultHandler);
		
		$.get("/dish").done(function(data){
			$("#dish_table").html(data);
		}).fail(faultHandler);
		
		$("#addMaterialBtn").on("click",function(){
			$.post("/material",$("#material_form").serialize()).done(function(data){
				$.get("/material").done(function(data){
					$("#material_table").html(data);
					$("#material_form")[0].reset();
				}).fail(faultHandler);
			}).fail(faultHandler);
		});
		
		
		$("#backToManage").on("click",function(){
			window.location.href="/index/manage";
		});

		
		
	});
</script>

{~include file="footer.tpl"~}