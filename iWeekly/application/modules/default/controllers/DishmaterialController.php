<?php
class DishmaterialController extends Zend_Rest_Controller
{

    public function init()
    {
        /* Initialize action controller here */
    	//$this->view->hello="this is default index ";
    	$this->_helper->viewRenderer->setNoRender(true);
    }

    public function indexAction(){
    	
    }
 
    public function getAction(){
    	
    }
    
    public function putAction(){
		
    }
    
    public function postAction(){//新建
		$cascade = new Default_Model_DishMaterial($this->getRequest()->getParams());
		$mapper = new Default_Model_DishMaterialMapper();
		$result = $mapper -> save($cascade);
		if(is_numeric($result)){
			$this->getResponse()->setHttpResponseCode(201)->appendBody("success");
		}else{
			$this->getResponse()->setHttpResponseCode(422)->appendBody($result);
		}
		
    }
    
    public function deleteAction(){
		$ids = $this->getRequest()->getParam("id");
		$array = explode("_", $ids);
		$mapper = new Default_Model_DishMaterialMapper();
		$result = $mapper -> delete($array[1], $array[0]);
		if($result == "success"){
			$this->getResponse()->setHttpResponseCode(200);
		}else{
			$this->getResponse()->setHttpResponseCode(422)->appendBody($result);
		}
    }
       
    public function headAction(){
    	
    }


}