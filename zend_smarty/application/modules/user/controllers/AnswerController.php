<?php
class User_AnswerController extends Zend_Rest_Controller
{

    public function init()
    {
//     	$this->view->admin_name=Zend_Auth::getInstance()->getIdentity()->username;
//     	$this->view->title="系统管理->量表管理";
 		$this->_helper->viewRenderer->setNoRender(true);
    }

    public function indexAction()
    {
    	//$this->_helper->viewRenderer->setNoRender(false);
    }
    
 
    
    public function getAction(){

    }
    
    
    
    public function postAction(){
		echo "post";
    	
    }

    
    public function deleteAction(){
    	echo "delete one or more";
    }
    public function putAction(){
    	$gauge_id=$this->getRequest()->getParam("gid");
    	$user_id=$this->getRequest()->getParam("id");
    	$name=$this->getRequest()->getParam("name");
    	$score=$this->getRequest()->getParam("score");
    	$passed=$this->getRequest()->getParam("passed");
    	$conc=$this->getRequest()->getParam("conclusion");
    	$sex=$this->getRequest()->getParam("sex");
    	
    	$conclusion=new stdClass();
    	$conclusion->passed=$passed;
    	$conclusion->comment=array();
    	
    	$result=new User_Model_Result();
    	$result->setScore($score);
    	$result->setDimension_name($name);
    	$result->setConclusion($conc);
    	$result->setPassed($passed);
        $result->setDecisive(true);
    	
    	
    	array_push($conclusion->comment, $result);
    	$answer=new User_Model_Answer();
    	$answer->setUser_id($user_id);
    	$answer->setGauge_id($gauge_id);
    	$answer->setResult($conclusion);
    	
    	$answerMapper=new User_Model_AnswerMapper();
    	$re=$answerMapper->save($answer);

    	$stdClass=$answerMapper->findComplished($user_id);
        if($stdClass->speed===true && $stdClass->response===true && $stdClass->deep===true && $stdClass->sight===true){
    	//if($stdClass->speed===true && $stdClass->response===true && $stdClass->deep===true && $stdClass->hand===true){
    		//有问题，需要判断是否完成试卷。
    		$userMapper=new User_Model_UserMapper();
    		$user=new User_Model_User();
    		$userMapper->find($user_id, $user);
    		$gaugeMapper=new System_Model_GaugeMapper();
    		$result=$gaugeMapper->findPublished($user->getSex(), $user->getId());
    		
    		if(!count($result->rows)){
    			$date=new DateTime();
    			$user->setClear_time($date->format('Y-m-d H:i:s'));
    			//生成报告。
    			$user->setFinish(true);
    			$ans=$answerMapper->findByUserid(Zend_Auth::getInstance()->getIdentity()->id);
    			$pass=true;
    			foreach($ans as $an){
    				$con=$an->getResult();
    				if($con->passed===false){
    					$pass=false;
    				}
    			}
    			
    			
    			$report=new User_Model_Report();
    			$report->setUser_id($user_id);
    			$report->setPassed($pass);
    			 
    			$reportMapper=new User_Model_ReportMapper();
    			$reportMapper->save($report);    			
    		}
    		$user->setState(true);
    		
    		$u=$userMapper->update($user);
    		if($u){
    			$this->getResponse()->setHttpResponseCode(200);
    		}
    	}else{
    		$this->getResponse()->setHttpResponseCode(201)->appendBody("success");
    	}
    }
    public function headAction(){
    	 
    }

    


}