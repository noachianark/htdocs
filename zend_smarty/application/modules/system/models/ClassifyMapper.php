<?php
class System_Model_ClassifyMapper {
	protected $_dbTable;
	
	public function setDbTable($dbTable)
	{
		if (is_string($dbTable)) {
			$dbTable = new $dbTable();
		}
		if (!$dbTable instanceof Zend_Db_Table_Abstract) {
			throw new Exception('Invalid table data gateway provided');
		}
		$this->_dbTable = $dbTable;
		return $this;
	}
	public function getDbTable(){
		if (null === $this->_dbTable) {
			$this->setDbTable('System_Model_DbTable_Classify');
		}
		return $this->_dbTable;
	}
	public function save(System_Model_Classify $classify){
		try{
			$result=$this->getDbTable()->insert($classify->toArray());
		}catch (Exception $e){
			return Utils_SQLErrorMsg::formatException($e);
		}
		return (int)$result;
	}
	public function findAll($perPage=20,$curPage=1){
		$rows=$this->getDbTable()->fetchRow("SELECT FOUND_ROWS()");
		$select=$this->getDbTable()->select()->from("classify")->order("cid")
							->limitPage($curPage, $perPage);
		$data=array();
		try{
			$data=$this->getDbTable()->fetchAll($select)->toArray();
		}catch(Exception $e){
			return $e;
		}
		return $data;
	}
	
}
