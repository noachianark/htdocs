<?php

class ErrorController extends Zend_Controller_Action
{
	
	public function indexAction(){
		$this->errorAction();
	}
	
	
	public function clientAction(){
		$this->_helper->ViewRenderer->setNoRender(true);
		$this->getResponse()
		->setHttpResponseCode(403)
		->appendBody("No Valid Client");
	}
	
	public function accessAction()
	{
		
		if($this->getRequest()->isXmlHttpRequest()){
			$this->_helper->ViewRenderer->setNoRender(true);
			$this->getResponse()
                    ->setHttpResponseCode(403)
                    ->appendBody("您尚未登录或登录已经超时请重新登录");
			return;
		}
		$this->view->params=$this->getRequest()->getParams();
		$this->view->title="您的权限已经过期，请重新登录";
	}

    public function errorAction()
    {
        $errors = $this->_getParam('error_handler');
        
        if (!$errors || !$errors instanceof ArrayObject) {
            $this->view->message = 'You have reached the error page';
            $this->view->title="您的权限已经过期，请重新登录";
            return;
        }
        
        switch ($errors->type) {
            case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_ROUTE:
            case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_CONTROLLER:
            case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_ACTION:
                // 404 error -- controller or action not found
                $this->getResponse()->setHttpResponseCode(404);
                $priority = Zend_Log::NOTICE;
                $this->view->message = 'Page not found!@!@!@!@!';
                break;
            default:
                // application error
                $this->getResponse()->setHttpResponseCode(500);
                $priority = Zend_Log::CRIT;
                $this->view->message = 'Application error';
                break;
        }
        
        // Log exception, if logger available
        $log=$this->getLog();
        if ($log) {
            $log->log($this->view->message, $priority, $errors->exception);
            $log->log('Request Parameters', $priority, $errors->request->getParams());
        }
        
        // conditionally display exceptions
        if ($this->getInvokeArg('displayExceptions') == true) {
            $this->view->exception = $errors->exception;
        }
        
        $this->view->request   = $errors->request;
    }

    public function getLog()
    {
        $bootstrap = $this->getInvokeArg('bootstrap');
        if (!$bootstrap->hasResource('Log')) {
            return false;
        }
        $log = $bootstrap->getResource('Log');
        return $log;
    }


}

