<div class="alert alert-info">
	<a class="close" data-dismiss="alert">×</a>
								<form class="form-horizontal" id="edit-profile">
									<fieldset>
										
										<div class="control-group">
											<label for="sequence" class="control-label">序号 </label>
											<div class="controls">
												<input type="text" value="{~(int)$records+1~}" id="sequence" class="input-small">											
											</div>
										</div>
										<div class="control-group">
											<label for="question_dimension" class="control-label">选择所属维度</label>
											<div class="controls">
											    <div class="btn-group" style="width:160px;">
													<select class="input-large" id="dimen_id">
												    	{~foreach $dimension_list as $dimen~}
															<option value="{~$dimen['id']~}">{~$dimen['name']~}</option>													
														{~/foreach~}														
													</select>
											    </div>											
											</div>
										</div>
	
										
										<div class="control-group">
											<label for="question" class="control-label">题干</label>
											<div class="controls">
												<textarea style="width:496px;" type="text" value="" id="question" class="input-very-large"></textarea>
											</div>
										</div>
		
										<div id="differ_answer_group" >
												{~for $item=1 to 6~}
													<div class="control-group">
														<label for="answer" class="control-label">备选答案</label>
														<div class="controls choice">
															<input type="text" value=""  class="input-very-large answer">
															权重：
															<input type="text" value="0" class="input-small weight">
														</div>
													</div>
												{~/for~}
										</div>
										
										
										<br>
										
											
									<div class="form-actions">
										<a class="btn btn-primary" onclick="saveQuestion()">添加</a> 
									</div> <!-- /form-actions -->
									</fieldset>
								</form>	

</div>