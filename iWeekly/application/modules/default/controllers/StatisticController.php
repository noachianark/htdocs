<?php
class StatisticController extends Zend_Rest_Controller
{

    public function init()
    {
        /* Initialize action controller here */
    	//$this->view->hello="this is default index ";
    	$this->_helper->viewRenderer->setNoRender(true);
    }



    public function indexAction()
    {
    	$this->_helper->viewRenderer->setNoRender(false);
		$mapper=new Default_Model_CookbookMapper();
		$result = $mapper->findStatistic();
		$this->view->statistics = $result; 
		//$this->render("index.tpl");
		//$this->getResponse()->setHttpResponseCode(200)->appendBody(json_encode($result));
    }
    
    public function getAction(){
        $this->_helper->viewRenderer->setNoRender(false);
        $mapper=new Default_Model_CookbookMapper();
        $result = $mapper->findStatistic($this->getRequest()->getParam("day"));
        $this->view->statistics = $result; 
        $this->getResponse()->setHttpResponseCode(200)->appendBody(json_encode($result));
        $this->render("index");
    }
    
    public function putAction(){

    }
    
    public function postAction(){

    }
    
    public function deleteAction(){

    }
    
    public function headAction(){
    	 
    }
    
   

}