<?php
class User_Model_Basicinfo{
	protected $userid;
	protected $phone;
	protected $age;
	protected $name;
	protected $is_driver;
	protected $driver_age;
	protected $lisence;
	protected $car_type;
	protected $smoke_drive;
	protected $alcohol;
	protected $is_accident;
	protected $unit;
	protected $is_complished;
	
	
	public function __construct(array $options = null)
	{
		if (is_array($options)) {
			$this->setOptions($options);
		}
	}
	
	public function setOptions(array $options)
	{
		$methods = get_class_methods($this);
		foreach ($options as $key => $value) {
			$method = 'set' . ucfirst($key);
			if (in_array($method, $methods)) {
				$this->$method($value);
			}
		}
		return $this;
	}
	
	public function toJson(){
		return json_encode($this->toArray());
	}
	
	public function toArray(){
		$array=array();
		foreach ($this as $key => $value) {
			if($value!=null){
				$array[$key] = $value;
			}
		}
		return $array;
	}
	/**
	 * @return the $userid
	 */
	public function getUserid() {
		return $this->userid;
	}

	/**
	 * @param field_type $userid
	 */
	public function setUserid($userid) {
		$this->userid = $userid;
	}

	/**
	 * @return the $phone
	 */
	public function getPhone() {
		return $this->phone;
	}

	/**
	 * @param field_type $phone
	 */
	public function setPhone($phone) {
		$this->phone = $phone;
	}

	/**
	 * @return the $age
	 */
	public function getAge() {
		return $this->age;
	}

	/**
	 * @param field_type $age
	 */
	public function setAge($age) {
		$this->age = $age;
	}

	/**
	 * @return the $nation
	 */
	public function getUnit() {
		return $this->unit;
	}

	/**
	 * @param field_type $nation
	 */
	public function setUnit($unit) {
		$this->unit = $unit;
	}

	/**
	 * @return the $height
	 */
	public function getName() {
		return $this->name;
	}

	/**
	 * @param field_type $height
	 */
	public function setName($name) {
		$this->name = $name;
	}

	/**
	 * @return the $weight
	 */
	public function getIs_accident() {
		return $this->is_accident;
	}

	/**
	 * @param field_type $weight
	 */
	public function setIs_accident($is_accident) {
		$this->is_accident = $is_accident;
	}


	/**
	 * @return the $is_driver
	 */
	public function getIs_driver() {
		return $this->is_driver;
	}

	/**
	 * @param field_type $is_driver
	 */
	public function setIs_driver($is_driver) {
		$this->is_driver = $is_driver;
	}

	/**
	 * @return the $driver_age
	 */
	public function getDriver_age() {
		return $this->driver_age;
	}

	/**
	 * @param field_type $driver_age
	 */
	public function setDriver_age($driver_age) {
		$this->driver_age = $driver_age;
	}

	/**
	 * @return the $lisence
	 */
	public function getLisence() {
		return $this->lisence;
	}

	/**
	 * @param field_type $lisence
	 */
	public function setLisence($lisence) {
		$this->lisence = $lisence;
	}

	/**
	 * @return the $car_type
	 */
	public function getCar_type() {
		return $this->car_type;
	}

	/**
	 * @param field_type $car_type
	 */
	public function setCar_type($car_type) {
		$this->car_type = $car_type;
	}


	/**
	 * @return the $smoke_drive
	 */
	public function getSmoke_drive() {
		return $this->smoke_drive;
	}

	/**
	 * @param field_type $smoke_drive
	 */
	public function setSmoke_drive($smoke_drive) {
		$this->smoke_drive = $smoke_drive;
	}


	/**
	 * @return the $alcohol
	 */
	public function getAlcohol() {
		return $this->alcohol;
	}

	/**
	 * @param field_type $alcohol
	 */
	public function setAlcohol($alcohol) {
		$this->alcohol = $alcohol;
	}



	/**
	 * @return the $is_complished
	 */
	public function getIs_complished() {
		return $this->is_complished;
	}

	/**
	 * @param field_type $is_complished
	 */
	public function setIs_complished($is_complished) {
		$this->is_complished = $is_complished;
	}


	

}