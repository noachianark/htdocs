<script>
	function questions_page(url){
		$.get(url).promise()
		.done(function(data){
			$("#question_list").html(data);
		}).fail(function(data){
			toastr.error(data.responseText,"好像出错了");
		});
	}
	
	function setDimension(obj){
		$("#choice").find("span:first").text($(obj).find("a").text());
		$("#choice").find("input").val($(obj).find(':input:eq(0)').val());
	}
	function saveQuestion(){
		var bValid=true;
		bValid=bValid && validateRegexp($("#sequence"),/^([0-9])+$/i,"序列只能输入数字");
		bValid=bValid && validateLength($("#sequence"),1,3,"序列最多到999");
		bValid=bValid && validateLength($("#question"),3,50,"题干");
		var choices=[];
		
		$("#differ_answer_group .choice").each(function(){
			if($(".answer",this).val().length>0){
				bValid=bValid && validateRegexp($(".weight",this),/^([0-9])+$/i,"选项的权重必须为数字");
				if(bValid){
					var answer6={"answer":$(".answer",this).val(),"weight":$(".weight",this).val()};
					choices.push(answer6);
				}
			}
		});

		if(bValid){
			console.log($("#current_page").text());
			var qid=$("#question_id").val();
			$.post(qid?"/system/question/"+qid:"/system/question/",{
				sequence:$("#sequence").val(),
				content:$("#question").val(),
				choices:choices,
				gid:$("#gid").val(),
				dimension_id:$("#dimen_id").val()
			}).promise().done(function(data){
				$('#questionDialog').modal('hide');
				$("#sequence").val(parseInt($("#sequence").val())+1);
				$("#question_list").load("/system/question/"+$("#gid").val()+"?page="+$("#current_page").text());
				toastr.success((qid?"修改":"添加")+"题目成功","提示信息");
			}).fail(function(data){
				toastr.warning(data.responseText);
			});
			
		}
	}
	
	function modifyQuestion(qid){
		$("#add_area").empty();
		$("#questionDialog p").empty();
		$("#questionDialog p").load("/system/question/"+qid+"/edit?gid="+$("#gid").val());
	}
	
	function addQuestion(){
		$("#questionDialog p").empty();
		$.get("/system/question/new?id="+$("#gid").val()).promise().done(function(data){
			$("#add_area").html(data);
		});
	}
	
</script>
<a class="btn btn-info" onclick="addQuestion()" style="margin-bottom:15px;"><i class="icon-plus"></i>批量添加题目</a>
<div id="add_area"></div>						
<div id="question_list">
	{~include file="../question/get.tpl"~}
</div>