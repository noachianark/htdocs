<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>{~$title~}</title>
<link rel="stylesheet" type="text/css" href="/css/custom-theme/jquery-ui-1.10.1.custom.min.css">
<link rel="stylesheet" type="text/css" href="/css/ui.jqgrid.css">
<link rel="stylesheet" type="text/css" href="/css/global.css"/>
<link rel="stylesheet" type="text/css" href="/css/toastr.min.css"/>

<link href="/css/bootstrap.min.css" rel="stylesheet" />
<link href="/css/bootstrap-responsive.min.css" rel="stylesheet" />

<link href="/css/fonts.css" rel="stylesheet" />
<link href="/css/font-awesome.css" rel="stylesheet" />

<link href="/css/adminia.css" rel="stylesheet" /> 
<link href="/css/adminia-responsive.css" rel="stylesheet" /> 

    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
</head>
<body>
<div id="content">
	<div class="container">
		<div class="alert alert-block alert-error fade in">
            <h4 class="alert-heading">哎呀~访问权限不够鸟~！</h4>
            <p>
	      您登录已经超时或者根本木有登录，请重新登录~
	    </p>
            <p>
              <a href="/" class="btn btn-danger">重新登录</a> <a href="/" class="btn">不当回事</a>
            </p>
          </div>
	</div>
</div>
<div id="footer">
	
	<div class="container">				
		<hr />
		<p>&copy; 2012 Go Ideate.</p>
	</div> <!-- /container -->
	
</div> <!-- /footer -->
<script src="/scripts/jquery-1.8.3.js"></script>
<!--<script src="/scripts/jquery-ui-1.10.1.custom.min.js"></script>-->
<script src="/scripts/toastr.min.js"></script>
<script src="/scripts/framework/bootstrap.js"></script>
</body>
</html>
</body>