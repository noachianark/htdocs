<script>
	function user_page(url){
		$.get(url).promise()
		.done(function(data){
			$("#user_list").html(data);
		}).fail(function(data){
			toastr.error(data.responseText,"好像出错了");
		});
	}
	$(function(){
		$(".apparatusBtn").click(function(){
			window.location.href="/system/profile/entry/user_id/"+$(".uid",this).val()+"/page/{~$users->page~}/sex/"+$(".usex",this).val();
		});

		$(".deleteBtn").click(function(){
			console.log($(".user_id",this).val());
			$.ajax({url:"/user/user/" + $(".user_id",this).val(),type:"DELETE"}).done(function(data){
				// $('#deleteDialog').modal('hide');
				console.log(data);
				toastr.success("删除成功");
				// setTimeout(function(){
				// 	window.location.href="/system/profile";					
				// },1500);
			}).fail(function(data){
				console.log(data.responseText);
				toastr.success("删除失败"+data.responseText);
			});

		});

	});
</script>

	<div class="widget widget-table">		
		<div class="widget-header"> 
			<i class="icon-th-list"></i>
			<h3>受测者列表</h3>
			<div class="pull-right" style="margin-right:15px;">
			{~if ($users->page > 1) and ($users->page < $users->total) ~}
				<a onclick="user_page('/user/user/index/page/{~$users->page-1~}')" >上一页</a>
				 | 
				 <a onclick="user_page('/user/user/index/page/{~$users->page+1~}')">下一页</a> |
			{~elseif $users->page eq 1 ~}
				<a onclick="user_page('/user/user/index/page/{~$users->page+1~}')">下一页</a> |
			{~else~}
				<a onclick="user_page('/user/user/index/page/{~$users->page-1~}')">上一页</a> |
			{~/if~}
				总页数：{~$users->total~} | 当前页：{~$users->page~}
			</div>
		</div> <!-- /widget-header -->
		<div class="widget-content">					
			<table class="table table-striped table-bordered">
				<thead>
					<th>序列号</th>
					<th>身份证号</th>
					<th>受测者姓名</th>
					<th>受测者性别</th>
					<th>完成日期</th>
					<th> </th>
				</thead>
				<tbody>
					{~foreach $users->rows as $user~}
						{~if $user['is_delete'] eq 0~}
						<tr>
							<td style="width:100px;">{~$user['serial']~}</td>
							<td style="width:120px;">{~$user['id_card']~}</td>
							<td style="width:100px;">{~$user['name']~}</td>
							<td style="width:95px;">{~if $user['sex'] eq 1~}男{~else~}女{~/if~}</td>
							<td style="width:100px;">{~$user['clear_time']~}</td>
							<td class="action-td">
								{~if $user['state'] eq 0~}
								<a  role="button"  class="btn apparatusBtn" title="录入仪器数据" onclick="">
									<input class="uid" type="hidden" value="{~$user['id']~}">
									<input class="usex" type="hidden" value="{~$user['sex']~}">
									<i class="icon-cog"></i>								
								</a>
								{~else if $user['finish'] eq 0~}
									<a class="btn disabled" title="数据已经录入">
										<i class="icon-minus-sign"></i>
									</a>
								{~else~}
								<a   role="button"  class="btn btn-info report" title="查看报告" href="/system/profile/report/id/{~$user['id']~}" target="_blank">
									<input class="uid" type="hidden" value="{~$user['id']~}">
									<i class="icon-calendar-empty"></i>								
								</a>								
								{~/if~}
								<a class="btn btn-danger deleteBtn" title="删除用户">
									<i class="icon-trash"></i>
									<input class="user_id" type="hidden" value="{~$user['id']~}">					
								</a>				
							</td>
						</tr>
						{~/if~}
					{~/foreach~}
				</tbody>
			</table>
		</div> <!-- /widget-content -->
	</div>