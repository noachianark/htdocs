-- phpMyAdmin SQL Dump
-- version 3.5.4
-- http://www.phpmyadmin.net
--
-- 主机: localhost
-- 生成日期: 2013 年 11 月 24 日 13:56
-- 服务器版本: 5.5.19
-- PHP 版本: 5.3.14

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- 数据库: `iweekly`
--
CREATE DATABASE `iweekly` DEFAULT CHARACTER SET utf8 COLLATE utf8_bin;
USE `iweekly`;

-- --------------------------------------------------------

--
-- 表的结构 `cookbooks`
--

CREATE TABLE IF NOT EXISTS `cookbooks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `day` int(11) DEFAULT NULL,
  `time` int(11) DEFAULT NULL,
  `dish_id` int(11) NOT NULL,
  `parameter` float DEFAULT '1',
  PRIMARY KEY (`id`,`dish_id`),
  KEY `fk_cookbooks_dishes1_idx` (`dish_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=9 ;

-- --------------------------------------------------------

--
-- 表的结构 `dishes`
--

CREATE TABLE IF NOT EXISTS `dishes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=7 ;

-- --------------------------------------------------------

--
-- 表的结构 `dishes_materials`
--

CREATE TABLE IF NOT EXISTS `dishes_materials` (
  `dish_id` int(11) NOT NULL,
  `material_id` int(11) NOT NULL,
  `count` float DEFAULT '0',
  PRIMARY KEY (`dish_id`,`material_id`),
  KEY `fk_dishes_has_materials_materials1_idx` (`material_id`),
  KEY `fk_dishes_has_materials_dishes_idx` (`dish_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- 表的结构 `materials`
--

CREATE TABLE IF NOT EXISTS `materials` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) COLLATE utf8_bin NOT NULL,
  `price` float DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=25 ;

--
-- 限制导出的表
--

--
-- 限制表 `cookbooks`
--
ALTER TABLE `cookbooks`
  ADD CONSTRAINT `fk_cookbooks_dishes1` FOREIGN KEY (`dish_id`) REFERENCES `dishes` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- 限制表 `dishes_materials`
--
ALTER TABLE `dishes_materials`
  ADD CONSTRAINT `fk_dishes_has_materials_dishes` FOREIGN KEY (`dish_id`) REFERENCES `dishes` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_dishes_has_materials_materials1` FOREIGN KEY (`material_id`) REFERENCES `materials` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
