<?php
class System_IndexController extends Zend_Controller_Action
{

    public function init()
    {
        /* Initialize action controller here */
    	//$this->view->hello="this is default index ";
    	$this->view->admin_name=Zend_Auth::getInstance()->getIdentity()->username;
    	$this->view->title="系统管理";
    }

    public function indexAction()
    {	
        $page=$this->getRequest()->getUserParam("page");
    	$limit=$this->getRequest()->getUserParam("rows");
    	$page=isset($page)?$page:1;
    	$limit=isset($limit)?$limit:10;
    	$gaugeMapper=new System_Model_GaugeMapper();
    	$gauges=$gaugeMapper->findAll($page,$limit,false);
    	$userMapper=new User_Model_UserMapper();
    	$dashboard=$userMapper->getDashboard();
    	$this->view->gauges=$gauges;
    	$this->view->dash=$dashboard;
    }
    
    public function searchAction(){
    	//user info search

    	
    	
    }
    



    


}