{~foreach $materials as $material~}
	<tr>
		<td>
				<a class="edit_material_name" data-url="/material/{~$material['id']~}" data-name="name" data-type="text" data-pk="{~$material['id']~}">{~$material["name"]~}</a>						
		</td>
		<td>
				<a class="edit_material_price" data-url="/material/{~$material['id']~}" data-name="price" data-type="text" data-pk="{~$material['id']~}">{~$material["price"]~}</a><span class="pull-right">元</span>									
		</td>
		<td style="width:80px;">
			<a class="btn btn-danger btn-sm delMaterialBtn" data-id="{~$material['id']~}">删除</a>
		</td>												
	</tr>
{~/foreach~}

<script>
	$(function(){
		
		$(".delMaterialBtn").on("click",function(){
			$.ajax({
				url:"/material/"+$(this).data("id"),
				type:"DELETE"
			}).done(function(){
				$.get("/material").done(function(data){
					$("#material_table").html(data);
				}).fail(faultHandler);
			}).fail(faultHandler);
		});		
		
		
		$(".edit_material_name").editable({
			title:"修改原料名",
		    ajaxOptions: {
			    type: 'POST'
		    },
			params: function(params) {
			    //originally params contain pk, name and value
			    params.name = params.value;
			    return params;
		    },
			success:function(){
				$.get("/material").done(function(data){
					$("#material_table").html(data);
				}).fail(faultHandler);			
			}
		});
		
		$(".edit_material_price").editable({
			title:"修改原料价格",
		    ajaxOptions: {
			    type: 'POST'
		    },
			params: function(params) {
			    //originally params contain pk, name and value
				var par = {};
			    par.price = params.value;
			    return par;
		    },
			success:function(){
				$.get("/material").done(function(data){
					$("#material_table").html(data);
				}).fail(faultHandler);		
			}
		});					
	});
</script>