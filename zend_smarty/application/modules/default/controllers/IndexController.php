<?php
class IndexController extends Zend_Controller_Action
{

    public function init()
    {
        /* Initialize action controller here */
    	//$this->view->hello="this is default index ";

    }

    public function indexAction()
    {
		$this->view->title="欢迎您";
		$a=array();
		$admin=array();
		try {
			Zend_Db::factory('PDO_MYSQL', Zend_Registry::get("db_comm")->getConfig())->getConnection();
			$admin=Zend_Db::factory('PDO_MYSQL', Zend_Registry::get("db_comm")->getConfig())->query("select username from system_user")->fetchAll();
		} catch (Exception $e) {
			if($e->getCode()==1049){
				$this->_forward("index","install","default");
			}
		}
		if(count($admin)){
			$this->view->admins=$admin;
		}else{
			$this->_forward("create","install","default");
		}
    }
    

    public function logoutAction(){
    	$this->_helper->viewRenderer->setNoRender(true);
    	Zend_Auth::getInstance()->getStorage()->clear();
    	echo "success";
    }

    
    public function userAction(){
    	$this->_helper->viewRenderer->setNoRender(true);
    	if(!$this->getRequest()->isPost() && !$this->getRequest()->isXmlHttpRequest()){
    		$this->redirect("/");
    		return;
    	}
    	$auth=Zend_Auth::getInstance();
    	
    	$mapper=new User_Model_UserMapper();
    	$admin=new User_Model_User();
    	$admin->setOptions($this->getRequest()->getParams());
    	if(!$admin->getName()){
    		$this->getResponse()->setHttpResponseCode(422)->appendBody("姓名与身份证号不匹配111");
    		return;
    	}
    	$AA=$admin->getId_card();
    	$BB=$admin->getName();
    	$adapter=new Zend_Auth_Adapter_DbTable(Zend_Registry::get("db_comm"));
    	$adapter->setTableName("users")->setIdentityColumn('id_card')
    	->setCredentialColumn('name')
    	->setIdentity($admin->getId_card())
    	->setCredential($admin->getName());
    	
    	$result=$auth->authenticate($adapter);
    	
    	if($result->isValid()){
    		$auth->clearIdentity();
    		$auth->getStorage()->write($adapter->getResultRowObject());
    		$authSession = new Zend_Session_Namespace('Zend_Auth');
    		$authSession->setExpirationSeconds(3600);
    		$this->getResponse()->setHttpResponseCode(200);
    	}else{
    		$this->getResponse()->setHttpResponseCode(422)->appendBody("用户名与序列号不匹配222");
    	}
    	
    }
    
    public function loginAction(){
    	
    	$string=md5(md5("admin"));
    	
    	$this->_helper->viewRenderer->setNoRender(true);
    	if(!$this->getRequest()->isPost() && !$this->getRequest()->isXmlHttpRequest()){
    		$this->redirect("/");
    		return;
    	}

    	 
    	$auth=Zend_Auth::getInstance();
    	$mapper=new System_Model_AdministratorMapper();
    	$admin=new System_Model_Administrator();
    	$admin->setOptions($this->getRequest()->getParams());
    	if(!$admin->getUsername()){
    		$this->getResponse()->setHttpResponseCode(422)->appendBody("用户名或密码错误11");
    		return;
    	}

    	$adapter=new Zend_Auth_Adapter_DbTable(Zend_Registry::get("db_comm"));
    	$adapter->setTableName("system_user")->setIdentityColumn('username')
    	->setCredentialColumn('password')
    	->setIdentity($admin->getUsername())
    	->setCredential(md5(md5($admin->getPassword())));
    	 
    	$result=$auth->authenticate($adapter);
    	 
    	if($result->isValid()){
    		$mapper->findByName($admin->getUsername(), $admin);
    		if($admin->getForbidden()==1){
    			$this->getResponse()->setHttpResponseCode(422)->appendBody("该用户已经被禁用，请联系超级管理员");
    			return;
    		}
    		$auth->clearIdentity();
    		$auth->getStorage()->write($adapter->getResultRowObject());
    		$authSession = new Zend_Session_Namespace('Zend_Auth');
    		$authSession->setExpirationSeconds(3600);
    		$this->getResponse()->setHttpResponseCode(200);
    	}else{
    		$this->getResponse()->setHttpResponseCode(422)->appendBody("用户名或密码错误22");
    	}
    	 
    }
    
   

}