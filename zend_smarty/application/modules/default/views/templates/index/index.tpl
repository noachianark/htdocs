{~include file="header.tpl"~}
<body>
    <script>
    
        $(function() {
			$("#password").keydown(function(e){ 
                  if(e.keyCode==13){ 
                      login_admin();
                  } 
            });
			$("#username").keydown(function(e){ 
                  if(e.keyCode==13){ 
                      $("#password").focus();
                  } 
            });
        }); 
        
        
        function updateTips(text){
            $("#error_tips").removeClass("display_off");
        }
        function login_user(){
			$.post(
                "/index/user",
                {
                    name:$("#u").val(),
                    id_card:$("#serial").val()
                }
            ).promise().done(function(data){
				console.log(data);
				window.location.href="/user/home";
			}).fail(function(data){
				toastr.error(data.responseText,"提示信息");
			});			
		}
        function login_admin(){				
			$.post(
                "/index/login",
                {
                    username:$("#username").val(),
                    password:$("#password").val()
                }
            ).promise().done(function(data){
				window.location.href="/system/gauge";
			}).fail(function(data){
				toastr.error(data.responseText,"提示信息");
			});
        }
        

    </script>
<style>
#login_panel{
	padding: 0px 0 0 592px;
	font-family: 微软雅黑;
}
.log_admin,.log_inp,.login_button{
	background: url("/images/spr_x.png") no-repeat scroll 0 0 transparent;
    overflow: hidden;	
}
#panel_head, #panel_body, #panel_bottom{

    overflow: hidden;
}

#panel_head, #panel_body, #panel_bottom{
    width: 274px;
}
#panel_head{
	background-position: 0 0;
    height: 20px;
}
#panel_body{
	background-position: -275px 0;
    background-repeat: repeat-y;
    padding: 0 23px 0 24px;
    width: 227px;
}
#panel_bottom{
	background-position: -550px 0;
    height: 20px;
}
#index_background{
	background:#dddddd;
	width:100%;
}
#panel_body h2{
	color: #646464;
    font-size: 18px;
    font-weight: normal;
}
.login_field {
    border-bottom: 1px solid #DEDDDD;
    margin: 10px 0 0;
}
.log_unit {
    height: 36px;
    margin: 0 0 10px;
}

.log_inp {
	height:30px;
    background-position: 0 -169px;
    width: 220px;
}
.log_inp_on{
	 background-position: 0 -206px;
}
.log_inp .txt {
    color: #AAAAAA;
    font: 14px/20px 微软雅黑;
    height: 20px;
    outline: medium none;
    width: 210px;
}
.log_other {
    border-top: 1px solid #FFFFFF;
    padding: 14px 0 0;
}
.log_admin {
    background-position: 0 -141px;
    color: #646464;
    display: block;
    font-size: 14px;
    height: 27px;
    line-height: 26px;
    margin: 0 auto;
    padding: 0 0 0 36px;
    width: 122px;

}
#panel_body .txt:hover{
	background:#fbfbfb;
	color:#444444;
}
.log_btn{
	padding: 4px 0 34px;
}
.login_button{
	background-position: 0 0;
    display: block;
    float: right;
    height: 43px;
    text-indent: -999em;
    width: 125px;
}
.login_button:hover{
	background-position:-126px 0;
}
.center_div{
		position: absolute;
		width:990px;
		height:500px;
		left:50%;
		top:50%;
		margin-left:-495px;
		margin-top:-250px;
		border:0px;
}

</style>
	<div id="index_background">
			<div style="margin:auto;width:100%;">
				<div style="width:100%;px;background:url(/images/background_1.png);background-position:center center;height:100%;position:absolute;"></div>
				
			</div>
	</div>
	    <div class="center_div">
			<div style="margin:auto;width:944px;height:82px;background:url(/images/logo.png) no-repeat;position:absolute;"></div>
			<div id="content">
				<div id="login_panel">
					<div id="panel_head"></div>
					<div  class="alert alert-success" style="width: 247px;">
					<div id="panel_body">
						<h2>填写名字与身份证进入测试</h2>
						<div class="login_field" id="for_user">
							<p id="error_tips" class="display_off"></p>
							<div class="log_unit">
								<div class="log_inp">
									<input id="u" class="txt"  type="text" value="" placeholder="姓名"  tabindex="0" >
								</div>
							</div>
							<div class="log_unit">
								<div class="log_inp">
									<input id="serial" class="txt"  type="text" placeholder="身份证号码"  tabindex="0">
								</div>
							</div>
							<div class="log_btn">
								<div id="login_user" style="height:44px;">
									<a class="login_button" onclick="login_user()"></a>
								</div>
							</div>
						</div>
						
						
						<div class="login_field display_off" id="for_admin">
							<p class="display_off"></p>
							<div class="log_unit">
								<div class="log_inp">
									<input id="username" class="txt" type="text" placeholder="账号"  tabindex="0">
								</div>
							</div>
							<div class="log_unit">
								<div class="log_inp">
									<input id="password" class="txt" type="password" placeholder="密码" tabindex="0">
								</div>
							</div>
							<div class="log_btn">
								<div id="login_admin" style="height:44px;">
									<a class="login_button" onclick="login_admin()"></a>
								</div>
							</div>
						</div>
						
						<div style="text-align:center;">
							<div data-toggle="buttons-radio" class="btn-group log_other" style="padding-left:25px;">
			                    <button class="btn btn-info" onclick="$('#for_user').addClass('display_off');$('#for_admin').removeClass('display_off');$('#admin_log_btn').addClass('display_off');$('#user_log_btn').removeClass('display_off');$('#panel_body h2').text('请输入管理员账户和密码');">管理员入口</button>
			                    <button class="btn btn-info active" onclick="$('#for_user').removeClass('display_off');$('#for_admin').addClass('display_off');$('#user_log_btn').addClass('display_off');$('#admin_log_btn').removeClass('display_off');$('#panel_body h2').text('填写名字及身份证进入测试');">测试者入口</button>
			                 </div>							
						</div>
					</div>
					</div>
					
					<div id="panel_bottom"></div>
				</div>
			</div>
			
			
			<div id="footer">
				
				<div style="background:url(/images/panel_background.png);">				
					<p style="color:#ffffff;font-size:16px;font-weight:bolder;margin-left:20px;margin-top:50px;">&copy; 2013 云南交通职业技术学院.</p>
				</div> <!-- /container -->
				
			</div> <!-- /footer -->
			
			
	    </div>	
	


<!--<script src="/scripts/jquery-ui-1.10.1.custom.min.js"></script>-->
<script src="/scripts/toastr.min.js"></script>
<script src="/scripts/framework/bootstrap.js"></script>
</body>
{~include file="footer.tpl"~}