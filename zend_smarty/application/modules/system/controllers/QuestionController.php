<?php
class System_QuestionController extends Zend_Rest_Controller
{

    public function init()
    {
    	//$this->_helper->viewRenderer->setNoRender(true);
    	//$this->view->admin_name=Zend_Auth::getInstance()->getIdentity()->username;
    	$this->view->dimension_list=array();
    	$gid=$this->getRequest()->getParam("gid");
    	if(isset($gid)){
    		$mapper=new System_Model_DimensionMapper();
    		$result=$mapper->findAllList($gid);
    		if(is_array($result)){
    			$this->view->dimension_list=$result;
    		}
    	}
    }

    public function indexAction()
    {	
    	$gid=$this->getRequest()->getParam("gid");
    	$page=$this->getRequest()->getParam("page");
    	if(isset($gid)){
    		$page=$page?$page:1;
    		$questions=new System_Model_QuestionMapper();
    		$result=$questions->findAll($gid,$page,20,"DESC");
    		$result->gid=$gid;
    		$this->view->questions=$result;
    		$this->view->question=new System_Model_Question();
    	}else{
    		$this->getResponse()->setHttpResponseCode(422)->appendBody("参数错误");
    	}
    }
    
    public function newAction(){
    	$this->_helper->viewRenderer->setNoRender(false);
    	$gid=$this->getRequest()->getParam("id");
    	$qMapper=new System_Model_QuestionMapper();
    	$mapper=new System_Model_DimensionMapper();
    	$result=$mapper->findAllList($gid);
    	if(is_array($result)){
    		$this->view->dimension_list=$result;
    	}
    	$this->view->records=$qMapper->getRecords($gid);
    	$this->view->question=new System_Model_Question();
    }
    
    public function editAction(){
    	$id=$this->getRequest()->getParam("id");
    	$gid=$this->getRequest()->getParam("gid");
    	if(isset($id)){
    		$this->_helper->viewRenderer->setNoRender(false);
    		$model=new System_Model_Question();
    		$mapper=new System_Model_QuestionMapper();
    		$model=$mapper->find($id);
    		$this->view->question=$model;
    	    $mapper=new System_Model_DimensionMapper();
    		$result=$mapper->findAllList($gid);
    		if(is_array($result)){
    			$this->view->dimension_list=$result;
    		}
    	}else{
    		$this->getResponse()->setHttpResponseCode(422)->appendBody("id not valid");
    	}
    }
    
    public function getAction(){
        $gid=$this->getRequest()->getParam("id");
    	$page=$this->getRequest()->getParam("page");
    	if(isset($gid)){
    		$page=$page?$page:1;
    		$questions=new System_Model_QuestionMapper();
    		$result=$questions->findAll($gid,$page);
    		$result->gid=$gid;
    		$this->view->questions=$result;
    	}else{
    		$this->getResponse()->setHttpResponseCode(422)->appendBody("参数错误");
    	}
    }
    
    public  function postAction(){
    	$this->_helper->viewRenderer->setNoRender(true);
    	if(!count($this->getRequest()->getUserParams())){
    		$this->getResponse()->setHttpResponseCode(422)->appendBody("param invalid");
    		return;
    	}
    	$model=new System_Model_Question();
    	$mapper=new System_Model_QuestionMapper();
    	$arr=$this->getRequest()->getParams();
    	$model->setOptions($this->getRequest()->getParams());
    	$result=$mapper->save($model);
    	if(is_numeric($result)){
    		$this->getResponse()->setHttpResponseCode(201)->appendBody($model->getQid());
    	}else{
    		$this->getResponse()->setHttpResponseCode(422)->appendBody($result);
    	}
    }
    
    public function deleteAction(){
    	echo "delete";
    }

    public function putAction(){
    	//$ids=explode(",", $this->getRequest()->getParam("id"));
    	$this->_helper->viewRenderer->setNoRender(true);
    	$id=$this->getRequest()->getParam("id");
    	if(isset($id)){
    		$mapper=new System_Model_QuestionMapper();
    		$model=new System_Model_Question($this->getRequest()->getUserParams());
    		$model->setQid($id);
    		$result=$mapper->update($model);
    		if(is_numeric($result)){
    			$this->getResponse()->setHttpResponseCode(201)->appendBody($result);
    		}else{
    			$this->getResponse()->setHttpResponseCode(422)->appendBody($result);
    		}
    	}else{
    		$this->getResponse()->setHttpResponseCode(422)->appendBody("参数错误");
    	}
    }
    
    public function headAction(){
    	
    }

    


}