{~include file="header.tpl"~}
<div class="container">

    <div class="row">
        <div id="backToMain" class="backBtn btn-hg col-md-2">
            <span class="glyphicon glyphicon-chevron-left"></span> 返回
        </div>    
    </div>

	<div class="row">
		<div class="col-md-6">
			<h5 id="titles">{~$day~}的{~$time~}餐：</h5>
		</div>
	</div>

	<div class="row">
		<form role="form" id="cookbook_form">
			
			<input type="hidden" name="day" value="{~$day~}">
			<input type="hidden" name="time" value="{~$time~}">
			
			<div class="form-group col-md-3">
				<select name="dish_id" id="dish_selector">
					{~foreach $dishes as $dish~}
						<option value = "{~$dish['id']~}">{~$dish['name']~}</option>
					{~/foreach~}
				</select>				
			</div>

			<div class="form-group col-md-2">
				<div class="input-group">
					<input class="form-control" type="text" name="parameter" placeholder = "系数">
					<span class="input-group-addon">人</span>
				</div>
			</div>

			<div class="col-md-4">
                <a type="button" class="btn btn-primary" id="addCookbookBtn">
                  <span class="glyphicon glyphicon-ok"></span> 添加菜品
                </a>					
			</div>

			
		</form>
	</div>

	<div class="row">
		<table class="table table-striped table-responsive table-bordered">
			<thead>
				<tr>
					<th>菜名</th>
					<th>系数</th>
				</tr>
			</thead>
			<tbody>
				{~foreach $lists as $cookbook~}
					<tr>
						<td>{~$cookbook['name']~}</td>
						<td>{~$cookbook['parameter']~}</td>
						<td style="width:80px;">
							<a class="btn btn-danger btn-sm delCookbookBtn" data-id="{~$cookbook['id']~}">删除</a>							
						</td>
					</tr>
				{~/foreach~}
			</tbody>
		</table>
	</div>

</div>
{~include file="js.tpl"~}
<script type="text/javascript">
	$(function(){
		$("#titles").text(dayArr["{~$day~}"]+"的"+timeArr["{~$time~}"]);
		$("#dish_selector").selectpicker({
			style: 'btn btn-primary', 
			menuStyle: 'dropdown-inverse'
		});

		$("#backToMain").on("click",function(){
			window.location.href="/";
		});

		$("#addCookbookBtn").on("click",function(){
			$.post("/cookbook",$("#cookbook_form").serialize())
			.done(function(data){
				window.location.href="/cookbook/{~$day~}_{~$time~}";
			})
			.fail(faultHandler);
		});

		$(".delCookbookBtn").on("click",function(){
			$.ajax({
				type:"DELETE",
				url:"/cookbook/"+$(this).data("id")
			})
			.done(function(){
				window.location.href="/cookbook/{~$day~}_{~$time~}";
			})
			.fail(faultHandler);
		})

	});
</script>
{~include file="footer.tpl"~}