<?php
class ClassifyController extends Zend_Rest_Controller
{

    public function init()
    {
        /* Initialize action controller here */
    	//$this->view->hello="this is default index ";
    	$this->_helper->viewRenderer->setNoRender(true);
    }



    public function indexAction()
    {
    	 $mapper = new Default_Model_ClassifyMapper();
    	 $result = $mapper -> findAll();
    	 if(array_sum($result)){
    	 	$this->getResponse()->setHttpResponseCode(200)->appendBody(json_encode($result));
    	 }else{
    	 	$this->getResponse()->setHttpResponseCode(422)->appendBody("server error");
    	 }
    }
    
    public function getAction(){
    	 
    }
    
    public function putAction(){
    	$classify = new Default_Model_Classify();
    	$classify->setOptions($this->getRequest()->getParams());
    	$mapper = new Default_Model_ClassifyMapper();
    	$result = $mapper->update($classify);
    	if(is_numeric($result)){
    		$this->getResponse()->setHttpResponseCode(201)->appendBody("success~?");
    	}else{
    		$this->getResponse()->setHttpResponseCode(422)->appendBody("server error");
    	}
    }
    
    public function postAction(){
    	
    	$classify = new Default_Model_Classify();
    	$classify->setOptions($this->getRequest()->getParams());

    	$mapper = new Default_Model_ClassifyMapper();

    	$result = $mapper->save($classify);

    	if(is_numeric($result)){
    		$this->getResponse()->setHttpResponseCode(201)->appendBody($result);
    	}else{
    		$this->getResponse()->setHttpResponseCode(422)->appendBody("server error");
    	}
    }
    
    public function deleteAction(){
    	 $id = $this->getRequest()->getParam("id");
    	 if(isset($id)){
    	 	$mapper = new Default_Model_ClassifyMapper();
    	 	$result = $mapper->delete($id);
    	 	if($result=="success"){
    	 		$this->getResponse()->setHttpResponseCode(200)->appendBody("delete success");
    	 	}else{
    	 		$this->getResponse()->setHttpResponseCode(422)->appendBody("server error");
    	 	}
    	 }
    }
    
    public function headAction(){
    	 
    }
    
   

}