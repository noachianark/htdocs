{~include file="header.tpl"~}
	
	
    <div class="container main" style="height:100%;overflow:visible;">
		
		<div class="row">
            <div class="col-md-6">
                <h5><a class="btn btn-sm btn-warning exportCookbookBtn">导出Excel表</a>每周菜谱</h5>
            </div>
            <div class="col-md-6">
                <h5>
                    <a data-placement="left" data-toggle="popover" data-container="body" class="clearCookbook btn pull-right btn-sm btn-primary" type="button" data-original-title="警告！" title="">清空食谱</a>
                </h5>
            </div>
        </div>

        <div class="row">
            <table id="cookbook_table" class="table table-responsive  table-bordered">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>星期一</th>
                        <th>星期二</th>
                        <th>星期三</th>
                        <th>星期四</th>
                        <th>星期五</th>
                        <th>星期六</th>
                        <th>星期日</th>
                    </tr>
                </thead>
                <tbody id="week_cookbooks">
                    
                </tbody>
            </table>
        </div>
		
		

		<div class="row">
			<div class="col-md-6">
				<h5><a class="btn btn-sm btn-warning exportStatBtn">导出Excel表</a>食材统计</h5>
			</div>
			<div class="col-md-6">
				
			</div>
		</div>

        <div class="row" style="padding-bottom:10px;">
            <ul class="nav nav-tabs nav-justified">
              <li data-day = "" class="active"><a class="btn">一周</a></li>
              <li data-day = "0"><a class="btn">周一</a></li>
              <li data-day = "1"><a class="btn">周二</a></li>
              <li data-day = "2"><a class="btn">周三</a></li>
              <li data-day = "3"><a class="btn">周四</a></li>
              <li data-day = "4"><a class="btn">周五</a></li>
              <li data-day = "5"><a class="btn">周六</a></li>
              <li data-day = "6"><a class="btn">周日</a></li>
            </ul>
        </div>

    	<div class="row">
            <table id="statistic_table" class="table table-striped table-responsive  table-bordered">
                   <thead>
                       <tr>
                           <th>原料名</th>
                           <th>总计数量</th>
                           <th>总计价格</th>
                       </tr>
                   </thead>
                   <tbody id="statistics">
                       
                   </tbody>
               </table>   
        </div>
    </div>



 {~include file="js.tpl"~}
    <script type="text/javascript">



      var initialize =  function(){
      			$(".dishBtn").on("click",function(){
      				window.location.href="/index/manage";
      			});
      			
      			$.get("/statistic").done(function(data){
      				$("#statistics").html(data);
      			}).fail(faultHandler);

            $.get("/cookbook").done(function(data){
                $("#week_cookbooks").html(data);
            }).fail(faultHandler);

            $(".exportStatBtn").on("click",function(){
                tableToExcel("statistic_table","W3C Examle Table");
            });


            $(".exportCookbookBtn").on("click",function(){
                tableToExcel("cookbook_table","W3C Examle Table");
            });

            $(".nav li").on("click",function(){
                $(".nav .active").removeClass("active");
                $(this).addClass("active");
                $.get("/statistic/day/"+$(this).data("day")).done(function(data){
                    $("#statistics").html(data);
                }).fail(faultHandler);
            });

            $(".clearCookbook").popover({
                html:true,
                content:
                  '<p>你真的要清空食谱表？？</p>'+
                  '<div class="row">'+
                    '<div class="col-md-6">'+
                      '<a class="btn btn-success btn-sm cancelDel" onclick="application.delCookbook()">取消</a>'+
                    '</div>'+
                    '<div class="col-md-6">'+
                      '<a class="btn btn-danger btn-sm confirmDel pull-right" onclick="application.confirmDelCookbook()">确认</a>'+
                    '</div>'+
                  '</div>'

            });

        };

        var application = {
            init:initialize,
            delCookbook:function(){
                $(".clearCookbook").popover("hide");
            },
            confirmDelCookbook:function(){
                $.ajax({
                  url:"/cookbook",
                  type:"DELETE"
                }).done(function(){
                  window.location.href="/";
                }).fail(faultHandler);
            }
        };


        var tableToExcel = (function() {
          var uri = 'data:application/vnd.ms-excel;base64,'
            , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--><meta http-equiv="content-type" content="text/plain; charset=UTF-8"/></head><body><table>{table}</table></body></html>'
            , base64 = function(s) { return window.btoa(unescape(encodeURIComponent(s))) }
            , format = function(s, c) { return s.replace(/{(\w+)}/g, function(m, p) { return c[p]; }) }
          return function(table, name) {
            if (!table.nodeType) table = document.getElementById(table)
            var ctx = {worksheet: name || 'Worksheet', table: table.innerHTML}
            window.location.href = uri + base64(format(template, ctx))
          }
        })();


        $(function(){
            application.init();
        });
    </script>
 
 
 
 {~include file="footer.tpl"~}