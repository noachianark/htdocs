<?php
class System_Model_Classify {
	protected $cid;
	protected $classify_name;
	protected $display=true;
	protected $parentid=-1;
	public function __construct(array $options = null)
	{
		if (is_array($options)) {
			$this->setOptions($options);
		}
	}
	
	public function setOptions(array $options)
	{
		$methods = get_class_methods($this);
		foreach ($options as $key => $value) {
			$method = 'set' . ucfirst($key);
			if (in_array($method, $methods)) {
				$this->$method($value);
			}
		}
		return $this;
	}
	public function toJson(){
		return json_encode($this->toArray());
	}
	
	public function toArray(){
		$array=array();
		foreach ($this as $key => $value) {
			if($value!=null){
				$array[$key] = $value;
			}
		}
		return $array;
	}
	/**
	 * @return the $cid
	 */
	public function getCid() {
		return $this->cid;
	}

	/**
	 * @return the $classify_name
	 */
	public function getClassify_name() {
		return $this->classify_name;
	}

	/**
	 * @return the $display
	 */
	public function getDisplay() {
		return $this->display;
	}

	/**
	 * @return the $parentid
	 */
	public function getParentid() {
		return $this->parentid;
	}

	/**
	 * @param field_type $cid
	 */
	public function setCid($cid) {
		$this->cid = $cid;
	}

	/**
	 * @param field_type $classify_name
	 */
	public function setClassify_name($classify_name) {
		$this->classify_name = $classify_name;
	}

	/**
	 * @param boolean $display
	 */
	public function setDisplay($display) {
		$this->display = $display;
	}

	/**
	 * @param number $parentid
	 */
	public function setParentid($parentid) {
		$this->parentid = $parentid;
	}

	
	
	
}

?>