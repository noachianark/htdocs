    <style type="text/css">
        .container{
            width: 1000px;
        }
        table{
            vertical-align: middle;
        }
        td{
            padding:0px;
            text-align: center;
            vertical-align: middle;
        }
        .bs-footer {
            text-align: left;
        }
        .bs-footer {
            position: fixed;
            border-top: 1px solid #E5E5E5;
            color: #777777;
            margin-bottom: 0px;
            padding-bottom: 30px;
            padding-top: 40px;
            text-align: center;
        }
        article, aside, details, figcaption, figure, footer, header, hgroup, main, nav, section, summary {
            display: block;
        }
        .table>td {
            padding:0px;
            text-align: center;
            vertical-align: middle;            
        }
        .item{
            min-height: 26px;
            cursor: pointer;
        }
		.item:hover{
			background-color:#ff0000;
		}
        .dishpanel, .materialpanel{
            display: none;
        }
        .backBtn{
            color:#1abc9c;
            cursor: pointer;
        }
        .add_form{
            border: 2px solid #1ABC9C;
            border-radius: 6px;
        }
		html, body{
			height: 100%;
		}
  		

    </style>
  </body>
</html>