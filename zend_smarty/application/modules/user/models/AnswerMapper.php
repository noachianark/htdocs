<?php
class User_Model_AnswerMapper{
	protected $_dbTable;
	
	public function setDbTable($dbTable)
	{
		if (is_string($dbTable)) {
			$dbTable = new $dbTable();
		}
		if (!$dbTable instanceof Zend_Db_Table_Abstract) {
			throw new Exception('Invalid table data gateway provided');
		}
		$this->_dbTable = $dbTable;
		return $this;
	}
	
	public function getDbTable()
	{
		if (null === $this->_dbTable) {
			$this->setDbTable('User_Model_DbTable_Answer');
		}
		return $this->_dbTable;
	}
	

	
	public function save(User_Model_Answer $answer)
	{		
		try{
			$result=$this->getDbTable()->insert($answer->toArray());
		}catch(Exception $e){
			return Utils_SQLErrorMsg::formatException($e);
		}
		return $result;

	}
	

	
    public function findByUserid($id)
    {
		//$select=$this->getDbTable()->select()->from(array("gauge","answers"),array("gauge.gid",""))->where("user_id=".$id);
		$select=$this->getDbTable()->select()->setIntegrityCheck(false)
		->from("answers","*")->where("user_id='".$id."'")->order("gauge_id desc")
		->join("gauge","gauge.gid=answers.gauge_id","gauge_name");
		
    	try{
			$result=array();
			$datas=$this->getDbTable()->fetchAll($select)->toArray();
			foreach($datas as $data){
				$answer=new User_Model_Answer($data);
				$answer->guage_name=$data["gauge_name"];
				array_push($result, $answer);
			}
			return $result;
		}catch(Exception $e){
			return Utils_SQLErrorMsg::formatException($e);
		}
    }
    
    public function findAll($curPage=1,$perPage=30){
		$select=$this->getDbTable()->select()
							->limitPage($curPage, $perPage);
		$rows=$this->getDbTable()->getAdapter()->fetchOne("select count(*) from users");
		$data=array();
		try{
			$data=$this->getDbTable()->fetchAll($select)->toArray();
		}catch(Exception $e){
			return Utils_SQLErrorMsg::formatException($e);
		}
		$response=new stdClass();
		$response->page=$curPage;
		$response->total=ceil($rows/$perPage);
		$response->records=(int)$rows;
		$response->rows=$data;
		return $response;
    }
    
    public function delete($id){
    	if($id===null || $id==0){
    		return "ID不能为空";
    	}
    	$result = $this->getDbTable()->find($id);
        if (0 == count($result)) {
            return "没有找到该用户";
        }
        $result=$this->getDbTable()->update(array('actived'=>0), array("userid=? "=>$id));
        return isset($result)?true:false;
    }
    
    public function findComplished($user_id){
    	$select=$this->getDbTable()->select()->where("user_id=".$user_id." and gauge_id in(1,2,3,4)");
    	try{
    		$data=$this->getDbTable()->fetchAll($select)->toArray();
    		$stdClass= new stdClass();
    		$stdClass->speed=false;
    		$stdClass->response=false;
    		$stdClass->deep=false;
    		$stdClass->hand=false;
    		$stdClass->sight=false;
    		foreach($data as $result){
    			if($result['gauge_id']==1){
    				$stdClass->speed=true;
    				//break;
    			}
    			if($result['gauge_id']==2){
    				$stdClass->response=true;
    				//break;
    			}
    			if($result['gauge_id']==3){
    				$stdClass->deep=true;
    				//break;
    			}
    			// if($result['gauge_id']==4){
    			// 	$stdClass->hand=true;
    			// 	//break;
    			// }
    			if($result['gauge_id']==4){
    				$stdClass->sight = true;
    			}
    		}
    		return $stdClass;
    	}catch(Exception $e){
    		return "sql error".$e;
    	}
    }

    			
    
    public function getCondition($userid,$sex){
    	$mapper=new System_Model_GaugeMapper();
    	return $mapper->findPublished($sex,$userid);
    }

}