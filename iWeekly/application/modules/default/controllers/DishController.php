<?php
class DishController extends Zend_Rest_Controller
{

    public function init()
    {
        /* Initialize action controller here */
    	//$this->view->hello="this is default index ";
    	$this->_helper->viewRenderer->setNoRender(true);
    }

    
    public function indexAction(){
    	$this->_helper->viewRenderer->setNoRender(false);
    	$dishes = new Default_Model_DishMapper();
    	$this->view->dishes = $dishes -> findAll();
    }
    

    
    public function listAction(){
    	$this->_helper->viewRenderer->setNoRender(false);

    	
//     	$dishMaps = array();
//     	foreach($result as $record){
//     		$dishMaps[$record['day']][$record['time']] = $record;
//     	}
    	//$this->view->dishMap = json_encode($result);
    }

    public function getAction()
    {
    	$id = $this->getRequest()->getParam("id");
    	
    	if(isset($id)){
    		$mapper = new Default_Model_DishMapper();
    		$materialMapper = new Default_Model_MaterialMapper();
    		$all = $materialMapper -> findAll();
    		$result = $mapper -> findById($id);
    		$me = $mapper -> findSelf($id);
    		
    		
    		//$this->getResponse()->setHttpResponseCode(200)->appendBody(json_encode($result));
    		
     		$this->view->me = $me;
     		$this->view->all = $all;
     		$this->view->materials = $result;
     		$this->render("dishpanel");
    		
    	}else{
    		$this->_redirect("/index/manage");
    		return;
    	}

//     	$rowset = $mapper -> findMaterial($id);
//     	$this->view->dish = $rowset["dish"];
//     	$this->view->materials = $rowset["materials"];
//     	$classify_mapper = new Default_Model_ClassifyMapper();
//     	$result = $classify_mapper->findAll();
//     	$this->view->classify = $result;
//     	$this->render("dishpanel");
    }
    
    public function postAction()//新建
    {
    	//$this->getResponse()->setHttpResponseCode(422)->appendBody(json_encode($this->_getAllParams()));
    	$dish = new Default_Model_Dish();
    	$dish->setOptions($this->getRequest()->getParams());

        $mapper = new Default_Model_DishMapper();
        $result = $mapper->save($dish);
        if(is_numeric($result)){
        	$this->getResponse()->setHttpResponseCode(201)->appendBody($dish->getId());
        }else{
        	$this->getResponse()->setHttpResponseCode(422)->appendBody($result);
        }

    }
    
    public function putAction()//修改
    {
        $dish = new Default_Model_Dish();
    	$dish->setOptions($this->getRequest()->getParams());

        $mapper = new Default_Model_DishMapper();
        $result = $mapper->update($dish);
        if(is_numeric($result)){
        	$this->getResponse()->setHttpResponseCode(201)->appendBody($dish->getId());
        }else{
        	$this->getResponse()->setHttpResponseCode(422)->appendBody($result);
        }   
    }
    
    public function deleteAction()
    {
        $mapper = new Default_Model_DishMapper();
        $result = $mapper -> delete($this->getRequest()->getParam("id"));
		$this->getResponse()->setHttpResponseCode(200)->appendBody("delete success");
    }
    
    public function headAction(){
    	
    }

    

    public function abortIndexAction()
    {
    	$this->_helper->viewRenderer->setNoRender(false);
    	$day = $this->getRequest()->getParam("day");
    	$time = $this->getRequest()->getParam("time");
    	if(!isset($day) && !isset($time)){
    		$mapper = new Default_Model_DishMapper();
    		$result = $mapper->findAll();
    		$dishMaps = array(
    				"0"=>array(
    						"0"=>array(),
    						"1"=>array(),
    						"2"=>array()
    				),
    				"1"=>array(
    						"0"=>array(),
    						"1"=>array(),
    						"2"=>array()
    				),
    				"2"=>array(
    						"0"=>array(),
    						"1"=>array(),
    						"2"=>array()
    				),
    				"3"=>array(
    						"0"=>array(),
    						"1"=>array(),
    						"2"=>array()
    				),
    				"4"=>array(
    						"0"=>array(),
    						"1"=>array(),
    						"2"=>array()
    				),
    				"5"=>array(
    						"0"=>array(),
    						"1"=>array(),
    						"2"=>array()
    				),
    				"6"=>array(
    						"0"=>array(),
    						"1"=>array(),
    						"2"=>array()
    				)
    		);
    
    		foreach($result as $record){
    			array_push($dishMaps[$record["day"]][$record["time"]], $record);
    		}
    		$this->view->dishMap = $dishMaps;
    		$classify_mapper = new Default_Model_ClassifyMapper();
    		$classifies = $classify_mapper -> findAll();
    		$material_mapper = new Default_Model_MaterialMapper();
    		$this->view->classifies = $classifies;
    		$this->view->statistics = $material_mapper->findTotalPrice();
    		$this->render("list");
    
    		return;
    	}
    	$mapper = new Default_Model_DishMapper();
    	$result = $mapper->findByDayTime((int)$day, (int)$time);
    	$this->view->dishes = $result;
    	$this->view->day = $day;
    	$this->view->time = $time;
    }

}