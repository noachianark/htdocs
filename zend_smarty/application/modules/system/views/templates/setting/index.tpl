{~include file="header.tpl"~}
<script>
	$(function(){
		$("#setting_navi").addClass("active");
		
		$("#add_user").click(function(){
			$.post("/system/admin/",$("#form").serialize()).promise().done(function(){
				window.location.href="/system/setting";
			}
			).fail(function(data){
				toastr.warning(data.responseText);
			});
		});
		
		$(".forbidden").click(function(){
			$.post("/system/admin/"+$(".uid",this).val(),{forbidden:1}).promise().done(function(data){
				console.log("asdasdasd");
				window.location.href="/system/setting";
			}).fail(function(data){
				toastr.warning(data.responseText);
			});
		});
		
		$(".release").click(function(){
			$.post("/system/admin/"+$(".uid",this).val(),{forbidden:0}).promise().done(function(data){
				console.log("asdasdasd");
				window.location.href="/system/setting";
			}).fail(function(data){
				toastr.warning(data.responseText);
			});			
		});
		
	});
</script>
<link rel="stylesheet" type="text/css" href="/css/dashboard.css"/>
	<h1 class="page-title">
		<i class="icon-user"></i>
		<span style="cursor:pointer;" onclick="window.location.href='/system/setting'">系统设定</span>
	</h1>
{~if $role eq 0~}
	<div class="widget">		
		<div class="widget-header"> 
			<i class="icon-th-list"></i>
			<h3>管理员列表</h3>
			<div class="pull-right" style="margin-right:15px;margin-top:8px;">
 				<a class="btn btn-info" data-toggle="modal" href="#myModal"  data-keyboard="false" data-backdrop="true">添加管理员</a>
			</div>
		</div> <!-- /widget-header -->
		<div class="widget-content">	
			
			
			<table class="table table-striped table-bordered">
				<thead>
					<th>管理员id</th>
					<th>管理员用户名</th>
					<th>管理员类型</th>
					<th> </th>
				</thead>
				<tbody>
					{~foreach $users->rows as $user~}
						<tr>
							<td style="width:200px;">{~$user['id']~}</td>
							<td style="width:200px;">{~$user['username']~}</td>
							<td style="width:95px;">{~if $user['role_id'] eq 0~}<i class="icon-user icon-large" style="color:#5BC0DE;"></i>超级管理员{~else~}<i class="icon-user icon-large"></i>普通管理员{~/if~}</td>
							<td class="action-td">
								{~if $user['role_id'] eq 1~}
									{~if $user['forbidden'] eq 0~}
										<a   role="button"  class="btn btn-danger forbidden" title="停用">
											<input class="uid" type="hidden" value="{~$user['id']~}">
											<i class="icon-stop"></i>
										</a>
									{~else~}
										<a   role="button"  class="btn btn-success release" title="启用">
											<input class="uid" type="hidden" value="{~$user['id']~}">
											<i class="icon-play"></i>
										</a>										
									{~/if~}
								{~/if~}
							</td>
						</tr>
					{~/foreach~}
				</tbody>
			</table>
			<div class="modal hide" id="myModal">
				 <div class="modal-header">
					<a class="close" data-dismiss="modal">×</a>
					<h3>创建管理员</h3>
				</div>
				<div class="modal-body">
					<p>
						<form id="form" class="form-horizontal">
							
							<div class="control-group">											
								<label for="username" class="control-label">登录用户名</label>
								<div class="controls">
									<input name="username" type="text" id="username" class="input-4x">
								</div> <!-- /controls -->				
							</div> <!-- /control-group -->
							
							<div class="control-group">											
								<label for="username" class="control-label">登陆密码</label>
								<div class="controls">
									<input name="password" type="password" id="password" class="input-4x">
								</div> <!-- /controls -->				
							</div> <!-- /control-group -->						

							<div class="control-group">											
								<label for="username" class="control-label">重复密码</label>
								<div class="controls">
									<input type="password" id="password2" class="input-4x">
								</div> <!-- /controls -->				
							</div> <!-- /control-group -->		
						
						</form>
					</p>
				</div>
				<div class="modal-footer">
					<a class="btn btn-primary" id="add_user">确定添加</a>
				</div>				
				
			</div>
		</div> <!-- /widget-content -->
	</div>
	{~else~}
		{~include file = "../admin/new.tpl"~}
	{~/if~}

{~include file="footer.tpl"~}