<?php
require_once 'Zend/Db/Table/Abstract.php';
class Default_Model_DbTable_Cookbook extends Zend_Db_Table_Abstract{
	protected $_name="cookbooks";
	protected $_primary="id";
	protected $_referenceMap =  array(
		"Dish" => array(
				"columns" => "dish_id",
				"refTableClass" => "Default_Model_DbTable_Dish",
				"refColumns" => array("id")		
		)
	);
}