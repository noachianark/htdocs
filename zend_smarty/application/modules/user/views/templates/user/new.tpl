<style>
	#add_user_form label{
		color:#3A87AD;
	}
	#add_user_form{
		margin-bottom:0px;
	}
</style>
<script>
	function addUser(){
		var bValid=true;
		bValid=bValid && validateLength($('#user_name'),2,10,"用户姓名");
		bValid=bValid && validateRegexp($('#user_name'),/^[\u4e00-\u9fa5a-zA-Z]+$/,"用户名只能包含中文或英文");
		if(bValid){
			$.post("/user/user",{
				name:$('#user_name').val(),
				sex:$('#inlineCheckbox1').attr('checked')?1:2,
				id_card:$("#user_identity").val()
			}).promise().done(function(data){
				toastr.success("用户创建成功","提示信息");
				$("#user_list").load("/user/user/index/page/{~$page~}");
			}).fail(function(data){
				toastr.error(data.responseText,"内部错误");
			});
		}
	}
</script>
<form id="add_user_form" class="form-horizontal">
	<fieldset>
		<div class="control-group">											
			<label for="user_name" class="control-label">受测姓名</label>
			<div class="controls">
				<input type="text" id="user_name" class="input-large" value="{~$user->getName()~}">
			</div> <!-- /controls -->				
		</div> <!-- /control-group -->
		<div class="control-group">											
			<label for="user_name" class="control-label">身份证号码</label>
			<div class="controls">
				<input type="text" id="user_identity" class="input-large" value="{~$user->getId_card()~}">
			</div> <!-- /controls -->				
		</div> <!-- /control-group -->		
		<div class="control-group">
            <label for="inlineCheckboxes" class="control-label">受测者性别</label>
            <div class="controls">
              <label class="checkbox inline">
                <input type="radio" name="sex_radio" checked="true"  value="option1" id="inlineCheckbox1"> 男
              </label>
              <label class="checkbox inline">
                <input type="radio" name="sex_radio"  value="option2" id="inlineCheckbox2"> 女
              </label>
            </div>
        </div>
		<div class="control-group" style="margin-bottom:0px;">
            <label  class="control-label"></label>
            <div class="controls">
				<a  class="btn btn-info" onclick="addUser()">添加</a> 
				<a class="btn" onclick="document.getElementById('add_user_form').reset();">重置</a>
            </div>
        </div>

	</fieldset>
</form>
