<?php
class System_Model_Category{
	protected $cat_id;
	protected $gauge_id;
	protected $category_name;
	protected $category_content;
	
	public function __construct(array $options = null)
	{
		if (is_array($options)) {
			$this->setOptions($options);
		}
	}
	
	public function setOptions(array $options)
	{
		$methods = get_class_methods($this);
		foreach ($options as $key => $value) {
			$method = 'set' . ucfirst($key);
			if (in_array($method, $methods)) {
				$this->$method($value);
			}
		}
		return $this;
	}
	
	public function toJson(){
		return json_encode($this->toArray());
	}
	
	public function toArray(){
		$array=array();
		foreach ($this as $key => $value) {
			if($value!=null){
				$array[$key] = $value;
			}
		}
		return $array;
	}
	/**
	 * @return the $cat_id
	 */
	public function getCat_id() {
		return $this->cat_id;
	}

	/**
	 * @return the $gauge_id
	 */
	public function getGauge_id() {
		return $this->gauge_id;
	}

	/**
	 * @return the $question_id
	 */
	public function getCategory_content() {
		return $this->category_content;

	}

	/**
	 * @param field_type $cat_id
	 */
	public function setCat_id($cat_id) {
		$this->cat_id = $cat_id;
	}

	/**
	 * @param field_type $gauge_id
	 */
	public function setGauge_id($gauge_id) {
		$this->gauge_id = $gauge_id;
	}

	/**
	 * @param field_type $question_id
	 */
	public function setCategory_content($question_id) {
		$this->category_content = $question_id;
	}
	/**
	 * @return the $category_name
	 */
	public function getCategory_name() {
		return $this->category_name;
	}

	/**
	 * @param field_type $category_name
	 */
	public function setCategory_name($category_name) {
		$this->category_name = $category_name;
	}




	
}

?>