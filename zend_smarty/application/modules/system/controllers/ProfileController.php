<?php
class System_ProfileController extends Zend_Controller_Action
{

    public function init()
    {
    	//$this->_helper->viewRenderer->setNoRender(true);
    	$this->view->admin_name=Zend_Auth::getInstance()->getIdentity()->username;
    	$this->view->title="系统管理";
    }

    public function indexAction()
    {
    	$page=$this->getRequest()->getParam("page");
	  	$mapper=new User_Model_UserMapper();
	   	$result=$mapper->findAll($page?$page:1,30);
	   	$this->view->users=$result;
	   	$this->view->user=new User_Model_User();
	   	$this->view->page=$page;
    }
    public function searchAction()
    {
    	
    }
    
    public function entryAction(){
    	$user_id=$this->getRequest()->getParam("user_id");
    	$this->view->sex=$this->getRequest()->getParam("sex");
    	$this->view->page=$this->getRequest()->getParam("returnPage");
    	$entry=new System_Model_SettingMapper();
    	
    	$result = new User_Model_AnswerMapper();
    	$aaa=$result->findComplished($user_id);
    	$this->view->result=$result->findComplished($user_id);
    	$en=$entry->find($user_id);
    	$this->view->setting=$en;
    	$this->view->user_id=$user_id;
    	
    }
    
    public function reportAction(){
    	$user_id=$this->getRequest()->getParam("id");
    	$userMapper=new User_Model_UserMapper();
    	$user=new User_Model_User();
    	$userMapper->find($user_id, $user);
    	$reportMapper=new User_Model_ReportMapper();
    	$report=$reportMapper->findByUserid($user_id );
    	$answerMapper=new User_Model_AnswerMapper();
    	$answers=$answerMapper->findByUserid($user_id);

        $basicMapper = new User_Model_BasicinfoMapper();
        $basic = new User_Model_Basicinfo();
        $basicMapper->find($user_id,$basic);


    	$date=new DateTime();
    	$this->view->time=$date->format("Y年m月d日");
    	$this->view->answers=$answers;    	
    	$this->view->report=$report;
		$this->view->username=$user->getName();
		$this->view->userid=$user->getId();
		$this->view->serial=$user->getSerial();
		$this->view->id_card=$user->getId_card();
        $this->view->basic = $basic;
		
    }
    

}