<script>
	$(function(){
		$("#update_admin").click(function(){
			if($("#password").val()!=$("#repeat").val()){
				toastr.warning("新密码两次不一样，请重新输入");
				return;
			}
			$.get("/system/admin/{~$admin_id~}/edit",{oldpass:$("#oldpass").val(),password:$("#password").val()}).promise().done(function(){
				toastr.success("修改成功！");
				document.getElementById("edit-profile2").reset();
			}).fail(function(data){
				toastr.warning(data.responseText);
			});
		});
	});
</script>
<input id="user_id" type="hidden" value="">
<form class="form-horizontal" id="edit-profile2">
		<div class="control-group">											
			<label class="control-label">用户名</label>
			<div class="controls">
				<input type="text" class="input-very-large disabled" disabled="" value="{~$admin_name~}">
			</div> <!-- /controls -->				
		</div> <!-- /control-group -->

		<div class="control-group">											
			<label class="control-label">旧密码</label>
			<div class="controls">
				<input type="password" id="oldpass" class="input-very-large" value="">
			</div> <!-- /controls -->				
		</div> <!-- /control-group -->
		
		<div class="control-group">											
			<label class="control-label">新密码</label>
			<div class="controls">
				<input type="password" id="password" class="input-very-large" value="">
			</div> <!-- /controls -->				
		</div> <!-- /control-group -->
		
		<div class="control-group">											
			<label class="control-label">重复新密码</label>
			<div class="controls">
				<input type="password" id="repeat" class="input-very-large" value="">
			</div> <!-- /controls -->				
		</div> <!-- /control-group -->	
		
		<div class="control-group">											
			<label class="control-label"></label>
			<div class="controls">
				<a class="btn btn-info" id="update_admin">更新</a>
			</div> <!-- /controls -->				
		</div> <!-- /control-group -->							
</form>

