{~include file="header.tpl"~}
<body>
    <script>
        $(function(){

            if(!"{$hasDatabase}"){
                //alert("has");
                create_guid();
            }
            
        });
     
     
     
        
        function create_guid(){
            $("#dialog_create").dialog({
                width:"480px",
                minHeight:'320px',
                closeOnEscape:false,
                position:"center",
                modal:true,
                draggable:false,
                resizable:false,
                title:"新建数据库向导",
                show:{
                    effect:'fade',
                    duration:1200
                },
                buttons: {
                    "下一步":function(){
                        window.location.href="/default/install/create";
                    }
                }
            });
        }
        
        
    </script>
    <style>
        .ui-dialog-titlebar-close{
         display: none;
        }

    </style>
    <div class="page" style="990px;" id="container">
        
        <div id="dialog_create" class="ui-dialog ui-widget ui-widget-content ui-corner-all ui-dialog-buttons">
           <div style="height: 260px;font-size: 16px;">
               <p class="dialog_p">
                   欢迎您首次运行本系统程序，系统检测到mysql已经安装正确，但是并没有检测到系统数据库，现在点击<span style="color:#0073EA;">下一步</span>进入向导程序。
               </p> 
           </div>
        </div>
        

        
    </div>
</body>
{~include file="footer.tpl"~}