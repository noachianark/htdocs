<?php
class User_GaugeController extends Zend_Rest_Controller
{

    public function init()
    {
//     	$this->view->admin_name=Zend_Auth::getInstance()->getIdentity()->username;
//     	$this->view->title="系统管理->量表管理";
 		$this->_helper->viewRenderer->setNoRender(true);
    }

    public function indexAction()
    {
    	$this->_helper->viewRenderer->setNoRender(false);
    	$gid=$this->getRequest()->getParam("id");
    	$page=$this->getRequest()->getParam("page");
    	$questionMapper=new System_Model_QuestionMapper();
    	$questions=$questionMapper->findAll($gid,$page,10,"ASC");
    	$this->view->questions=$questions;
    	
    }
    
 
    
    public function getAction(){
    	$aa='[{"lala":5},{"xx":6}]';
    	$yy=json_decode($aa);
    	$aaaa="";
    }
    
    
    
    public function postAction(){

    }

    
    public function deleteAction(){
    	echo "delete one or more";
    }
    public function putAction(){
    	
    	$answers = $this->getRequest()->getParam("answers");
    	$gid = $this->getRequest()->getParam("id");
    	$gaugeMapper = new System_Model_GaugeMapper();
    	$gauge = new System_Model_Gauge();
    	$gaugeMapper->find($gid, $gauge);
    	$dimenMapper = new System_Model_DimensionMapper();
    	$dimensions = $dimenMapper->findAllList($gid);
    	$conclusion = new stdClass();
    	$conclusion->comment=array();
        $conclusion->passed=true; 
        foreach($dimensions as $dimension){
            $dimension['count']=0;
            $dimension['score']=0;
            if(isset($dimension['rules'])){
                $dimension['rules']=json_decode($dimension['rules']);
            }
            foreach($answers as $answer){
                if($answer['dimension_id']==$dimension['id']){
                    //这里放到维度数组中。
                    (int)$dimension['score']+=(int)$answer['value'];
                    (int)$dimension['count']+=1;
                }
            }
            //这里计算均数。
            //(int)$dimension['score']=(int)$dimension['sum']/(int)$dimension['count'];
            if(isset($dimension['rules'])){//存在重新计分规则。
                foreach($dimension['rules'] as $rule){
                    if((int)$dimension['score']>=$rule->min && (int)$dimension['score']<=$rule->max){
                        $dimension['score']=$rule->alter;
                    }
                }

			}else{
				if(!$gauge->getIs_average()){
					(int)$dimension['score']=round((int)$dimension['score']/(int)$dimension['count'],2);
				}
			}
			
			
			
			$result=new User_Model_Result();
			$result->setScore($dimension['score']);
			$result->setDimension_name($dimension['name']);
			$result->setDecisive($dimension['decisive']);

			if($dimension['score']>=$dimension['boundary']){
				$result->setConclusion("得分:".$dimension['score']."分\n".$dimension["high"]);
				if($dimension['lowpass']==1){
					if($dimension['decisive']==1){
						$conclusion->passed=false;
                        $result->setPassed(false);
					}
				}else{
					$result->setPassed(true);
				}
			}else{
				$result->setConclusion("得分:".$dimension['score']."分\n".$dimension["low"]);
				if($dimension['lowpass']==1){
					$result->setPassed(true);
				}else{
					if($dimension['decisive']==1){
						$conclusion->passed=false;
                        $result->setPassed(false);
					}
				}
			}
			array_push($conclusion->comment, $result);
		}
		
		
		
		
		
		$answer=new User_Model_Answer();
		$answer->setUser_id(Zend_Auth::getInstance()->getIdentity()->id);
		$answer->setGauge_id($gid);
		$answer->setResult($conclusion);
		
		$answerMapper=new User_Model_AnswerMapper();
		$result=$answerMapper->save($answer);
		
		//先保存，保存完后查看还有题目否
		$gaugeMapper=new System_Model_GaugeMapper();
		$result=$gaugeMapper->findPublished(Zend_Auth::getInstance()->getIdentity()->sex, Zend_Auth::getInstance()->getIdentity()->id);
		if(count($result->rows)){
			//返回列表。
			$this->getResponse()->setHttpResponseCode(201);
		}else{
			//计算综合成绩存入报告表。
			
			
			$user=new User_Model_User();
			$userMapper=new User_Model_UserMapper();
			$userMapper->find(Zend_Auth::getInstance()->getIdentity()->id, $user);
			if($user->getState() === 1){
				$date=new DateTime();
				$user->setClear_time($date->format('Y-m-d H:i:s'));
			}
			$user->setFinish(true);
			$stdClass=$answerMapper->findComplished(Zend_Auth::getInstance()->getIdentity()->id);
        	if($stdClass->speed===true && $stdClass->response===true && $stdClass->deep===true && $stdClass->sight===true){
        		$user->setState(true);
        		$date=new DateTime();
    			$user->setClear_time($date->format('Y-m-d H:i:s'));
        	}
			$finish=$userMapper->update($user);
			if(!is_numeric($finish)){
				$this->getResponse()->setHttpResponseCode(422)->appendBody("更新用户信息失败");
				return;
			}
			
			$ans=$answerMapper->findByUserid(Zend_Auth::getInstance()->getIdentity()->id);
			$pass=true;
			foreach($ans as $an){
				$con=$an->getResult();
				if($con->passed===false){
					$pass=false;
				}
			}
			$report=new User_Model_Report();
			$report->setUser_id(Zend_Auth::getInstance()->getIdentity()->id);
			$report->setPassed($pass);
			
			$reportMapper=new User_Model_ReportMapper();
			$reportMapper->save($report);
			
			$this->getResponse()->setHttpResponseCode(200);
			
		}
    }
    public function headAction(){
    	 
    }

    


}