<?php
class System_Model_QuestionMapper {
	protected $_dbTable;
	
	public function setDbTable($dbTable)
	{
		if (is_string($dbTable)) {
			$dbTable = new $dbTable();
		}
		if (!$dbTable instanceof Zend_Db_Table_Abstract) {
			throw new Exception('Invalid table data gateway provided');
		}
		$this->_dbTable = $dbTable;
		return $this;
	}
	public function getDbTable(){
		if (null === $this->_dbTable) {
			$this->setDbTable('System_Model_DbTable_Question');
		}
		return $this->_dbTable;
	}
	public function save(System_Model_Question $question){
		try{
			$result=$this->getDbTable()->insert($question->toArray());
		}catch (Exception $e){
			return Utils_SQLErrorMsg::formatException($e);
		}
		return (int)$result;
	}
	
	/**
	 * @param array | $ids
	 * @param array | $filter
	 * @return string|number
	 */
	public function update(System_Model_Question $question){
			try{
				//$result=$this->getDbTable()->update($filter, "qid in(".implode(',',$ids).")");
			$result=$this->getDbTable()->update($question->toArray(), $this->getDbTable()->getAdapter()->quoteInto('qid = ?', $question->getQid()));
			}catch(Exception $e){
				return Utils_SQLErrorMsg::formatException($e);
			}
		return (int)$result;
	}
	
	public function find($id){
		try{
			$data=$this->getDbTable()->fetchRow("qid=".$id)->toArray();
			$model=new System_Model_Question($data);
		}catch(Exception $e){
			return Utils_SQLErrorMsg::formatException($e);
		}
		return $model;
	}
	
	public function getRecords($gid){
		$rows=$this->getDbTable()->getAdapter()->fetchOne("select count(*) from questions where gid=".$gid);
		return $rows;
	}

	
	public function findAll($gid,$curPage=1,$perPage=20,$sort="DESC"){
		$rows=$this->getDbTable()->getAdapter()->fetchOne("select count(*) from questions where gid=".$gid);
// 		$select=$this->getDbTable()->select("qid,sequence,content,name from questions,dimension,dimension_id where questions.dimension_id=dimension.id")
// 							->order("sequence")
// 							->limitPage($curPage, $perPage);
		$select=$this->getDbTable()->select()->setIntegrityCheck(false)
							->from("questions",array("qid","sequence","content","choices","dimension_id"))
							->join("dimension", "questions.dimension_id=dimension.id",array("id","name"))->where("questions.gid=".$gid)
							->order("sequence ".$sort)
							->limitPage($curPage, $perPage);
		$data=array();
		try{
			$data=$this->getDbTable()->fetchAll($select)->toArray();
		}catch(Exception $e){
			return $e;
		}
		$result=array();
		foreach($data as $question){
			$model=new System_Model_Question($question);
			$arr=$model->toArray();
			$arr['name']=$question['name'];
			array_push($result,$arr);
		}
		$response=new stdClass();
		$response->page=$curPage;
		$response->total=ceil($rows/$perPage);
		$response->records=(int)$rows;
		$response->rows=$result;
		
		return $response;
	}
	
	public function deleteAll($gid){
		try{
			$this->getDbTable()->delete("gid=".$gid);
		}catch(Exception $e){
			return $e;
		}
	}
	
	public function count(){
		try{
			$select=$this->getDbTable()->select();
			$result=$this->getDbTable()->fetchAll($select)->count();
			return $result;
		}catch(Exception $e){
			return Utils_SQLErrorMsg::formatException($e);
		}
	}
	
}
