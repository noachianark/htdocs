<?php
require_once 'Zend/Db/Table/Abstract.php';
class Default_Model_DbTable_Dish extends Zend_Db_Table_Abstract{
	protected $_name="dishes";
	protected $_primary="id";
	protected $_dependentTables = array('Default_Model_DbTable_DishMaterial',"Default_Model_DbTable_Cookbook");
}