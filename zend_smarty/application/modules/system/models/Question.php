<?php
class System_Model_Question {
	protected $qid;
	protected $gid;
	protected $dimension_id;
	protected $sequence;
	protected $content;
	protected $single;
	protected $choices;

	public function __construct(array $options = null)
	{
		if (is_array($options)) {
			$this->setOptions($options);
		}
	}
	
	public function setOptions(array $options)
	{
		$methods = get_class_methods($this);
		foreach ($options as $key => $value) {
			$method = 'set' . ucfirst($key);
			if (in_array($method, $methods)) {
				$this->$method($value);
			}
		}
		return $this;
	}
	public function toJson(){
		return json_encode($this->toArray());
	}
	
	public function toArray(){
		$array=array();
		foreach ($this as $key => $value) {
			if($value!=null){
				$array[$key] = $value;
			}
		}
		return $array;
	}


	/**
	 * @return the $dimension_id
	 */
	public function getDimension_id() {
		return $this->dimension_id;
	}

	/**
	 * @param field_type $dimension_id
	 */
	public function setDimension_id($dimension_id) {
		$this->dimension_id = $dimension_id;
	}

	/**
	 * @return the $option
	 */
	public function getChoices() {
		return $this->choices;
	}
	
	/**
	 * @param multitype: $option
	 */
	public function setChoices($option) {
		if(is_array($option)){
			$this->choices = serialize($option);
		}else{
			$this->choices=unserialize($option);
		}
	}
	/**
	 * @return the $qid
	 */
	public function getQid() {
		return $this->qid;
	}

	/**
	 * @return the $gid
	 */
	public function getGid() {
		return $this->gid;
	}

	/**
	 * @return the $sequence
	 */
	public function getSequence() {
		return $this->sequence;
	}

	/**
	 * @return the $content
	 */
	public function getContent() {
		return $this->content;
	}

	/**
	 * @return the $single
	 */
	public function getSingle() {
		return $this->single;
	}

	/**
	 * @param field_type $qid
	 */
	public function setQid($qid) {
		$this->qid = $qid;
	}

	/**
	 * @param field_type $gid
	 */
	public function setGid($gid) {
		$this->gid = $gid;
	}

	/**
	 * @param field_type $sequence
	 */
	public function setSequence($sequence) {
		$this->sequence = $sequence;
	}

	/**
	 * @param field_type $content
	 */
	public function setContent($content) {
		$this->content = $content;
	}

	/**
	 * @param field_type $single
	 */
	public function setSingle($single) {
		$this->single = $single;
	}


	
	
	
}

?>