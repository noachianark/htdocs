<style>
	#questions .item{
		margin-left:10px;
		margin-bottom:15px;
	}
	#questions .question{
		text-align:left;
		padding-right:20px;
		font-size:15px;
		color:#656766;
		padding-top:5px;
		display:inline;
	}
</style>
<div>
	返回评测中心列表
</div>
<div id="questions" style="margin-left:20px;margin-right:20px;">
{~foreach from=$questions item=item~}
	<div class="item">
		<label class="question">{~$item['content']~}</label>
		<div class="answer">
			{~foreach from=$item['option'] item=answer~}
				<div class="back_answer">{~$answer['answer']~}</div>
			{~/foreach~}
		</div>
	</div>
{~/foreach~}
</div>