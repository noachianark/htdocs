<?php
class User_Model_Answer{
	protected $id;
	protected $user_id;
	protected $gauge_id;
	protected $result;
	
	public $guage_name;
	
	public function __construct(array $options = null)
	{
		if (is_array($options)) {
			$this->setOptions($options);
		}
	}
	
	public function setOptions(array $options)
	{
		$methods = get_class_methods($this);
		foreach ($options as $key => $value) {
			$method = 'set' . ucfirst($key);
			if (in_array($method, $methods)) {
				$this->$method($value);
			}
		}
		return $this;
	}
	
	public function toJson(){
		return json_encode($this->toArray());
	}
	
	public function toArray(){
		$array=array();
		foreach ($this as $key => $value) {
			if($value!=null){
				$array[$key] = $value;
			}
		}
		return $array;
	}
	/**
	 * @return the $id
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * @return the $user_id
	 */
	public function getUser_id() {
		return $this->user_id;
	}

	/**
	 * @return the $gauge_id
	 */
	public function getGauge_id() {
		return $this->gauge_id;
	}

	/**
	 * @return the $result
	 */
	public function getResult() {
		return $this->result;
	}

	/**
	 * @param field_type $id
	 */
	public function setId($id) {
		$this->id = $id;
	}

	/**
	 * @param field_type $user_id
	 */
	public function setUser_id($user_id) {
		$this->user_id = $user_id;
	}

	/**
	 * @param field_type $gauge_id
	 */
	public function setGauge_id($gauge_id) {
		$this->gauge_id = $gauge_id;
	}

	/**
	 * @param field_type $result
	 */
	public function setResult($result) {
		if(is_object($result)){
			$this->result = serialize($result);
		}else{
			$this->result=unserialize($result);
		}
	}


	
	

}