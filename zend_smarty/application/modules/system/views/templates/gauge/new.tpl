{~include file="header.tpl"~}
<link href="/css/plans.css" rel="stylesheet" />
<script>
	
	function saveGauge(){
		var bValid=true;
		bValid=bValid && validateLength($("#gauge_name"),3,30,"量表名称");
		bValid=bValid && validateRegexp($("#time"),/^([0-9])+$/i,"<时间限制>一栏只能填写数字");
		bValid=bValid && validateLength($("#time"),1,4,"时间限制");
		bValid=bValid && validateLength($("#introduction"),3,500,"引导语");
		bValid=bValid && validateLength($("#peroration"),3,500,"结论语");
		if(bValid){
			$.post($("#gid").val().length>0?"/system/gauge/"+$("#gid").val():"/system/gauge/",
			{
				gid:$("#gid").val(),
				gauge_name:$("#gauge_name").val(),
				time_limit:$("#time").val(),
				introduction:$("#introduction").val(),
				peroration:$("#peroration").val(),
				description:$("#gauge_description").val(),
				sex:$("input[name='optionsRadios']:checked").val(),
				manual:$("input[name='manual']:checked").val(),
				is_average:$("input[name='is_average']:checked").val()
			}).promise().done(function(data){
				console.log(data);
				toastr.success("保存成功");
				setTimeout(function(){
					window.location.href="/system/gauge/"+data+"/edit";					
				},1500);
			}).fail(function(data){
				console.log(data);
				toastr.error(data.responseText,"错误");
			});
		}
	}
	
	function saveDimension(){
		var bValid=true;
		bValid=bValid && validateLength($("#dimension_name"),3,30,"维度名称");
		bValid=bValid && validateLength($("#dimension_desc"),3,100,"描述信息");
		bValid=bValid && validateLength($("#boundary"),1,5,"界限值");
		bValid=bValid && validateRegexp($("#boundary"),/^-?\d+\.?\d*$/,"<区间最小值>一栏只能填写数字");
		if(bValid){
			var url="/system/dimension/";
			if($("#dimension_id").val()){
				url+=$("#dimension_id").val();
			}
			$.post(url,{
				gid:$("#gid").val(),
				name:$("#dimension_name").val(),
				description:$("#dimension_desc").val(),
				boundary:$("#boundary").val(),
				lowpass:$("input[name='lowpass']:checked").val(),
				rules:$("#dimension_attributes").val(),
				decisive:$("input[name='decisive']:checked").val(),
				high:$("#high").val(),
				low:$("#low").val()
			}).promise().done(function(data){
				$.get("/system/dimension/index/gid/"+$("#gid").val()+"/page/1").promise()
				.done(function(data){
					$("#dimension_list").html(data);
					$('#dimensionDialog').modal('hide');
				}).fail(function(data){
					toastr.error(data.responseText,"好像出错了");
				});
			}).fail(function(data){
				toastr.error(data.responseText,"出错了");
			});
		}
	}
	
	function modifyDimension(id){
		$("#dimensionDialog p").load("/system/dimension/"+id+"/edit");
	}
	
	function deleteDimension(id){
		
	}
	
	function newDimension(){
		$("#dimensionDialog p").load("/system/dimension/new");
	}
	
	$(function(){
		switch($("#tab").val()){
			case "que":
				$('.nav-tabs #que').tab('show');
				break;
			case "dim":
				$('.nav-tabs #dim').tab('show');
				break;
			case "bas":
			default:
				$('.nav-tabs #bas').tab('show');
				break;
		}
		if($("#gid").val()){
			$.get("/system/dimension/index/gid/"+$("#gid").val()+"/page/1").promise()
			.done(function(data){
				$("#dimension_list").html(data);
			}).fail(function(data){
				toastr.error(data.responseText,"好像出错了");
			});
		}
		
        $('a[data-toggle="tab"]').on('show', function (e) {
			if(e.target.getAttribute("id")=="que"){
				$("#questions").load("/system/question/index/gid/"+$("#gid").val()+"/page/1");
			}
        });
		
		$("#gauge_navi").addClass("active");
	});
	
	
</script>
<h1 class="page-title">
	<i class="icon-th-list"></i>
	<span style="cursor:pointer;" onclick="window.location.href='/system'">量表管理</span>
	>
	<span>
		{~if $gauge->getGid()~}
			设定量表
		{~else~}
			新建量表
		{~/if~}
	</span>
</h1>
<div id="dimension_dialog" class="display_off"></div>
<input id="gid" value="{~$gauge->getGid()~}" type="hidden">
<input id="tab" value="{~$tab~}" type="hidden">
<div class="widget">
							
		<div class="widget-header">
			<h3 style="margin-top:10px;">
					 {~if $gauge->getGid()~}
						设定量表：{~$gauge->getGauge_name()~}
					{~else~}
						新建量表信息
					{~/if~}
			</h3>
		</div> <!-- /widget-header -->
				
		<div class="widget-content">
			<div class="tabbable">

				<ul class="nav nav-tabs" id="gauge_tab">
				  <li class="active"><a data-toggle="tab" href="#basic" id="bas">基本信息</a></li>
				  {~if $gauge->getGid()~}
				  <li class=""><a data-toggle="tab" href="#dimension" id="dim">量表维度</a></li>
				  <li class=""><a data-toggle="tab" href="#questions" id="que">评测题目</a></li>
				  {~/if~}
				</ul>

				<div class="tab-content">
					
					<div id="basic" class="tab-pane active">
						<form class="form-horizontal" id="edit-profile">
							<fieldset>
								
								<div class="control-group">											
									<label for="gauge_name" class="control-label">量表名称</label>
									<div class="controls">
										<input type="text" value="{~$gauge->getGauge_name()~}" id="gauge_name" class="input-very-large">
										<p class="help-block">量表名称要求唯一，并且将显示在测试当中。</p>
									</div> <!-- /controls -->				
								</div> <!-- /control-group -->
								
								
								<div class="control-group display_off">											
									<label for="time" class="control-label">测试时间限制</label>
									<div class="controls">
										<input type="text" value="{~$gauge->getTime_limit()~}" id="time" class="input-small">分钟
										<p class="help-block">测试时间如果不限，则填写0</p>
									</div> <!-- /controls -->				
								</div> <!-- /control-group -->

								<div class="control-group">											
									<label for="gauge_description" class="control-label">量表描述</label>
									<div class="controls">
										<textarea type="text" id="gauge_description" class="input-very-large" >{~$gauge->getDescription()~}</textarea>
										<p class="help-block"></p>
									</div> <!-- /controls -->				
								</div> <!-- /control-group -->								
								
								<div class="control-group">											
									<label for="introduction" class="control-label">引导语</label>
									<div class="controls">
										<textarea type="text" id="introduction" class="input-very-large" >{~$gauge->getIntroduction()~}</textarea>
										<p class="help-block"></p>
									</div> <!-- /controls -->				
								</div> <!-- /control-group -->
								
								
								<div class="control-group">											
									<label for="peroration" class="control-label">结论语</label>
									<div class="controls">
										<textarea type="text" id="peroration" class="input-very-large">{~$gauge->getPeroration()~}</textarea>
									</div> <!-- /controls -->				
								</div> <!-- /control-group -->


								<div class="control-group">											
									<label for="is_average" class="control-label">是否计算总分</label>
									<div class="controls">
										{~if $gauge->getIs_average() eq 1~}
											<label class="radio">
												<input type="radio" value="1"  class="question_input" name="is_average" checked="true">是
											</label>
											<label class="radio">
												<input type="radio" value="0" class="question_input" name="is_average">否
											</label>
										{~else~}
											<label class="radio">
												<input type="radio" value="1"  class="question_input" name="is_average">是
											</label>
											<label class="radio">
												<input type="radio" value="0" class="question_input" name="is_average" checked="true">否
											</label>
										{~/if~}			
									</div> <!-- /controls -->				
								</div> <!-- /control-group -->		
								
								
								<div class="control-group">											
									<label for="lowpass" class="control-label">非仪器量表</label>
									<div class="controls">
										{~if $gauge->getManual() eq 1~}
											<label class="radio">
												<input type="radio" value="1"  class="question_input" name="manual" checked="true">是
											</label>
											<label class="radio">
												<input type="radio" value="0" class="question_input" name="manual">否
											</label>
										{~else~}
											<label class="radio">
												<input type="radio" value="1"  class="question_input" name="manual">是
											</label>
											<label class="radio">
												<input type="radio" value="0" class="question_input" name="manual" checked="true">否
											</label>
										{~/if~}			
									</div> <!-- /controls -->				
								</div> <!-- /control-group -->									
								
								
								
								<div class="control-group">
						            <label class="control-label">性别限制</label>
						            <div class="controls">
						            	{~if $gauge->getSex() eq 0~}
						              <label class="radio">
						                <input type="radio"  checked="checked" value="0" id="optionsRadios1" name="optionsRadios">
											不限制
						              </label>
						              <label class="radio">
						                <input type="radio" value="1" id="optionsRadios2" name="optionsRadios">
											男性
						              </label>
						              <label class="radio">
						                <input type="radio" value="2" id="optionsRadios3" name="optionsRadios">
											女性
						              </label>										
										{~elseif $gauge->getSex() eq 1~}
									  <label class="radio">
						                <input type="radio"  value="0" id="optionsRadios1" name="optionsRadios">
											不限制
						              </label>
						              <label class="radio">
						                <input type="radio" checked="checked"  value="1" id="optionsRadios2" name="optionsRadios">
											男性
						              </label>
						              <label class="radio">
						                <input type="radio" value="2" id="optionsRadios3" name="optionsRadios">
											女性
						              </label>
										{~else~}
						              <label class="radio">
						                <input type="radio"   value="0" id="optionsRadios1" name="optionsRadios">
											不限制
						              </label>
						              <label class="radio">
						                <input type="radio" value="1" id="optionsRadios2" name="optionsRadios">
											男性
						              </label>
						              <label class="radio">
						                <input type="radio" value="2" checked="checked" id="optionsRadios3" name="optionsRadios">
											女性
						              </label>										
										{~/if~}

						            </div>
						          </div>
								
								<br>
								
									
								<div class="form-actions">
									<a class="btn btn-primary" onclick="saveGauge()">保存信息</a> 
								</div> <!-- /form-actions -->
							</fieldset>
						</form>
					</div>
					
					<div id="dimension" class="tab-pane">
						<a data-target="#dimensionDialog" onclick="newDimension()" role="button" data-keyboard="true" data-backdrop="true"  data-toggle="modal" class="btn btn-success"  style="margin-bottom: 1.25em;">
							<i class="icon-plus icon-large pull-left" style="line-height:inherit;margin-top:0px;margin-right:3px;"></i>
							新建维度	
						</a>						
						<div id="dimension_list"></div>					
					</div>

				    <!-- Modal -->
				    <div id="dimensionDialog" style="width:680px;" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="dimensionDialogLabel" aria-hidden="true">
					    <div class="modal-header">
					    	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
					    	<h3 id="dimensionDialogLabel">设置维度</h3>
					    </div>
					    <div class="modal-body">
					    	<p></p>
					    </div>
					    <div class="modal-footer">
					    	<button class="btn" data-dismiss="modal" aria-hidden="true">关闭</button>
					    	<button class="btn btn-primary" onclick="saveDimension()">保存设置</button>
					    </div>
				    </div>



					<div id="questions" class="tab-pane">
						
					</div>

					
				</div>
			</div>
		</div> <!-- /widget-content -->
		
	</div>

{~include file="footer.tpl"~}