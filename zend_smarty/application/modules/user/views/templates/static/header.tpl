<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>{~$title~}</title>
<link rel="stylesheet" type="text/css" href="/css/custom-theme/jquery-ui-1.10.1.custom.min.css">
<link rel="stylesheet" type="text/css" href="/css/ui.jqgrid.css">
<link rel="stylesheet" type="text/css" href="/css/global.css"/>
<link rel="stylesheet" type="text/css" href="/css/toastr.min.css"/>

<link href="/css/bootstrap.min.css" rel="stylesheet" />
<link href="/css/bootstrap-responsive.min.css" rel="stylesheet" />

<link href="/css/fonts.css" rel="stylesheet" />
<link href="/css/font-awesome.css" rel="stylesheet" />

<link href="/css/adminia.css" rel="stylesheet" /> 
<link href="/css/adminia-responsive.css" rel="stylesheet" /> 
<script src="/scripts/jquery-1.8.3.js"></script>
    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
</head>

<body>
<style>
	.input-very-large{
		width:350px;
	}
</style>
<script>
	function validateLength(obj,min,max,tip){
		if(obj.val().length>max || obj.val().length<min){
			toastr.warning(tip+"的长度必须在"+min+"到"+max+"之间","提示");
			return false;
		}
		return true;
	}
	function validateRegexp(obj,regexp,tip){
		if(!(regexp.test(obj.val()))){
			toastr.warning(tip,"提示");
			return false;
		}
		return true;
	}
	function validateEquals(value1,value2,tip){
		if(value1.val()!=value2.val()){
			toastr.warning(tip,"提示");
			return false;
		}
		return true;
	}
	function navigate(obj){
		$("#main-nav li").removeClass("active");
		//$(obj).addClass("active");
	}
</script>
<div class="navbar navbar-fixed-top">
	
	<div class="navbar-inner">
		
		<div class="container">
			
			
			<a href="/system" class="brand">心理测评平台</a>
			
			<div class="nav-collapse in" style="height: 0px;">
			
				<ul class="nav pull-right">
					<li>
						<a onclick="$.get('/index/logout').promise().done(function(){window.location.href='/'})"><span class="badge badge-warning">注销</span></a>
					</li>
					
					<li class="divider-vertical"></li>
					
					<li class="dropdown">
						
						<a href="#" class="dropdown-toggle " data-toggle="dropdown">
							{~$username~} <b class="caret"></b>							
						</a>
						
						<ul class="dropdown-menu">
							<li>
								<a href="./account.html"><i class="icon-user"></i> 账号设置  </a>
							</li>
							
							<li>
								<a href="./change_password.html"><i class="icon-lock"></i> 修改密码</a>
							</li>
							
							<li class="divider"></li>
							
							<li>
								<a onclick="$.get('/index/logout').promise().done(function(){window.location.href='/'})"><i class="icon-off"></i> 注销</a>
							</li>
						</ul>
					</li>
				</ul>
				
			</div> <!-- /nav-collapse -->
			
		</div> <!-- /container -->
		
	</div> <!-- /navbar-inner -->
	
</div>	
	

<div id="content">
	<div class="container">
		<div class="row">


