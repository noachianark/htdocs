<?php
class Default_Model_DishMaterialMapper {
	protected $_dbTable;
	
	public function setDbTable($dbTable)
	{
		if (is_string($dbTable)) {
			$dbTable = new $dbTable();
		}
		if (!$dbTable instanceof Zend_Db_Table_Abstract) {
			throw new Exception('Invalid table data gateway provided');
		}
		$this->_dbTable = $dbTable;
		return $this;
	}
	public function getDbTable(){
		if (null === $this->_dbTable) {
			$this->setDbTable('Default_Model_DbTable_DishMaterial');
		}
		return $this->_dbTable;
	}
	public function save(Default_Model_DishMaterial $dishmaterial){
		try{
			$result=$this->getDbTable()->insert($dishmaterial->toArray());
		}catch (Exception $e){
			return json_encode($dishmaterial);
		}
		return (int)$result;
	}
	public function update(Default_Model_Dish $dish){
		if($dish->getId()){
			try{
				$result=$this->getDbTable()->update($dish->toArray(), "id=".$dish->getId());
			}catch(Exception $e){
				return "数据格式不正确";
			}
		}else{
			return "没有找到该记录，无法更新";
		}
		return (int)$result;		
	}
	
	public function findByDayTime($day,$time){
		try{
			$select=$this->getDbTable()->select()->where("day = $day")->where("time = $time");
			$data = $this->getDbTable()->fetchAll($select)->toArray();
			return $data;
		}catch(Exception $e){
			return $e;
		}

	}
	
	public function findAll(){
		try{
			$result = $this->getDbTable()->fetchAll()->toArray();
			return $result;
		}catch(Exception $e){
			return $e;
		}
	}
	
	
	public function delete($dish_id,$material_id){
		try{
			$select = $this->getDbTable()->select()->where("dish_id=$dish_id")->where("material_id=$material_id");
			$result = $this->getDbTable()->fetchAll($select)->current();
			$result->delete();
			return "success";
		}catch(Exception $e){
			return $e;
		}
	}
	
	public function findMaterial($id){
		try{
			$rowset = $this->getDbTable()->find($id);
			$dish = $rowset->current();
			$result = $dish->findDependentRowset("Default_Model_DbTable_DishMaterial");
			$cls = array("dish"=>$dish->toArray(),"materials"=>$result->toArray());
			 return $cls;
		}catch(Exception $e){
			return $e;
		}
	}

	

	
}
