<script>
	function dimension_page(url){
		$.get(url).promise()
		.done(function(data){
			$("#dimension_list").html(data);
		}).fail(function(data){
			toastr.error(data.responseText,"好像出错了");
		});
	}
</script>
	<div class="widget  widget-table">		
		<div class="widget-header">
			<i class="icon-th-list"></i>
			<h3>维度列表</h3>
			<div class="pull-right" style="margin-right:15px;">
			{~if ($dimensions->page > 1) and ($dimensions->page < $dimensions->total) ~}
				<a onclick="dimension_page('/system/dimension/index/gid/{~$dimensions->gid~}/page/{~$dimensions->page-1~}')" >上一页</a>
				 | 
				 <a onclick="dimension_page('/system/dimension/index/gid/{~$dimensions->gid~}/page/{~$dimensions->page+1~}')">下一页</a> |
			{~elseif $dimensions->page eq 1 ~}
				<a onclick="dimension_page('/system/dimension/index/gid/{~$dimensions->gid~}/page/{~$dimensions->page+1~}')">下一页</a> |
			{~else~}
				<a onclick="dimension_page('/system/dimension/index/gid/{~$dimensions->gid~}/page/{~$dimensions->page-1~}')">上一页</a> |
			{~/if~}
				总页数：{~$dimensions->total~} | 当前页：{~$dimensions->page~}
			</div>
		</div> <!-- /widget-header -->
		<div class="widget-content">					
			<table class="table table-striped table-bordered">
				<thead>
					<th>id号</th>
					<th>维度名称</th>
					<th>描述</th>
					<th> </th>
				</thead>
				<tbody>
					{~foreach $dimensions->rows as $dimension~}
						<tr>
							<td style="width:30px;">{~$dimension['id']~}</td>
							<td>{~$dimension['name']~}</td>
							<td style="width:400px;">{~$dimension['description']~}</td>
							<td class="action-td">
								
								<a  data-target="#dimensionDialog" role="button" data-toggle="modal"  class="btn" title="修改维度设置" onclick="modifyDimension($('input',this).val())">
									<input type="hidden" class="did" value="{~$dimension['id']~}">
									<i class="icon-cog"></i>								
								</a>
								<a class="btn btn-danger" title="删除维度">
									<i class="icon-trash"></i>						
								</a>				
							</td>
						</tr>
					{~/foreach~}
				</tbody>
			</table>
		</div> <!-- /widget-content -->
	</div>