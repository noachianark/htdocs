        <script>

			var tableToExcel = (function() {
			  var uri = 'data:application/vnd.ms-excel;base64,'
			    , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>'
			    , base64 = function(s) { return window.btoa(unescape(encodeURIComponent(s))) }
			    , format = function(s, c) { return s.replace(/{(\w+)}/g, function(m, p) { return c[p]; }) }
			  return function(table, name) {
			    if (!table.nodeType) table = document.getElementById(table)
			    var ctx = {worksheet: name || 'Worksheet', table: table.innerHTML}
			    window.location.href = uri + base64(format(template, ctx))
			  }
			})();



        	$(function(){
				//$("#day_type").selectpicker({style: 'btn-lg btn-primary', menuStyle: 'dropdown-inverse'});
				$(".item").on("click",function(){
	                var day = $(this).data("day");
	                var time = $(this).data("time");
	                $.get("/dish",{day:day,time:time}).done(function(data){
	                    $(".dishpanel").html(data);
	                    $(".dishpanel").slideDown();
	                    $(".main").slideUp();
	                }).fail(faultHandler);
	            });
				
				$(".btnClassify").on("click",function(){
					$.ajax({
						url:"/classify",
						type:"POST",
						data:$("#classify_form").serialize()
					}).done(function(data){
						$.get("/dish").done(function(data){
							$(".main").html(data);
						}).fail(faultHandler);
					}).fail(faultHandler);
				});
				
				$(".updateClassifyBtn").on("click",function(){
					$.ajax({
						url:"/classify/"+$(this).data("id"),
						type:"PUT"
					}).done(function(data){
						$.get("/dish").done(function(data){
							$(".main").html(data);
						}).fail(faultHandler);						
					}).fail(faultHandler);
				});
				
				$(".deleteClassifyBtn").on("click",function(){
					$.ajax({
						url:"/classify/"+$(this).data("id"),
						type:"DELETE"
					}).done(function(data){
						$.get("/dish").done(function(data){
							$(".main").html(data);
						}).fail(faultHandler);						
					}).fail(faultHandler);
				});
				
				$(".edit_classify").editable({
					title:"修改类型",
				    ajaxOptions: {
					    type: 'POST'
				    },
					params: function(params) {
					    //originally params contain pk, name and value
					    params.name = params.value;
					    return params;
				    },
					success:function(){
						$.get("/dish").done(function(data){
							$(".main").html(data);
						}).fail(faultHandler);						
					}
				});		
				
				
				
				$(".btn_export").on("click",function(){
					tableToExcel('excel', 'W3C Example Table');
				});

			});
        </script>
		
		<h4>一周菜谱</h4>
        <table class="table table-bordered table-striped table-responsive">
            <thead>
                <tr>
                    <th>#</th>
                    <th class="palette palette-alizarin">星期一</th>
                    <th class="palette palette-carrot">星期二</th>
                    <th class="palette palette-sun-flower">星期三</th>
                    <th class="palette palette-emerald">星期四</th>
                    <th class="palette palette-turquoise">星期五</th>
                    <th class="palette palette-peter-river">星期六</th>
                    <th class="palette palette-amethyst">星期日</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td style="padding:0px;vertical-align:middle;text-align: center;">早餐</td>
                    <td style="padding:0px;vertical-align:middle;text-align: center;background-color:#bdc3c7;" class="item btn-primary" data-day="0" data-time="0">
                        <div>
								{~foreach $dishMap["0"]["0"] as $dish~}
								<dd>{~$dish["name"]~}</dd>
								{~/foreach~}
                        </div>
                    </td>
                    <td style="padding:0px;vertical-align:middle;text-align: center;background-color:#bdc3c7;" class="item btn-primary" data-day="1" data-time="0">
                        <div>
								{~foreach $dishMap["1"]["0"] as $dish~}
								<dd>{~$dish["name"]~}</dd>
								{~/foreach~}
                        </div>
                    </td>
                    <td style="padding:0px;vertical-align:middle;text-align: center;background-color:#bdc3c7;" class="item btn-primary" data-day="2" data-time="0">
                        <div >
								{~foreach $dishMap["2"]["0"] as $dish~}
								<dd>{~$dish["name"]~}</dd>
								{~/foreach~}
                        </div>
                    </td>
                    <td style="padding:0px;vertical-align:middle;text-align: center;background-color:#bdc3c7;" class="item btn-primary" data-day="3" data-time="0">
                        <div>
								{~foreach $dishMap["3"]["0"] as $dish~}
								<dd>{~$dish["name"]~}</dd>
								{~/foreach~}
                        </div>
                    </td>
                    <td style="padding:0px;vertical-align:middle;text-align: center;background-color:#bdc3c7;" class="palette-concrete btn-primary item" data-day="4" data-time="0">
                        <div>
								{~foreach $dishMap["4"]["0"] as $dish~}
								<dd>{~$dish["name"]~}</dd>
								{~/foreach~}
                        </div>                       
                    </td>
                    <td style="padding:0px;vertical-align:middle;text-align: center;background-color:#bdc3c7;" class="palette-concrete btn-primary item" data-day="5" data-time="0">
                        <div>
								{~foreach $dishMap["5"]["0"] as $dish~}
								<dd>{~$dish["name"]~}</dd>
								{~/foreach~}
                        </div>                    
                    </td>
                    <td style="padding:0px;vertical-align:middle;text-align: center;background-color:#bdc3c7;" class="palette-concrete btn-primary item" data-day="6" data-time="0">
                        <div>
								{~foreach $dishMap["6"]["0"] as $dish~}
								<dd>{~$dish["name"]~}</dd>
								{~/foreach~}
                        </div>                        
                    </td>
                </tr>
                <tr>
                    <td style="padding:0px;vertical-align:middle;text-align: center;">中餐</td>
                    <td style="padding:0px;vertical-align:middle;text-align: center;background-color:#bdc3c7;" class="palette-concrete btn-primary item" data-day="0" data-time="1">
                        <div>
								{~foreach $dishMap["0"]["1"] as $dish~}
								<dd>{~$dish["name"]~}</dd>
								{~/foreach~}						
                        </div>
                    </td>
                    <td style="padding:0px;vertical-align:middle;text-align: center;background-color:#bdc3c7;" class="palette-concrete btn-primary item" data-day="1" data-time="1">
                        <div>
								{~foreach $dishMap["1"]["1"] as $dish~}
								<dd>{~$dish["name"]~}</dd>
								{~/foreach~}
                        </div>
                    </td>
                    <td style="padding:0px;vertical-align:middle;text-align: center;background-color:#bdc3c7;" class="btn-primary item" data-day="2" data-time="1">
                        <div>
								{~foreach $dishMap["2"]["1"] as $dish~}
								<dd>{~$dish["name"]~}</dd>
								{~/foreach~}
                        </div>                    
                    </td>
                    <td style="padding:0px;vertical-align:middle;text-align: center;background-color:#bdc3c7;" class="palette-concrete btn-primary item" data-day="3" data-time="1">
                        <div>
								{~foreach $dishMap["3"]["1"] as $dish~}
								<dd>{~$dish["name"]~}</dd>
								{~/foreach~}
                        </div>                    
                    </td>
                    <td style="padding:0px;vertical-align:middle;text-align: center;background-color:#bdc3c7;" class="palette-concrete btn-primary item" data-day="4" data-time="1">
                        <div>
								{~foreach $dishMap["4"]["1"] as $dish~}
								<dd>{~$dish["name"]~}</dd>
								{~/foreach~}
                        </div>                    
                    </td>
                    <td style="padding:0px;vertical-align:middle;text-align: center;background-color:#bdc3c7;" class="palette-concrete btn-primary item" data-day="5" data-time="1">
                        <div>
								{~foreach $dishMap["5"]["1"] as $dish~}
								<dd>{~$dish["name"]~}</dd>
								{~/foreach~}
                        </div>                    
                    </td>
                    <td style="padding:0px;vertical-align:middle;text-align: center;background-color:#bdc3c7;" class="palette-concrete btn-primary item" data-day="6" data-time="1">
                        <div>
								{~foreach $dishMap["6"]["1"] as $dish~}
								<dd>{~$dish["name"]~}</dd>
								{~/foreach~}
                        </div>                        
                    </td>
                </tr>
                <tr>
                    <td style="padding:0px;vertical-align:middle;text-align: center;">晚餐</td>
                    <td style="padding:0px;vertical-align:middle;text-align: center;background-color:#bdc3c7;" class="palette-concrete btn-primary item" data-day="0" data-time="2">
                        <div>
								{~foreach $dishMap["0"]["2"] as $dish~}
								<dd>{~$dish["name"]~}</dd>
								{~/foreach~}
                        </div>
                    </td>
                    <td style="padding:0px;vertical-align:middle;text-align: center;background-color:#bdc3c7;" class="palette-concrete btn-primary item" data-day="1" data-time="2">
                        <div>
								{~foreach $dishMap["1"]["2"] as $dish~}
								<dd>{~$dish["name"]~}</dd>
								{~/foreach~}
                        </div>
                    </td>
                    <td style="padding:0px;vertical-align:middle;text-align: center;background-color:#bdc3c7;" class="palette-concrete btn-primary item" data-day="2" data-time="2">
                        <div>
								{~foreach $dishMap["2"]["2"] as $dish~}
								<dd>{~$dish["name"]~}</dd>
								{~/foreach~}
                        </div>
                    </td>
                    <td style="padding:0px;vertical-align:middle;text-align: center;background-color:#bdc3c7;" class="palette-concrete btn-primary item" data-day="3" data-time="2">
                        <div>
								{~foreach $dishMap["3"]["2"] as $dish~}
								<dd>{~$dish["name"]~}</dd>
								{~/foreach~}
                        </div>
                    </td>
                    <td style="padding:0px;vertical-align:middle;text-align: center;background-color:#bdc3c7;" class="palette-concrete btn-primary item" data-day="4" data-time="2">
                        <div>
								{~foreach $dishMap["4"]["2"] as $dish~}
								<dd>{~$dish["name"]~}</dd>
								{~/foreach~}
                        </div>
                    </td>
                    <td style="padding:0px;vertical-align:middle;text-align: center;background-color:#bdc3c7;" class="palette-concrete btn-primary item" data-day="5" data-time="2">
                        <div>
								{~foreach $dishMap["5"]["2"] as $dish~}
								<dd>{~$dish["name"]~}</dd>
								{~/foreach~}
                        </div>
                    </td>
                    <td style="padding:0px;vertical-align:middle;text-align: center;background-color:#bdc3c7;" class="palette-concrete btn-primary item" data-day="6" data-time="2">
                        <div>
								{~foreach $dishMap["6"]["2"] as $dish~}
								<dd>{~$dish["name"]~}</dd>
								{~/foreach~}
                        </div>                        
                    </td>
                </tr>
            </tbody>
        </table>
		
        <div class="row">
            <div class="col-md-6">
                <h4>一周食材统计</h4>
            </div>
			<a class="btn btn-primary btn_export pull-right">导出Excel表格</a>
        </div>
		
        <div class="row" style="min-height:260px;">
            <table id="excel" class="table table-striped table-responsive table-bordered">
                <thead>
                    <tr>
                        <th>食材名称</th>
                        <th>数量</th>
                        <th>总价</th>
                    </tr>
                </thead>
				<tbody>
					{~foreach $statistics as $row~}
						<tr>
							<td>{~$row["name"]~}</td>
							<td>{~$row["count"]~}<span class="pull-right">公斤</span></td>
							<td>{~$row["total"]~}<span class="pull-right">元</span></td>
						</tr>
					{~/foreach~}
				</tbody>
            </table>            
        </div>
		
		<div class="row">
			<h4>原料种类（如：鸡腿肉、葱、姜、蒜等）</h4>
		</div>
		
		<div class="row">
			<div class="col-md-6">
				<form id="classify_form" role="form">
					<div class="row">
						<div class="col-md-8">
		                    <div class="form-group">
		                        <input name="name" class="form-control" type="text" placeholder="请输入菜名" value="">
		                    </div>
						</div>
						<div class="col-md-4">
							<a class="btn btn-primary btnClassify">添加种类</a>
						</div>
					</div>			
				</form>				
			</div>
			
			<div class="col-md-6">
			       <div class="row" style="min-height:260px;">
			            <table class="table table-striped table-responsive">
			                <thead>
			                    <tr>
			                        <th>种类名称</th>
			                    </tr>
			                </thead>
							<tbody>
								
									{~foreach $classifies as $classify~}
									<tr>
										<td>
												<a class="edit_classify" data-url="/classify/{~$classify['id']~}" data-name="name" data-type="text" data-pk="{~$classify['id']~}">{~$classify["name"]~}</a>
										</td>
										<td style="width:130px;display:none;">
											<a class="deleteClassifyBtn btn btn-sm btn-danger" data-id="{~$classify['id']~}">删除</a>
										</td>
									</tr>
									{~/foreach~}						
								
							</tbody>
			            </table>            
			        </div>				
			</div>
			
		</div>
 
