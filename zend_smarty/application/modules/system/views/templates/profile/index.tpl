{~include file="header.tpl"~}
<script>
	$(function(){
		$("#profile_navi").addClass("active");
		
		$("#search_btn").click(function(){
			$.get("/user/user/index/keyword/"+$("#keyword").val()).promise().done(function(data){
				$("#user_list").html(data);
				$("#call_back_url").val("/user/user/index/keyword/"+$("#keyword").val());
			}).fail(function(data){
				console.log("好像出错了");
			});
		});
		
	});
	function openCloseAdvSearch(){
		if($('#collapseThree').hasClass('in')){
			$('#adv_icon').removeClass('icon-chevron-sign-up');
			$('#adv_icon').addClass('icon-chevron-sign-down');
		}else{
			$('#adv_icon').removeClass('icon-chevron-sign-down');
			$('#adv_icon').addClass('icon-chevron-sign-up');
		}
		if(!$('#addPanel').hasClass('display_off')){
			$('#addPanel').addClass('display_off');
		}
	}
</script>
<style>
	.search_txt{
		color: #AAAAAA;
    	font: 14px/20px 微软雅黑;
		margin:0;
		padding:0;
		height:35px;
	}

	#search_form strong{
		color:#468847;
	}

	#search_form .control-group{
		margin-bottom:0px;
	}
	#search_form{
		margin-bottom:0px;
	}
	#search_form .checkbox input{
		margin-bottom:10px;
		margin-left:10px;
	}
	#search_form label{
		width:auto;
	}
	#search_form .controls{
		margin-left:90px;
	}
	
</style>
<link rel="stylesheet" type="text/css" href="/css/daterangepicker.css"/>
	<input type="hidden" id="call_back_url">
	<h1 class="page-title">
		<i class="icon-pushpin"></i>
		<span style="cursor:pointer;" onclick="window.location.href='/system/profile'">档案管理</span>
	</h1>
	<a style="margin-bottom: 1.25em;"  class="btn btn-large btn-success" onclick="$('#addPanel').removeClass('display_off');if($('#collapseThree').hasClass('in')){$('#adv_btn').trigger('click');}">
		<i style="line-height:inherit;margin-top:0px;margin-right:3px;" class="icon-smile icon-large pull-left"></i>
		添加受测者	
	</a>
	

	<!--需要添加class = "alert accordion-group"  -->
	<div id="adv_search_panel" class="pull-right" style="padding:0 0 0 0;margin-bottom: 1.25em;width:400px;">
          <div class="accordion-heading">
          	<div class="pull-right">
				<input id="keyword" class="search_txt" placeholder="输入身份证号或者姓名，模糊搜索">
				<a class="btn btn-large btn-warning" id="search_btn">
					<i style="line-height:inherit;margin-top:0px;margin-right:3px;" class="icon-search icon-large pull-left"></i>
					搜索	
				</a>		
			</div>
 <!--           <div id="adv_btn" style="cursor:pointer;" href="#collapseThree" data-parent="#accordion2"  onclick="openCloseAdvSearch()"
						data-toggle="collapse" class="accordion-toggle">
				<i id="adv_icon" class="icon-chevron-sign-down icon-large" style="line-height:inherit;margin-top:0px;margin-right:3px;"></i>高级搜索
            </div>
-->
          </div>
          <div class="accordion-body collapse" id="collapseThree" style="height: 0px;">
            <div class="accordion-inner" style="border-color:#DFF0D8;">
				<form class="form-horizontal" id="search_form">
					<fieldset>
						<div class="control-group">
							<label for="sequence" class="control-label"><strong style="color:#468847">时间范围</strong></label>
							<div class="controls">
				               <div id="reportrange"  style="width:250px;background: #fff; cursor: pointer; padding: 3px 10px; border: 1px solid #ccc">
				                  <i class="icon-calendar icon-large"></i>
				                  从 <span id="start"></span> 至 <span id="end"></span> 止 <b class="caret" style="margin-top: 8px"></b>
				               </div>										
							</div>
						</div>
						<div class="control-group">
							<label for="option1" class="control-label"><strong style="color:#468847">测试情况</strong></label>
							<div class="controls">
					              <div class="checkbox inline">
					                <input type="radio" value="option1" name="finished" >已完成
									<input type="radio" value="option1" name="finished" >未完成
									<input type="radio" value="option1" name="finished" checked="true">不限制
					              </div>				
							</div>
						</div>
						<div class="control-group">
							<label for="option1" class="control-label"><strong style="color:#468847">性别范围</strong></label>
							<div class="controls">
					              <div class="checkbox inline">
					                <input type="radio" value="option1" name="sex_range" >男性
									<input type="radio" value="option1"  name="sex_range" >女性
									<input type="radio" value="option1"  name="sex_range"  checked="true">不限制
					              </div>				
							</div>
						</div>
						<div class="control-group pull-right">
							<label for="option1" class="control-label"><strong style="color:#468847"></strong></label>
							<div class="controls">
								<a  class="btn btn-small btn-warning">
									<i style="line-height:inherit;margin-top:0px;margin-right:3px;" class="icon-search icon-large pull-left"></i>
									确定搜索	
								</a>						
							</div>
						</div>
					</fieldset>
				</form>
               <script type="text/javascript">
               $(document).ready(function() {
                  $('#reportrange').daterangepicker(
                     {
                        ranges: {
                           '今日': [new Date(), new Date()],
                           '昨日': [moment().subtract('days', 1), moment().subtract('days', 1)],
                           '最近7日': [moment().subtract('days', 6), new Date()],
                           '最近30日': [moment().subtract('days', 29), new Date()],
                           '本月': [moment().startOf('month'), moment().endOf('month')],
                           '上月': [moment().subtract('month', 1).startOf('month'), moment().subtract('month', 1).endOf('month')]
                        },
                        opens: 'left',
                        format: 'YYYY-MM-DD',
                        separator: ' 至 ',
                        startDate: moment().subtract('days', 29),
                        endDate: new Date(),
                        minDate: '2012-01-01',
                        maxDate: '2025-12-31',
                        locale: {
                            applyLabel: '确定',
                            fromLabel: '从',
                            toLabel: '至',
                            customRangeLabel: '手动选择',
                            daysOfWeek: ['日', '一', '二', '三', '四', '五','六'],
                            monthNames: ['一月', '二月', '三月', '四月', '五月', '六月', '七月', '八月', '九月', '十月', '十一月', '十二月'],
                            firstDay: 1
                        },
                        showWeekNumbers: true,
                        buttonClasses: ['btn-danger'],
                        dateLimit: false
                     },
                     function(start, end) {
						$('#start').html(start.format('YYYY-MM-DD'));
				  		$('#end').html(end.format('YYYY-MM-DD'));
                     }
                  );
                  //Set the initial state of the picker label
                  $('#start').html(moment().subtract('days', 29).format('YYYY-MM-DD'));
				  $('#end').html(moment().format('YYYY-MM-DD'));

               });
               </script>

            </div>
          </div>
   </div>

	<div class="alert alert-info display_off" id="addPanel">
		<a class="close" onclick="$('#addPanel').addClass('display_off');document.getElementById('add_user_form').reset();">×</a>
		{~include file="../../../../user/views/templates/user/new.tpl"~}
	</div>

	
	<div id="user_list">
		{~include file="../../../../user/views/templates/user/index.tpl"~}
	</div>
	
	
<script>
	$(function(){
		$("#daterange").daterangepicker();
	});
</script>	
<script src="/scripts/framework/daterangepicker.js"></script>
<script src="/scripts/framework/moment.min.js"></script>
{~include file="footer.tpl"~}