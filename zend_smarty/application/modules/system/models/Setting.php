<?php
class System_Model_Setting {
	protected $id;
	protected $speed_boundary_min;
	protected $speed_boundary_max;
	protected $speed_pass;
	protected $speed_unfort;
	protected $response_time_max;
	protected $response_time_min;
	protected $response_time_right;
	protected $response_four_max;
	protected $response_four_min;
	protected $response_four_right;
	protected $response_pass;
	protected $response_unfort;
	protected $deep_boundary;
	protected $deep_high;
	protected $deep_low;
	protected $hand_time;
	protected $hand2_time;
	protected $hand_count;
	protected $hand2_count;
	protected $hand_pass;
	protected $hand_unfort;


	public function __construct(array $options = null)
	{
		if (is_array($options)) {
			$this->setOptions($options);
		}
	}
	
	public function setOptions(array $options)
	{
		$methods = get_class_methods($this);
		foreach ($options as $key => $value) {
			$method = 'set' . ucfirst($key);
			if (in_array($method, $methods)) {
				$this->$method($value);
			}
		}
		return $this;
	}
	public function toJson(){
		return json_encode($this->toArray());
	}
	
	public function toArray(){
		$array=array();
		foreach ($this as $key => $value) {
			if($value!=null){
				$array[$key] = $value;
			}
		}
		return $array;
	}
	/**
	 * @return the $id
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * @param field_type $id
	 */
	public function setId($id) {
		$this->id = $id;
	}


	/**
	 * @return the $response_boundary
	 */
	public function getResponse_boundary() {
		return $this->response_boundary;
	}

	/**
	 * @return the $response_high
	 */
	public function getResponse_high() {
		return $this->response_high;
	}

	/**
	 * @return the $response_low
	 */
	public function getResponse_low() {
		return $this->response_low;
	}

	/**
	 * @return the $deep_boundary
	 */
	public function getDeep_boundary() {
		return $this->deep_boundary;
	}

	/**
	 * @return the $deep_high
	 */
	public function getDeep_high() {
		return $this->deep_high;
	}

	/**
	 * @return the $deep_low
	 */
	public function getDeep_low() {
		return $this->deep_low;
	}

	/**
	 * @return the $hand_boundary
	 */
	public function getHand_boundary() {
		return $this->hand_boundary;
	}

	/**
	 * @return the $hand_high
	 */
	public function getHand_high() {
		return $this->hand_high;
	}

	/**
	 * @return the $hand_low
	 */
	public function getHand_low() {
		return $this->hand_low;
	}





	/**
	 * @return the $speed_boundary_min
	 */
	public function getSpeed_boundary_min() {
		return $this->speed_boundary_min;
	}

	/**
	 * @return the $speed_boundary_max
	 */
	public function getSpeed_boundary_max() {
		return $this->speed_boundary_max;
	}

	/**
	 * @return the $speed_pass
	 */
	public function getSpeed_pass() {
		return $this->speed_pass;
	}

	/**
	 * @return the $speed_unfort
	 */
	public function getSpeed_unfort() {
		return $this->speed_unfort;
	}

	/**
	 * @param field_type $speed_boundary_min
	 */
	public function setSpeed_boundary_min($speed_boundary_min) {
		$this->speed_boundary_min = $speed_boundary_min;
	}

	/**
	 * @param field_type $speed_boundary_max
	 */
	public function setSpeed_boundary_max($speed_boundary_max) {
		$this->speed_boundary_max = $speed_boundary_max;
	}

	/**
	 * @return the $hand_time
	 */
	public function getHand_time() {
		return $this->hand_time;
	}

	/**
	 * @return the $hand_count
	 */
	public function getHand_count() {
		return $this->hand_count;
	}

	/**
	 * @return the $hand_time
	 */
	public function getHand2_time() {
		return $this->hand2_time;
	}

	/**
	 * @return the $hand_count
	 */
	public function getHand2_count() {
		return $this->hand2_count;
	}

	/**
	 * @return the $hand_pass
	 */
	public function getHand_pass() {
		return $this->hand_pass;
	}

	/**
	 * @return the $hand_unfort
	 */
	public function getHand_unfort() {
		return $this->hand_unfort;
	}

	/**
	 * @param field_type $hand_time
	 */
	public function setHand_time($hand_time) {
		$this->hand_time = $hand_time;
	}

	/**
	 * @param field_type $hand_count
	 */
	public function setHand_count($hand_count) {
		$this->hand_count = $hand_count;
	}

	/**
	 * @param field_type $hand_time
	 */
	public function setHand2_time($hand2_time) {
		$this->hand2_time = $hand2_time;
	}

	/**
	 * @param field_type $hand_count
	 */
	public function setHand2_count($hand2_count) {
		$this->hand2_count = $hand2_count;
	}

	/**
	 * @param field_type $hand_pass
	 */
	public function setHand_pass($hand_pass) {
		$this->hand_pass = $hand_pass;
	}

	/**
	 * @param field_type $hand_unfort
	 */
	public function setHand_unfort($hand_unfort) {
		$this->hand_unfort = $hand_unfort;
	}

	/**
	 * @return the $response_time_max
	 */
	public function getResponse_time_max() {
		return $this->response_time_max;
	}

	/**
	 * @return the $response_time_min
	 */
	public function getResponse_time_min() {
		return $this->response_time_min;
	}

	/**
	 * @return the $response_time_right
	 */
	public function getResponse_time_right() {
		return $this->response_time_right;
	}

	/**
	 * @return the $response_four_max
	 */
	public function getResponse_four_max() {
		return $this->response_four_max;
	}

	/**
	 * @return the $response_four_min
	 */
	public function getResponse_four_min() {
		return $this->response_four_min;
	}

	/**
	 * @return the $response_four_right
	 */
	public function getResponse_four_right() {
		return $this->response_four_right;
	}

	/**
	 * @return the $response_pass
	 */
	public function getResponse_pass() {
		return $this->response_pass;
	}

	/**
	 * @return the $response_unfort
	 */
	public function getResponse_unfort() {
		return $this->response_unfort;
	}

	/**
	 * @param field_type $response_time_max
	 */
	public function setResponse_time_max($response_time_max) {
		$this->response_time_max = $response_time_max;
	}

	/**
	 * @param field_type $response_time_min
	 */
	public function setResponse_time_min($response_time_min) {
		$this->response_time_min = $response_time_min;
	}

	/**
	 * @param field_type $response_time_right
	 */
	public function setResponse_time_right($response_time_right) {
		$this->response_time_right = $response_time_right;
	}

	/**
	 * @param field_type $response_four_max
	 */
	public function setResponse_four_max($response_four_max) {
		$this->response_four_max = $response_four_max;
	}

	/**
	 * @param field_type $response_four_min
	 */
	public function setResponse_four_min($response_four_min) {
		$this->response_four_min = $response_four_min;
	}

	/**
	 * @param field_type $response_four_right
	 */
	public function setResponse_four_right($response_four_right) {
		$this->response_four_right = $response_four_right;
	}

	/**
	 * @param field_type $response_pass
	 */
	public function setResponse_pass($response_pass) {
		$this->response_pass = $response_pass;
	}

	/**
	 * @param field_type $response_unfort
	 */
	public function setResponse_unfort($response_unfort) {
		$this->response_unfort = $response_unfort;
	}

	/**
	 * @param field_type $speed_pass
	 */
	public function setSpeed_pass($speed_pass) {
		$this->speed_pass = $speed_pass;
	}

	/**
	 * @param field_type $speed_unfort
	 */
	public function setSpeed_unfort($speed_unfort) {
		$this->speed_unfort = $speed_unfort;
	}

	/**
	 * @param field_type $response_boundary
	 */
	public function setResponse_boundary($response_boundary) {
		$this->response_boundary = $response_boundary;
	}

	/**
	 * @param field_type $response_high
	 */
	public function setResponse_high($response_high) {
		$this->response_high = $response_high;
	}

	/**
	 * @param field_type $response_low
	 */
	public function setResponse_low($response_low) {
		$this->response_low = $response_low;
	}

	/**
	 * @param field_type $deep_boundary
	 */
	public function setDeep_boundary($deep_boundary) {
		$this->deep_boundary = $deep_boundary;
	}

	/**
	 * @param field_type $deep_high
	 */
	public function setDeep_high($deep_high) {
		$this->deep_high = $deep_high;
	}

	/**
	 * @param field_type $deep_low
	 */
	public function setDeep_low($deep_low) {
		$this->deep_low = $deep_low;
	}

	/**
	 * @param field_type $hand_boundary
	 */
	public function setHand_boundary($hand_boundary) {
		$this->hand_boundary = $hand_boundary;
	}

	/**
	 * @param field_type $hand_high
	 */
	public function setHand_high($hand_high) {
		$this->hand_high = $hand_high;
	}

	/**
	 * @param field_type $hand_low
	 */
	public function setHand_low($hand_low) {
		$this->hand_low = $hand_low;
	}


}

?>