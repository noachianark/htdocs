        <script>
        	$(function(){
				$("#material_type").selectpicker({style: 'btn-lg btn-primary', menuStyle: 'dropdown-inverse'});
				$("#backToDish").on("click",function(){
					$(".dishpanel").slideDown();
					$(".materialpanel").slideUp()
					$(".materialpanel").html();
					
				});
				

				$(".delMaterial").on("click",function(){
					$.ajax({
						url:"/material/"+$(this).data("id"),
						type:"DELETE"
					}).done(function(data){
						$.ajax({
							url:"/dish/"+eval("{~$dish['id']~}"),
							type:"GET"
						}).done(function(data){
							
							$(".materialpanel").html();
							$(".materialpanel").html(data);
							
						}).fail(faultHandler);				
					}).fail(faultHandler);
				});
				
				$("#addMaterial").on("click",function(){
					$.ajax({
						url:"/material",
						type:"POST",
						data:{
							type_id:$("#material_type").val(),
							dish_id:eval("{~$dish['id']~}"),
							count:parseFloat(eval("{~$dish['parameter']~}"))*parseFloat($("#count").val()),
							price:parseFloat($("#price").val())
						}
					}).done(function(data){
						$.ajax({
							url:"/dish/"+eval("{~$dish['id']~}"),
							type:"GET"
						}).done(function(data){
							
							$(".materialpanel").html();
							$(".materialpanel").html(data);
							
						}).fail(faultHandler);

					}).fail(faultHandler);
				});
				
				
				$(".edit_material_count").editable({
					title:"修改类型",
				    ajaxOptions: {
					    type: 'POST'
				    },
					params: function(params) {
					    //originally params contain pk, name and value
						var par = {};
					    par.count = params.value;
					    return par;
				    },
					success:function(){
						$.ajax({
							url:"/dish/"+eval("{~$dish['id']~}"),
							type:"GET"
						}).done(function(data){
							
							$(".materialpanel").html();
							$(".materialpanel").html(data);
							
						}).fail(faultHandler);			
					}
				});				
				
				$(".edit_material_price").editable({
					title:"修改类型",
				    ajaxOptions: {
					    type: 'POST'
				    },
					params: function(params) {
					    //originally params contain pk, name and value
						var par = {};
					    par.price = params.value;
					    return par;
				    },
					success:function(){
						$.ajax({
							url:"/dish/"+eval("{~$dish['id']~}"),
							type:"GET"
						}).done(function(data){
							
							$(".materialpanel").html();
							$(".materialpanel").html(data);
							
						}).fail(faultHandler);		
					}
				});				
				
				
			});
        </script>

        <div class="row">
            <div id="backToDish" class="backBtn btn-hg col-md-2">
                <span class="glyphicon glyphicon-chevron-left"></span> 返回
            </div>
        </div>
		
        


 

		<form id="material_form" role="form">
            
            	
			<div class="row">
				<div class="col-md-4">
	                <h5>菜品原料清单</h5>
	            </div>
			</div>
			
            <div class="row">
            	<input type="hidden" name="dish_id" value="{~$dish['id']~}">
                <div class="col-md-3">
                    <select name="type_id" class="selectpicker" id="material_type" data-container="body" data-size="auto">
                        {~foreach $classify as $cls~}
							<option value="{~$cls['id']~}">{~$cls['name']~}</option>
						{~/foreach~}
                    </select>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <div class="input-group">
                            <input id="count" name="count" type="text" class="form-control" placeholder="采购数量">
                            <span class="input-group-addon">公斤 X 系数{~$dish["parameter"]~}</span>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <div class="input-group">
                            <input id="price" name="price" type="text" class="form-control" placeholder="采购单价">
                            <span class="input-group-addon">元</span>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">

                    <a type="button" class="btn btn-primary" id="addMaterial">
                      <span class="glyphicon glyphicon-ok"></span> 添加原料
                    </a>


                </div>



            </div>
        
        </form>
        


        <table class="table table-bordered table-responsive">
            <thead>
                <tr>
                    <th>材料</th>
                    <th>数量</th>
                    <th>单价</th>
                    <th></th>
                </tr>
            </thead>
			<tbody>
				{~foreach $materials as $material~}
					<tr>
						<td>
							 {~foreach $classify as $cls~}
								{~if $cls["id"] eq $material["type_id"]~}
									{~$cls["name"]~}
								{~/if~}
							{~/foreach~}
						</td>
						<td>
							<a class="edit_material_count" data-url="/material/{~$material['id']~}" data-name="count" data-type="text" data-pk="{~$material['id']~}">{~$material["count"]~}</a>											
							<a class="pull-right">公斤</a>
						</td>
						<td>
							<a class="edit_material_price" data-url="/material/{~$material['id']~}" data-name="price" data-type="text" data-pk="{~$material['id']~}">{~$material["price"]~}</a>											
							<a class="pull-right">元</a>
						</td>
						<td  style="width:130px;">
								<a class="btn btn-sm btn-danger delMaterial"  data-id="{~$material['id']~}">删除</a>							
						</td>
					</tr>
				{~/foreach~}
			</tbody>
        </table>
        

        

        