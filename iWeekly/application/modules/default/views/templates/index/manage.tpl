{~include file="header.tpl"~}
<div class="container">
        <div class="row">
            <div id="backToMain" class="backBtn btn-hg col-md-2">
                <span class="glyphicon glyphicon-chevron-left"></span> 返回
            </div>    
        </div>
		
		<div class="row">
			<div class="col-md-5">
				
				<div class="row">
					<h5>菜品大全</h5>
				</div>
				
				<div class="row">
					<form role="form" id="dish_form">
					
	                    <div class="form-group col-md-8">
	                        <input name="name" class="form-control" type="text" placeholder="菜名" value="">
	                    </div>	
						
						<div class="form-group pull-right">
			                <a type="button" class="btn btn-primary pull" id="addDishBtn">
			                  <span class="glyphicon glyphicon-ok"></span> 添加菜品
			                </a>
						</div>
						
					</form>
				</div>
				
				<div class="row">
					<table class="table table-striped table-responsive  table-bordered">
			            <thead>
			                <tr>
			                    <th>菜名</th>
			                    <th></th>
			                </tr>
			            </thead>
						<tbody id="dish_table">
			
						</tbody>
			        </table>					
				</div>	
				
			</div><!--菜品管理 -->
			
			<div class="col-md-1"></div>
			
			<div class="col-md-6">
				
				<div class="row">
					<h5>原料大全</h5>
				</div>
				
				<form role="form" id="material_form">
					<div class="row">
						
						<div class="col-md-4">
		                    <div class="form-group">
		                        <input name="name" class="form-control" type="text" placeholder="原料名" value="">
		                    </div>							
						</div>
						
						<div class="col-md-4">
		                    <div class="form-group">
							     <div class="input-group">
		                            <input name="price"  type="text" class="form-control" placeholder="原料单价">
		                            <span class="input-group-addon">元</span>
		                        </div>
		                    </div>							
						</div>
						
						<div class="col-md-4">
			                <a type="button" class="btn btn-primary" id="addMaterialBtn">
			                  <span class="glyphicon glyphicon-ok"></span> 添加原料
			                </a>							
						</div>		
																
					</div>
				</form>
				
				<div class="row">
					<table class="table table-striped table-responsive  table-bordered">
			            <thead>
			                <tr>
			                    <th>原料名</th>
								<th>单价</th>
			                </tr>
			            </thead>
						<tbody id="material_table">

						</tbody>
			        </table>					
				</div>	

			</div><!--原料管理-->
			
			
		</div>
		
</div>
		
{~include file="js.tpl"~}


<script>
	$(function(){
		
		$.get("/material").done(function(data){
			$("#material_table").html(data);
		}).fail(faultHandler);
		
		$.get("/dish").done(function(data){
			$("#dish_table").html(data);
		}).fail(faultHandler);
		
		$("#addMaterialBtn").on("click",function(){
			$.post("/material",$("#material_form").serialize()).done(function(data){
				$.get("/material").done(function(data){
					$("#material_table").html(data);
					$("#material_form")[0].reset();
				}).fail(faultHandler);
			}).fail(faultHandler);
		});
		
		
		$("#backToMain").on("click",function(){
			window.location.href="/";
		});
		
		$("#addDishBtn").on("click",function(){
			$.post("/dish",$("#dish_form").serialize()).done(function(data){
				$.get("/dish").done(function(data){
					$("#dish_table").html(data);
					$("#dish_form")[0].reset();
				}).fail(faultHandler);
			}).fail(faultHandler);
		})

		
		
	});
</script>

{~include file="footer.tpl"~}