<?php
class System_Model_Administrator{
	protected $username;
	protected $password;
	protected $id;
	protected $role;
	protected $forbidden;
	
	public function __construct(array $options = null)
	{
		if (is_array($options)) {
			$this->setOptions($options);
		}
	}
	
	public function setOptions(array $options)
	{
		$methods = get_class_methods($this);
		foreach ($options as $key => $value) {
			$method = 'set' . ucfirst($key);
			if (in_array($method, $methods)) {
				$this->$method($value);
			}
		}
		return $this;
	}
	
	public function toJson(){
		return json_encode($this->toArray());
	}
	
	public function toArray(){
		$array=array();
		foreach ($this as $key => $value) {
			if($value!=null){
				$array[$key] = $value;
			}
		}
		return $array;
	}
	
	public function setUsername($text){
		$this->username = (string) $text;
		return $this;
	}
	public function getUsername(){
		return $this->username;
	}
	
	public function setPassword($text){
		$this->password = (string) $text;
		return $this;
	}
	public function getPassword(){
		return $this->password;
	}
	
	public function setRole($id){
		$this->role = (string) $id;
		return $this;
	}
	public function getRole(){
		return (int)$this->role;
	}
	
	public function setId($id){
		$this->id = (string) $id;
		return $this;
	}
	public function getId(){
		return $this->id;
	}
	/**
	 * @return the $forbidden
	 */
	public function getForbidden() {
		return $this->forbidden;
	}

	/**
	 * @param field_type $forbidden
	 */
	public function setForbidden($forbidden) {
		$this->forbidden = $forbidden;
	}

	
}

?>