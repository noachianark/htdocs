<?php
class User_Model_User{
	protected $id;
	protected $name;
	protected $serial;
	protected $sex;
	protected $finish;
	protected $clear_time;
	protected $state;
	protected $id_card;
	protected $is_delete;
	
	public function __construct(array $options = null)
	{
		if (is_array($options)) {
			$this->setOptions($options);
		}
	}
	
	public function setOptions(array $options)
	{
		$methods = get_class_methods($this);
		foreach ($options as $key => $value) {
			$method = 'set' . ucfirst($key);
			if (in_array($method, $methods)) {
				$this->$method($value);
			}
		}
		return $this;
	}
	
	public function toJson(){
		return json_encode($this->toArray());
	}
	
	public function toArray(){
		$array=array();
		foreach ($this as $key => $value) {
			if($value!=null){
				$array[$key] = $value;
			}
		}
		return $array;
	}
	/**
	 * @return the $finish
	 */
	public function getFinish() {
		return $this->finish;
	}

	/**
	 * @return the $clear_time
	 */
	public function getClear_time() {
		return $this->clear_time;
	}

	/**
	 * @param field_type $finish
	 */
	public function setFinish($finish) {
		$this->finish = $finish;
	}

	/**
	 * @param field_type $clear_time
	 */
	public function setClear_time($clear_time) {
		$this->clear_time = $clear_time;
	}

	/**
	 * @return the $id
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * @return the $name
	 */
	public function getName() {
		return $this->name;
	}

	/**
	 * @return the $serial
	 */
	public function getSerial() {
		return $this->serial;
	}

	/**
	 * @return the $sex
	 */
	public function getSex() {
		return $this->sex;
	}

	/**
	 * @param field_type $id
	 */
	public function setId($id) {
		$this->id = $id;
	}

	/**
	 * @param field_type $name
	 */
	public function setName($name) {
		$this->name = $name;
	}

	/**
	 * @return the $identity
	 */
	public function getId_card() {
		return $this->id_card;
	}

	/**
	 * @param field_type $identity
	 */
	public function setId_card($id_card) {
		$this->id_card = $id_card;
	}

	/**
	 * @param field_type $serial
	 */
	public function setSerial($serial) {
		$this->serial = $serial;
	}

	/**
	 * @param field_type $sex
	 */
	public function setSex($sex) {
		$this->sex = $sex;
	}
	/**
	 * @return the $state
	 */
	public function getState() {
		return $this->state;
	}

	/**
	 * @param field_type $state
	 */
	public function setState($state) {
		$this->state = $state;
	}


	public function setIs_delete($delete){
		$this->state = $delete;
	}


	
	

}