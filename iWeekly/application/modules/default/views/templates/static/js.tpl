    <script src="/js/jquery-1.8.3.min.js"></script>
    <script src="/js/jquery-ui-1.10.3.custom.min.js"></script>
    <script src="/js/jquery.ui.touch-punch.min.js"></script>
    <script src="/js/bootstrap.min.js"></script>
    <script src="/js/bootstrap-select.js"></script>
    <script src="/js/bootstrap-switch.js"></script>
    <script src="/js/flatui-checkbox.js"></script>
    <script src="/js/flatui-radio.js"></script>
    <script src="/js/jquery.tagsinput.js"></script>
    <script src="/js/jquery.placeholder.js"></script>
    <script src="/js/knockout-3.0.0.js" type="text/javascript"></script>
	<script src="/js/toastr.min.js"></script>
	<script src="/js/bootstrap-editable.min.js"></script>
	
	<script>
		var faultHandler = function(){
            console.log("服务器错误");
        }
        var dayArr = ["星期一","星期二","星期三","星期四","星期五","星期六","星期日"];
        var timeArr = ["早餐","中餐","晚餐"];
	</script>