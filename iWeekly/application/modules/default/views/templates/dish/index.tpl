{~foreach $dishes as $dish~}
	<tr>
		<td>
				<a class="edit_dish_name" data-url="/dish/{~$dish['id']~}" data-name="name" data-type="text" data-pk="{~$dish['id']~}">{~$dish["name"]~}</a>									
		</td>
		<td style="width:130px;">
			<a class="btn btn-info btn-sm setDishBtn" data-id="{~$dish['id']~}">设定</a>
			<a class="btn btn-danger btn-sm delDishBtn" data-id="{~$dish['id']~}">删除</a>
		</td>					
	</tr>
{~/foreach~}


<script>
	$(function(){

		$(".setDishBtn").on("click",function(){
			window.location.href="/dish/"+$(this).data("id");
		});
		
		$(".delDishBtn").on("click",function(){
			$.ajax({
				url:"/dish/"+$(this).data("id"),
				type:"DELETE"
			}).done(function(){
				$.get("/dish").done(function(data){
					$("#dish_table").html(data);
				}).fail(faultHandler);
			}).fail(faultHandler);
		});


		$(".edit_dish_name").editable({
			title:"修改菜名",
		    ajaxOptions: {
			    type: 'POST'
		    },
			params: function(params) {
			    //originally params contain pk, name and value
			    params.name = params.value;
			    return params;
		    },
			success:function(){
				$.get("/dish").done(function(data){
					$("#dish_table").html(data);
				}).fail(faultHandler);			
			}
		});
		
		
		
				
	});
</script>