<?php
class Auth_Plugin_Acl extends Zend_Controller_Plugin_Abstract{
	
	
	
	public function preDispatch(Zend_Controller_Request_Abstract $request){
		//全局登录控制。
		//return;
		if($request->getModuleName()=="default" || $request->getControllerName()=="login"){
			return;
		}else{
			$auth=Zend_Auth::getInstance();
			if(!$auth->hasIdentity()){
				
				$request->setParams(array(
						"module"=>$request->getModuleName(),
						"controller"=>$request->getControllerName(),
						"action"=>$request->getActionName(),
						"params"=>$request->getUserParams()
						))->setModuleName('default')
				                        ->setControllerName('error')
				                        ->setActionName('access')
				                        ->setDispatched(true);
			}else{
				
				$acl = new Zend_Acl();
				
				$acl->addRole(new Zend_Acl_Role('user'))->addRole(new Zend_Acl_Role('admin'));
				$acl->allow("user",null,array("user/home","user/answer","user/gauge","user/report"));
				if(!isset(Zend_Auth::getInstance()->getIdentity()->username)){
					//是user
					if(!$acl->isAllowed("user",null,$request->getModuleName()."/".$request->getControllerName())){
						$request->setParams(array(
								"module"=>$request->getModuleName(),
								"controller"=>$request->getControllerName(),
								"action"=>$request->getActionName(),
								"params"=>$request->getUserParams()
						))->setModuleName('default')
						->setControllerName('error')
						->setActionName('access')
						->setDispatched(true);
						$auth->clearIdentity();						
					}
				}
				//$request->setParam("admin", $auth->getIdentity());
// 				if($request->getControllerName()!="index"){
// 					if(!$request->isXmlHttpRequest()){
// 						$request->setParams(array(
// 								"module"=>$request->getModuleName(),
// 								"controller"=>$request->getControllerName(),
// 								"action"=>$request->getActionName(),
// 								"params"=>$request->getUserParams()
// 						))->setModuleName('default')
// 						->setControllerName('error')
// 						->setActionName('client')
// 						->setDispatched(true);
// 					}
// 				}
				
			}
		}
		
	}
	
	
	
	public function postDispatch(Zend_Controller_Request_Abstract $request){
		$auth=Zend_Auth::getInstance();
		if(!$auth->hasIdentity()){
			return;
		}else{
			$authSession = new Zend_Session_Namespace('Zend_Auth');
			$authSession->setExpirationSeconds(3600);
		}
		//增加操作日志。
	}
}